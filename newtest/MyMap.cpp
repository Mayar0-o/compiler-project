#include"MyMap.h"
#include"string.h"
#include"MyScope.h"
#include <stdio.h>
#include <iostream>
#include<numeric>
#include<string>
#include<vector>
MyMap::MyMap(){
	this->map = new Symbol*[MAX_LENGTH];
	for (int i = 0; i < MAX_LENGTH; i++)
		this->map[i] = nullptr;
}

MyMap::~MyMap(){

}

Symbol* MyMap::insert(Symbol* symbol)
{

	Symbol* sym = this->lookup(symbol->getName());
	if (sym){ 
		return nullptr;	
	}
	int hashIndex = this->hash(symbol->getName());
	Symbol* next = this->get(hashIndex);
	if (next == nullptr){ 
		return this->put(hashIndex, symbol);
	}
	
	while (next->getNext()){
		next = next->getNext();
	}
	next->setNext(symbol); 
	return symbol;

}

Symbol* MyMap::lookup(char* name){
	int hashIndex = this->hash(name);
	if (this->get(hashIndex) != nullptr && strcmp(this->get(hashIndex)->getName(), name) == 0)
		return this->get(hashIndex);
	if (this->get(hashIndex) == nullptr)
		return nullptr;
	Symbol* next = this->get(hashIndex)->getNext();
	while (next != nullptr){
		if (strcmp(next->getName(), name) == 0) 
			return next;
		next = next->getNext();
	}
	return nullptr;
}

int MyMap::hash(char* name){
	unsigned int i;
	int retVal = 0;
	for (i = 0; i < strlen(name); i++)
	{
		retVal <<= 2;
		retVal ^= (int)(*name);
		name++;
	}
	return (retVal % MAX_LENGTH);
}

Symbol* MyMap::get(int index){
	return this->map[index];
}

Symbol* MyMap::put(int index, Symbol* symbol){
	this->map[index] = symbol;
	return symbol;
}

string MyMap::toString(){

	string owner;
	string id ;
	string idper ;
	int ID = this->getOwnerScope()->GetScopeId();
	int IDPER;

	if (this->getOwnerScope()->getParentScope() == nullptr)
		idper = "NULL";
	else{
		IDPER = this->getOwnerScope()->getParentScope()->GetScopeId();
		idper = to_string(IDPER);
	}
	id = to_string(ID);
	
	this->getOwnerScope()->getOwnerSymbol() != nullptr ? owner = this->getOwnerScope()->getOwnerSymbol()->getName() :owner = this->getOwnerScope()->getownername();
	string str =id +":"+"Scope of " + owner +"->>inner in: <"+ idper+">..\r\n";
	for (int i = 0; i < MAX_LENGTH; i++){
		Symbol* symWalker = this->map[i]; 
		while (symWalker != nullptr){
			str.append(symWalker->toString() + "\r\n");
			symWalker = symWalker->getNext();
		}
	}
	str.append("__________________________________________\r\n");
	return str;
}

void MyMap::setOwnerScope(Scope* owner){
	this->ownerScope = owner;
}

Scope* MyMap::getOwnerScope(){
	return this->ownerScope;
}

bool MyMap::remove(char* name){
	int hashIndex = this->hash(name);
	if (this->get(hashIndex) != nullptr && strcmp(this->get(hashIndex)->getName(), name) == 0){
		this->map[hashIndex] = this->map[hashIndex]->getNext();
		return true;
	}

	if (this->get(hashIndex) == nullptr)
		return true;
	Symbol* next = this->get(hashIndex)->getNext();
	Symbol* prev = next;
	while (next != nullptr){
		if (strcmp(next->getName(), name) == 0){
			prev->setNext(next->getNext());
			return true;
		}
		prev = next;
		next = next->getNext();
	}
	return nullptr;
}



vector<Symbol*> MyMap::symbols() {
	vector<Symbol*> symbols_vector;
	for (int i = 0; i < MAX_LENGTH; ++i) {
		auto symbol = map[i];
		while (symbol) {
			symbols_vector.push_back(symbol);
			symbol = symbol->getNext();
		}
	}
	return symbols_vector;
}
