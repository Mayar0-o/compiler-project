﻿#include "MyParser.h"
#include"myvalue.h"
#include<vector>

MyParser::MyParser(){

	this->rootScope = new Scope(nullptr,0);
	this->currScope = this->rootScope;
	this->errRecovery = &errorRec;
	this->currClassSym = nullptr;
	this->classesStack = new stack<Class*>();

}

MyParser::~MyParser(){

}

void MyParser::setCurrentScope(Scope* currScope){
	this->currScope = currScope;
}

Scope* MyParser::getCurrentScope(){
	return this->currScope;
}

Scope* MyParser::getRootScope(){
	return this->rootScope;
}

Symbol* MyParser::insertSymbol(Symbol* symbol){

	Symbol* insertedSym = this->currScope->getSymbolTable()->insert(symbol);
	if (symbol->getSymbolType() == FUNCTION)
	{
		while (!insertedSym){
			symbol->setName(string(symbol->getName()) + "_X");
			insertedSym = this->currScope->getSymbolTable()->insert(symbol);
		}
	}
	else
	{
		while (!insertedSym){
			this->errRecovery->errQ->enqueue(symbol->getLineNo(), symbol->getColNo(), "Identifier Already Declared.", symbol->getName());

			symbol->setName(string(symbol->getName()) + "_X");
			insertedSym = this->currScope->getSymbolTable()->insert(symbol );
		}
	}
	return insertedSym;
}

Symbol* MyParser::insertSymbol(Symbol* symbol, Scope* scope){
	

	Symbol* insertedSym = scope->getSymbolTable()->insert(symbol);
	if (symbol->getSymbolType() == FUNCTION)
	{
		while (!insertedSym){
			symbol->setName(string(symbol->getName()) + "_X");
			insertedSym = scope->getSymbolTable()->insert(symbol);
		}
	}
	else{
		while (!insertedSym){
			this->errRecovery->errQ->enqueue(symbol->getLineNo(), symbol->getColNo(), "Identifier Already Declared.", symbol->getName());

			symbol->setName(string(symbol->getName()) + "_X");
			insertedSym = scope->getSymbolTable()->insert(symbol);
		}
	}

	return insertedSym;
}

Symbol* MyParser::lookUpSymbol(char* name,int lineNo,int colNo){
	
	Scope* scanningScope = this->currScope; 
	while (scanningScope != nullptr){ 
	
		Symbol* symbol = scanningScope->getSymbolTable()->lookup(name);
		if (symbol)
			return symbol;
		else
			scanningScope = scanningScope->getParentScope(); 
	}

	return nullptr;
	
}

Symbol* MyParser::lookUpSymbol(Scope* scope, char* name, int lineNo, int colNo){
	Scope* scanningScope = scope; 
	while (scanningScope != nullptr){ 
	
		Symbol* symbol = scanningScope->getSymbolTable()->lookup(name);
		if (symbol)
			return symbol;
		else
			scanningScope = scanningScope->getParentScope(); 
	}

	return nullptr;
}

Symbol* MyParser::lookUpSymbol(Scope* scope, char* name) {
	Scope* scanningScope = scope;
	while (scanningScope != nullptr) { 
		Symbol* symbol = scanningScope->getSymbolTable()->lookup(name);
		if (symbol)
			return symbol;
		else
			scanningScope = scanningScope->getParentScope(); 
	}
	return nullptr;
}

void MyParser::finishscop(){
	this->currScope = this->currScope->getParentScope();
}

void MyParser::printMyMap(){
	std::ofstream os;
	os.open("scope.txt", std::ofstream::out);
	os << this->buildTableString(rootScope) << std::endl;
	os.close();
}

string MyParser::buildTableString(Scope* scope){
	string str;
	str = scope->getSymbolTable()->toString();
	Scope* innersWalker = scope->getInnerScope();
	while (innersWalker){
		str.append(this->buildTableString(innersWalker));
		innersWalker = innersWalker->getNextScope();
	}
	return str;
}

void MyParser::removev(char* name, Scope* scope){
	scope->getSymbolTable()->remove(name);
}

Symbol* MyParser::insertFunctionSymbol(char* name, int returnType,int colNo, int lineNo, Scope* scope, Symbol* params,int* mod_opt ,int counter){
	
	Function* funcSym = new Function(name, returnType, colNo, lineNo, scope,mod_opt,counter);
	this->insertSymbol(funcSym, scope->getParentScope());
	
	scope->setOwnerSymbol(funcSym);
	funcSym->setBodyScope(scope);
	if (params){
		funcSym->setParams(params);
		
		return funcSym;
	}
	else
	return funcSym;
	

	
}

Symbol* MyParser::insertFunctionSymbol(char* name, int returnType, int colNo, int lineNo,  Symbol* params, int* mod_opt, int counter){

	Function* funcSym = new Function(name, returnType, colNo, lineNo, mod_opt, counter);
	this->insertSymbol(funcSym, this->getCurrentScope());

	funcSym->setBodyScope(nullptr);
	if (params){
		funcSym->setParams(params);

		return funcSym;
	}
	else
		return funcSym;
}

Symbol* MyParser::insertFunctionSymbol(char* name,  int colNo, int lineNo, Symbol* params, int* mod_opt, int counter){

	Function* funcSym = new Function(name,  colNo, lineNo, mod_opt, counter);
	this->insertSymbol(funcSym, this->getCurrentScope());

	funcSym->setBodyScope(nullptr);
	if (params){
		funcSym->setParams(params);

		return funcSym;
	}
	else
		return funcSym;
}

Symbol* MyParser::insertFunctionSymbol(char* name,  int colNo, int lineNo, Scope* scope, Symbol* params, int* mod_opt, int counter){

	Function* funcSym = new Function(name,  colNo, lineNo, scope, mod_opt, counter);
	this->insertSymbol(funcSym, scope->getParentScope());

	scope->setOwnerSymbol(funcSym);
	funcSym->setBodyScope(scope);

		if (params){
			funcSym->setParams(params);

			return funcSym;
		}
		else
			return funcSym;


}



Symbol* MyParser::setClass(char* inhertedFrom, Class* classSymbol, Scope* scope,Class* outerclass) {
	if (inhertedFrom != nullptr)
		classSymbol->setInhertedFrom(inhertedFrom);
	    classSymbol->setBodyScope(scope);
	    scope->setOwnerSymbol(classSymbol); 
		if (!outerclass)
			classSymbol->setOuterClass(nullptr);
			
		else
			classSymbol->setOuterClass(outerclass);

	    return classSymbol;
}

stack<Class*>* MyParser::getClassstack(){
	return this->classesStack;

}

Class* MyParser::getCurrentClassSym(){
	if (!(this->classesStack->empty()))
		return this->classesStack->top();
	else
		return this->currClassSym;
}

void MyParser::pushToClassesStack(Class* classSym){
	this->classesStack->push(classSym);
	this->currClassSym = classSym;
}

void MyParser::popFromClassesStack(){
	this->classesStack->pop();
	if (!(this->classesStack->empty()))
		this->currClassSym = this->classesStack->top();
	else
		this->currClassSym = nullptr;
}


Scope* MyParser::createNewScope(int scopeId){
	Scope* scope = new Scope(this->getCurrentScope(), scopeId);
	this->getCurrentScope()->addInCuurent(scope);
	this->setCurrentScope(scope);
	return scope;
}








//call all check methods
void MyParser::checkclassdec(){
	this->travelallscope(this->getRootScope());	//create class vector
	this->checkclassdecleration(); //final and undef inherted
	this->checkclassdeclerationname(); //same  name of two class in all programm
	this->check_ring_inher();
	this->checkInnerClasses();
	//this->constructerror();
	this->modifiersget();
	this->checkparametars();
	this->checkfinaloverridingmethods();
	this->checkmainmethod();
	this->checkoverloadmethod();
	this->checkabstructclass();
	this->onepubliclasscheck(this->getRootScope());
}
//init vector to begin check
void MyParser::travelallscope(Scope * scope){
	if (scope == nullptr)
		return;
	addclasstovector(scope);
	addsymboltovector(scope);
	addfunctiontovector(scope);
	Scope* innersWalker = scope->getInnerScope();
	if (innersWalker == nullptr)
		return;
	while (innersWalker != nullptr){
		travelallscope(innersWalker);
		innersWalker = innersWalker->getNextScope();
	}
}

//void MyParser::travelallscopeforinner(Scope * scope , Scope* curr,Class*inner){
//	if (scope == nullptr)
//		return;
//	if (scope == curr)
//		return;
//	addclasstovectorinner(scope,inner);
//	Scope* innersWalker = scope->getInnerScope();
//	if (innersWalker == nullptr)
//		return;
//	if (innersWalker == curr)
//		innersWalker = innersWalker->getNextScope();
//	while (innersWalker != nullptr){
//		travelallscopeforinner(innersWalker,curr,inner);
//		innersWalker = innersWalker->getNextScope();
//	}
//}
//void MyParser::addclasstovectorinner(Scope * scope, Class* inner)
//{
//	vector<Symbol*> symbols = scope->getSymbolTable()->symbols();
//	vector<Symbol*>::iterator i;
//	for (i = symbols.begin(); i != symbols.end(); ++i)
//	{
//		Symbol* s = *i;
//		int sType = s->getSymbolType();
//		if (sType == CLASS)
//		{
//			if (inner->getOuterClass() != nullptr){
//				Class* outersWalker = inner->getOuterClass();
//				while (outersWalker != nullptr){
//					string one = string(outersWalker->getName());
//					string two = string(s->getName());
//					if (one != two){
//						this->symbolsallclassforinner.push_back(s);
//					}
//					if (outersWalker->getOuterClass() != nullptr)
//						outersWalker = outersWalker->getOuterClass();
//					else
//						outersWalker = nullptr;
//				}
//			}
//		}
//	}
//}
//void MyParser::checkclassforinnerinher(Class* inner){
//
//	vector<Symbol*>::iterator j;
//	for (j = this->symbolsallclassforinner.begin(); j != this->symbolsallclassforinner.end(); ++j)
//	{
//		Symbol* ss = *j;
//		Class* c = dynamic_cast<Class*>(ss);
//		string one = c->getInhertedFrom();
//		string two = string(inner->getName());
//		if (one == two)
//		{
//			if (c->getOuterClass()->getInhertedFrom() != string(inner->getOuterClass()->getName()))
//			{
//				//string errString = string(c->getName()) + "outerrrr can't inhereted from inner " + inner->getName();
//				this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), "cant inherted from inner , should make your outer inherted from outer of class witch inhereted from", "");
//			}
//		}
//
//
//	}
//}

void MyParser::addsymboltovector(Scope * scope)
{
	vector<Symbol*> symbols = scope->getSymbolTable()->symbols();
	vector<Symbol*>::iterator i;
	for (i = symbols.begin(); i != symbols.end(); ++i)
	{
		Symbol* s = *i;
		int sType = s->getSymbolType();
		if (sType == CLASS | sType == FUNCTION | sType == DATA_MEMBER)
		{
			this->symbolsallsymbols.push_back(s);
			this->symbolsallsymbolscomp.push_back(s);
		}

	}

}
void MyParser::addclasstovector(Scope * scope)
{
	vector<Symbol*> symbols = scope->getSymbolTable()->symbols();
	vector<Symbol*>::iterator i;
	for (i = symbols.begin(); i != symbols.end(); ++i)
	{
		Symbol* s = *i;
		int sType = s->getSymbolType();
		if (sType == CLASS)
		{
			this->symbolsallclass.push_back(s);
		    this->symbolsallclasscomp.push_back(s);
		}
		
	}
	

}
void MyParser::addfunctiontovector(Scope * scope)
{
	vector<Symbol*> symbols = scope->getSymbolTable()->symbols();
	vector<Symbol*>::iterator i;
	for (i = symbols.begin(); i != symbols.end(); ++i)
	{
		Symbol* s = *i;
		int sType = s->getSymbolType();
		if (sType == FUNCTION)
		{
			this->symbolsallfunction.push_back(s);
			this->symbolsallfunctioncomp.push_back(s);
		}

	}


}
//class decleration and inheretance error
void MyParser::checkclassdecleration(){
	vector<Symbol*>::iterator j;
	for (j = this->symbolsallclass.begin(); j !=this->symbolsallclass.end(); ++j)
	{

		Symbol* ss = *j;
		Class* c = dynamic_cast<Class*>(ss);

		if (c->getInhertedFrom() != "Object")
		{

			bool foundBase = false;
			vector<Symbol*>::iterator k = this->symbolsallclasscomp.begin();
			while (k != this->symbolsallclasscomp.end() && !foundBase)
			{
				Symbol* st = *k;
				Class* ct = dynamic_cast<Class*>(st);
				if (string(ct->getName()) == c->getInhertedFrom())
				{
					foundBase = true;
					if (ct->isFinal)
					{ 
						string errString = string(ct->getName()) + " is Final and can't be inherited";
						this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
					}
				}
				++k;
			}
			if (!foundBase)
			{
				string errString = string(c->getInhertedFrom()) + " is Not defined ";
				this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
			}
				
		}
	}
	
}
void MyParser::checkclassdeclerationname(){
	vector<Symbol*>::iterator j;
	
	for (j = this->symbolsallclass.begin(); j != this->symbolsallclass.end(); ++j)
	{
		Symbol* ss = *j;
		Class* c = dynamic_cast<Class*>(ss);
		int tskeg = 0;
		//foreache element
		bool found = false;
		vector<Symbol*>::iterator k = symbolsallclass.begin();
		while (k != this->symbolsallclass.end() && !found)
		{
			Symbol* st = *k;
			Class* ct = dynamic_cast<Class*>(st);
			string one =string( st->getName());
			string two = string( ss->getName());
			if (one == two)
			{
			  tskeg++;
				if (tskeg > 1)
				{
					found = true;
					string errString = string(ct->getName()) + " class name is repeted ";
					this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");

				}
			}
			++k;
		}
	
		
	}


}
void MyParser::check_ring_inher()
{
vector<Symbol*>::iterator j;
for (j = this->symbolsallclass.begin(); j != this->symbolsallclass.end(); ++j)
{

	Symbol* ss = *j;
	Class* c = dynamic_cast<Class*>(ss);
	//for each class element
	string basename = string(c->getName());
	check_ring_inher_for_one_class(c, basename);
	
}

}
void MyParser::check_ring_inher_for_one_class(Class* current ,string basename){
	if (current->getInhertedFrom() == "Object")
	{
		return;
	}
	if (current->getInhertedFrom() == basename)
	{
		string errString = string("Class: "+current->getInhertedFrom()) + " part of Cirecel inheretance";
		this->errRecovery->errQ->enqueue(current->getLineNo(), current->getColNo(), errString.c_str(), "");
		return;
	}
	if ((current->getInhertedFrom() != "Object") && (current->getInhertedFrom()!= basename))
	{

		bool foundBase = false;
		vector<Symbol*>::iterator k = this->symbolsallclasscomp.begin();
		while (k != this->symbolsallclasscomp.end() && !foundBase)
		{
			Symbol* st = *k;
			Class* ct = dynamic_cast<Class*>(st);

			if (string(ct->getName()) == current->getInhertedFrom())
			{
				foundBase = true;
				check_ring_inher_for_one_class( ct,  basename);
				

			}
			++k;
		}


	}

}
//inner class error
void MyParser::checkInnerClasses(){
	
	vector<Symbol*>::iterator j;
	for (j = this->symbolsallclass.begin(); j != this->symbolsallclass.end(); ++j)
	{
		Symbol* ss = *j;
		Class* c = dynamic_cast<Class*>(ss);
		if (c->getOuterClass() != nullptr){
			this->checkNamingOfInners(c);
		  //	this->checkInherOfInners(c);
			this->travelallscopeforinner(this->getRootScope(), c->getBodyScope()->getParentScope());
			this->checkInherOfInnerscomp(c);

		}
	}
}
void MyParser::checkNamingOfInners(Class* inner){
	if (inner->getOuterClass() != nullptr){
		Class* outersWalker = inner->getOuterClass();
		while (outersWalker != nullptr){
			string one = string(outersWalker->getName());
			string two = string(inner->getName());
			if (one == two){
				string errString = string(inner->getName()) + " can't have the same name of outer " + outersWalker->getName();
				this->errRecovery->errQ->enqueue(inner->getLineNo(), inner->getColNo(), errString.c_str(), "");
			}
			if (outersWalker->getOuterClass() != nullptr)
				outersWalker = outersWalker->getOuterClass();
			else
				outersWalker = nullptr;
		}
	}
}
void MyParser::checkInherOfInners(Class* inner)
{
	if (inner->getOuterClass() != nullptr)
	{
		Class* outersWalker = inner->getOuterClass();
		while (outersWalker != nullptr)
		{
			string one = outersWalker->getInhertedFrom();
			string two = string(inner->getName());
			if (one == two)
			{
				string errString = string(outersWalker->getName()) + "outer can't inhereted from inner " + inner->getName();
				this->errRecovery->errQ->enqueue(outersWalker->getLineNo(), outersWalker->getColNo(), errString.c_str(), "");
			}
			if (outersWalker->getOuterClass() != nullptr)
				outersWalker = outersWalker->getOuterClass();
			else
				outersWalker = nullptr;
		}
	}
}

//constructor decleration
Symbol* MyParser::checksetconstructer(Class* classSymbol){// in yacc
	
	vector<Symbol*> symbols = classSymbol->getBodyScope()->getSymbolTable()->symbols();
	vector<Symbol*>::iterator i;
	bool foundconstr = false;
	bool infoundconstr = false;

	for (i = symbols.begin(); i != symbols.end(); ++i)
	{
		Symbol* s = *i;
		int sType = s->getSymbolType();
		if (sType == FUNCTION)
		{
			Function* c = dynamic_cast<Function*>(s);
			if (string(c->getName()) == string(classSymbol->getName()))
			{
				if (c->isconstruct())
				{
					foundconstr = true;
				}
			}
			if (string(c->getName()) != string(classSymbol->getName()))
			{
				if (c->isconstruct())
				{
					infoundconstr = true;
				}
			}

		}
	}
	if (!foundconstr)
	{
			int * temp = new int[1];
			temp[0] = PUBLICT;
			Function* constructor = new Function(classSymbol->getName(), classSymbol->getColNo(), classSymbol->getLineNo(), temp, 1);
			insertSymbol(constructor, classSymbol->getBodyScope());
	}
	if (infoundconstr){
		string errString = string(classSymbol->getName()) + "constructar shoud be same name of class :" + classSymbol->getName();
		this->errRecovery->errQ->enqueue(classSymbol->getLineNo(), classSymbol->getColNo(), errString.c_str(), "");
	}
	return classSymbol;
	//if (classSymbol->getBodyScope()->getSymbolTable()->lookup(classSymbol->getName()) == nullptr)
	//{ 
	//	int * temp = new int[1];
	//	temp[0] = PUBLICT;
	//	Function* constructor = new Function(classSymbol->getName(), classSymbol->getColNo(), classSymbol->getLineNo(), temp, 1);
	//	insertSymbol(constructor, classSymbol->getBodyScope());
	//}


	//return classSymbol;
}
void MyParser::constructerror(){
	vector<Symbol*>::iterator j;

	for (j = this->symbolsallclass.begin(); j != this->symbolsallclass.end(); ++j)
	{
		Symbol* ss = *j;
		Class* c = dynamic_cast<Class*>(ss);
		vector<Symbol*> symbols = c->getBodyScope()->getSymbolTable()->symbols();
		vector<Symbol*>::iterator i;
		for (i = symbols.begin(); i != symbols.end(); ++i)
		{
			Symbol* s = *i;
			int sType = s->getSymbolType();
			if (sType == FUNCTION)
			{
				Function* f = dynamic_cast<Function*>(s);
				if (f->isconstruct())
				{
					if (string(f->getName()) !=string( c->getName()))
					{
						string errString = string(f->getName()) + "constructar shoud be same name of class :" + c->getName();
						this->errRecovery->errQ->enqueue(f->getLineNo(), f->getColNo(), errString.c_str(), "");
					}
				}

			}

		}


	}
}
//modifier check
void MyParser::modifiersget(){
	vector<Symbol*>::iterator j;
	for (j = this->symbolsallsymbols.begin(); j != this->symbolsallsymbols.end(); ++j)
	{

		Symbol* ss = *j;
		int sType = ss->getSymbolType();
		if (sType == CLASS)
		{
			Class* c = dynamic_cast<Class*>(ss);
			int* n = c->getAccessModifier();
			int count = c->getAccessModifiercount();
			this->modifierscheck(n, count,c);
		}
		if (sType == FUNCTION)
		{
			Function* c = dynamic_cast<Function*>(ss);
			int* n = c->getAccessModifier();
			int count = c->getAccessModifiercount();
			this->modifierscheck(n, count,c);
		}
		if (sType == DATA_MEMBER)
		{
			DataMember* c = dynamic_cast<DataMember*>(ss);
			int* n = c->getAccessModifier();
			int count = c->getAccessModifiercount();
			this->modifierscheck(n, count,c);
		}
		
	}
}
void MyParser::modifierscheck(int*n ,int count,Class*c)
{//6,7,8
//same value
	string accessModifier;
	for (int i = 0; i < count; i++){
		int modi = n[i];
		for (int j = i+1 ; j < count; j++){
			int modi2 = n[j];
			if (modi == modi2)
			{
				
				switch (modi)
				{
				case ABSTRACTT:     accessModifier += "ABSTRAC "; break;
				case EXTERNT:     accessModifier += "EXTERN"; break;
				case INTERNALT:     accessModifier += "INTERNAl "; break;
				case NEWT:     accessModifier += "NEW "; break;
				case OVERRIDET:     accessModifier += "OVERRIDE "; break;
				case PRIVATET:     accessModifier += "PRIVATE"; break;
				case PROTECTEDT:     accessModifier += "PROTECTED"; break;
				case PUBLICT:     accessModifier += "PUBLIC "; break;
				case READONLYT:     accessModifier += "READONLY "; break;
				case SEALEDT:     accessModifier += "SEALED "; break;
				case STATICT:     accessModifier += "STATIC "; break;
				case UNSAFET:     accessModifier += "UNSAFE "; break;
				case VIRTUALT:     accessModifier += "VIRTUAL "; break;
				case VOLATILET:     accessModifier += "VOLATILE "; break;
				}
				string errString = accessModifier+ " Unexpetted repeted acces modifier" ;
				this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
			}	

		}		
	}
	int countcheck = 0;
	for (int i = 0; i < count; i++)
	{
		int modi = n[i];
		if ((modi == 6) | (modi == 7) | (modi == 8))
		{
			countcheck++;
		}
	}
	if (countcheck > 1)
	{
			string errString = " Unexpected more than one protection acces modifier ";
			this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
	}
}
void MyParser::modifierscheck(int*n, int count, Function*c)
{//6,7,8
	//same value
	string accessModifier;
	for (int i = 0; i < count; i++){
		int modi = n[i];
		for (int j = i + 1; j < count; j++){
			int modi2 = n[j];
			if (modi == modi2)
			{

				switch (modi){
				case ABSTRACTT:     accessModifier += "ABSTRACT "; break;
				case EXTERNT:     accessModifier += "EXTERNT "; break;
				case INTERNALT:     accessModifier += "INTERNALT "; break;
				case NEWT:     accessModifier += "NEWT "; break;
				case OVERRIDET:     accessModifier += "OVERRIDET "; break;
				case PRIVATET:     accessModifier += "PRIVATET"; break;
				case PROTECTEDT:     accessModifier += "PROTECTEDT"; break;
				case PUBLICT:     accessModifier += "PUBLICT "; break;
				case READONLYT:     accessModifier += "READONLYT "; break;
				case SEALEDT:     accessModifier += "SEALEDT "; break;
				case STATICT:     accessModifier += "STATICT "; break;
				case UNSAFET:     accessModifier += "UNSAFET "; break;
				case VIRTUALT:     accessModifier += "VIRTUALT "; break;
				case VOLATILET:     accessModifier += "VOLATILET "; break;
				}
				string errString = accessModifier + " acces modifier is repeteed";
				this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
			}
		}

	}
	int countcheck = 0;
	for (int i = 0; i < count; i++)
	{
		int modi = n[i];
		if ((modi == 6) | (modi == 7) | (modi == 8))
		{
			countcheck++;
		}
	}
	if (countcheck > 1)
	{
		string errString = " more than one protection modifier (public - private - protected)";
		this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
	}

}
void MyParser::modifierscheck(int*n, int count, DataMember*c)
{//6,7,8
	//same value
	string accessModifier;
	for (int i = 0; i < count; i++){
		int modi = n[i];
		for (int j = i + 1; j < count; j++){
			int modi2 = n[j];
			if (modi == modi2)
			{

				switch (modi){
				case ABSTRACTT:     accessModifier += "ABSTRACT "; break;
				case EXTERNT:     accessModifier += "EXTERNT "; break;
				case INTERNALT:     accessModifier += "INTERNALT "; break;
				case NEWT:     accessModifier += "NEWT "; break;
				case OVERRIDET:     accessModifier += "OVERRIDET "; break;
				case PRIVATET:     accessModifier += "PRIVATET"; break;
				case PROTECTEDT:     accessModifier += "PROTECTEDT"; break;
				case PUBLICT:     accessModifier += "PUBLICT "; break;
				case READONLYT:     accessModifier += "READONLYT "; break;
				case SEALEDT:     accessModifier += "SEALEDT "; break;
				case STATICT:     accessModifier += "STATICT "; break;
				case UNSAFET:     accessModifier += "UNSAFET "; break;
				case VIRTUALT:     accessModifier += "VIRTUALT "; break;
				case VOLATILET:     accessModifier += "VOLATILET "; break;
				}
				string errString = accessModifier + " acces modifier is repeteed";
				this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
			}
		}

	}

	int countcheck = 0;
	for (int i = 0; i < count; i++)
	{
		int modi = n[i];
		if ((modi == 6) | (modi == 7) | (modi == 8))
		{
			countcheck++;
		}
	}
	if (countcheck > 1)
	{
		string errString = " more than one protection modifier (public - private - protected)";
		this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
	}

}
//parametars check
void MyParser::checkparametars(){
	vector<Symbol*>::iterator j;

	for (j = this->symbolsallfunction.begin(); j != this->symbolsallfunction.end(); ++j)
	{
		Symbol* ss = *j;
		Function* c = dynamic_cast<Function*>(ss);
		Symbol* params= c->getParams();
		if (params){
			Symbol* walker = params;
			while (walker != nullptr){
				//code
			    int	tskeg = 0;
				bool found = false;
				string name = string(walker->getName());
				Symbol* walker2 = params;
				while (walker2 != nullptr && !found)
				{
				string name2 = string(walker2->getName());
				if (name == name2)
				{
					tskeg++;
					if (tskeg > 1)
					{
						found = true;
						string errString = name2 + " parametar is repeted";
						this->errRecovery->errQ->enqueue(walker->getLineNo(), walker->getColNo(), errString.c_str(), "");

					}
				}
				walker2 = walker2->node;
				}
				walker = walker->node;
			}
		}
	}
}
//final methods check
void MyParser::checkfinaloverridingmethods()
{
	vector<Symbol*>::iterator j;

	for (j = this->symbolsallfunction.begin(); j != this->symbolsallfunction.end(); ++j)
	{
		Symbol* s = *j;
		Function* c = dynamic_cast<Function*>(s);
		if (c->isfinal())
		{
			if ((c->getBodyScope()->getParentScope()->getOwnerSymbol()->getSymbolType()) == CLASS)
			{
				string class_name = string(c->getBodyScope()->getParentScope()->getOwnerSymbol()->getName());
				vector<Symbol*>::iterator k;
				for (k = this->symbolsallclass.begin(); k != this->symbolsallclass.end(); ++k)
				{

					Symbol* ss = *k;
					Class* cc = dynamic_cast<Class*>(ss);
					if (cc->getInhertedFrom() != "Object")
					{
						if (cc->getInhertedFrom() == class_name)
						{
							Scope* myscope = cc->getBodyScope();    //fe methods same of my final?
							vector<Symbol*> allfunctionsinscope;
							vector<Symbol*> symbols = myscope->getSymbolTable()->symbols();
							vector<Symbol*>::iterator i;
							for (i = symbols.begin(); i != symbols.end(); ++i)
							{
								Symbol* sss = *i;
								int sType = sss->getSymbolType();
								if ((sType == FUNCTION) && string(sss->getName()) == string(c->getName()))
								{
									string errString = string(sss->getName()) + " method is final and can't be overriden by chield of class";
									this->errRecovery->errQ->enqueue(sss->getLineNo(), sss->getColNo(), errString.c_str(), "");
								}

							}


						}
					}

				}
			}
		}
	}
}
void MyParser::checkmainmethod()
{
	int maincount = 0;
	int line=0;
	int col = 0;
	vector<Symbol*>::iterator j;
	for (j = this->symbolsallfunction.begin(); j != this->symbolsallfunction.end(); ++j)
	{
		Symbol* ss = *j;
		Function* c = dynamic_cast<Function*>(ss);
		
		if (string(c->getorignalName()) == "main" )
		{ 
			bool isstatic = false;
			int *acces = c->getAccessModifier();
			int count = c->getAccessModifiercount();
			for (int i = 0; i < count; i++)
			{
				int modi = acces[i];
				if (modi == STATICT)
				{	
					isstatic = true;
				}
			}
			if (isstatic)
			{
				if (c->getBodyScope() != nullptr){
				if (c->getBodyScope()->getParentScope()->getOwnerSymbol()->getSymbolType() == CLASS)
				{
					Symbol*z = c->getBodyScope()->getParentScope()->getOwnerSymbol();
					Class* zz = dynamic_cast<Class*>(z);
					bool ispublic = false;
					int *acces = zz->getAccessModifier();
					int count = zz->getAccessModifiercount();
					for (int i = 0; i < count; i++)
					{
						int modi = acces[i];
						if (modi == PUBLICT)
						{
							ispublic = true;
						}
					}
					if (ispublic)
					{
						maincount++;
						line = c->getLineNo();
						col = c->getColNo();
					}
				}
			       }//mmmm

			}
		}
	
	}
	if (maincount >1  )
	{
		string errString = " more than one static main in public class in programm";
		this->errRecovery->errQ->enqueue(line, col, errString.c_str(), "");
	}
	if (maincount == 0)
	{
	this->errRecovery->errQ->enqueue(line, col, "No enter point(main )to programm", "");
	}
}
void MyParser::checkoverloadmethod()
{
	vector<Symbol*>::iterator j;
	for (j = this->symbolsallclass.begin(); j != this->symbolsallclass.end(); ++j)
	{

		Symbol* ss = *j;
		Class* c = dynamic_cast<Class*>(ss);
		vector<Symbol*> symbols = c->getBodyScope()->getSymbolTable()->symbols();
		vector<Symbol*>::iterator i;
		for (i = symbols.begin(); i != symbols.end(); ++i)
		{
			Symbol* s = *i;
			int sType = s->getSymbolType();
			if (sType == FUNCTION)
			{
				Function* f = dynamic_cast<Function*>(s);
				vector<Symbol*>::iterator j;
				for (j = symbols.begin(); j != symbols.end(); ++j)
				{
					Symbol* sss = *j;
					int sType = sss->getSymbolType();
					if (sType == FUNCTION)
					{
						Function* f2 = dynamic_cast<Function*>(sss);
						if (string(f->getorignalName()) == string(f2->getorignalName()))
						{
							if (f->getColNo() != f2->getColNo())
							{
								//////////////////////////////
								Symbol * first = f->getParams();
								Symbol *sec = f2->getParams();
								int numoffirst = 0;
								int numofsec = 0;
								Symbol* walker = first;
								while (walker != nullptr)
								{
									numoffirst++;
									walker = walker->node;
								}
								Symbol* walker2 = sec;
								while (walker2 != nullptr)
								{
									numofsec++;
									walker2 = walker2->node;
								}
								if (numoffirst != numofsec)
								{
									f->setasoverloaded();
									f2->setasoverloaded();

								}
								else
								{
									Symbol* walker3 = first;
									Symbol* walker4 = sec;
									bool overload = false;
									for (int k = 0; k < numoffirst; k++)
									{
										Parameter* x3 = dynamic_cast<Parameter*>(walker3);
										Parameter* x4 = dynamic_cast<Parameter*>(walker4);
										if (x3->getVariableType() != x4->getVariableType())
										{
											overload = true;
										}
										walker3 = walker3->node;
										walker4 = walker4->node;
									}
									if (overload)
									{
										f->setasoverloaded();
										f2->setasoverloaded();
									}
									else
									{
										if (f->getReturnType() == f2->getReturnType())
										{
											string errString = " method redefination";
											this->errRecovery->errQ->enqueue(f2->getLineNo(), f2->getColNo(), errString.c_str(), "");
										}
										else
										{
											string errString = " can't overload by return type only!";
											this->errRecovery->errQ->enqueue(f2->getLineNo(), f2->getColNo(), errString.c_str(), "");
										}
									}

								}
							}
						}
					}
				}
			}
		}
	}
}
				
void MyParser::checkabstructclass()
{
	vector<Symbol*>::iterator i;
	for (i = this->symbolsallclass.begin(); i != this->symbolsallclass.end(); ++i)
	{

		Symbol* s = *i;
		Class* c = dynamic_cast<Class*>(s);
		int *modif = c->getAccessModifier();
		int modifcount = c->getAccessModifiercount();
		bool isabstruct = false;
		for (int j = 0; j < modifcount; j++)
		{
			int modi = modif[j];
			if (modi == ABSTRACTT)
			{
				isabstruct = true;
			}
		}
		if (isabstruct)
		{
			
			this->symbolsallfunctionforabstruct.clear();
			vector<Symbol*> symbols = c->getBodyScope()->getSymbolTable()->symbols();
			vector<Symbol*>::iterator j;
			for (j = symbols.begin(); j != symbols.end(); ++j)
			{
				Symbol* ss = *j;
				int sType = ss->getSymbolType();
				if (sType == FUNCTION)
				{
					Function* f = dynamic_cast<Function*>(ss);
					if (!f->isconstruct())
					this->symbolsallfunctionforabstruct.push_back(ss);
				}
			}		
			vector<Symbol*>::iterator k;
			for (k = this->symbolsallfunctionforabstruct.begin(); k != this->symbolsallfunctionforabstruct.end(); ++k)
			{
				Symbol* sss = *k;
				Function* ccc = dynamic_cast<Function*>(sss);
				if (ccc->getBodyScope() != nullptr)
				{
					string errString = " not allwod to make body scope";
					this->errRecovery->errQ->enqueue(ccc->getLineNo(), ccc->getColNo(), errString.c_str(), "");
				}
				int *modiff = ccc->getAccessModifier();
				int modiffcount = ccc->getAccessModifiercount();
				bool ispublic = false;
				for (int ii = 0; ii < modifcount; ii++)
				{
					int modi = modiff[ii];
					if (modi == PUBLICT)
					{
						ispublic = true;
					}
				}			
				if (!ispublic)
				{
					string errString = " should be public ";
					this->errRecovery->errQ->enqueue(ccc->getLineNo(), ccc->getColNo(), errString.c_str(), "");
				}

				bool isabstructfun = false;
				for (int ii = 0; ii < modifcount; ii++)
				{
					int modi = modiff[ii];
					if (modi == ABSTRACTT)
					{
						isabstructfun = true;
					}
				}
				if (!isabstructfun)
				{
					string errString = " should be abstract";
					this->errRecovery->errQ->enqueue(ccc->getLineNo(), ccc->getColNo(), errString.c_str(), "");
				}
			}
		}
		vector<Symbol*>::iterator l;
		for (l = this->symbolsallclass.begin(); l != this->symbolsallclass.end(); ++l)
		{
			Symbol* x = *l;
			Class* z = dynamic_cast<Class*>(x);
			if (z->getInhertedFrom() == string(c->getName()))
			{
				vector<Symbol*> symbols = z->getBodyScope()->getSymbolTable()->symbols();
				vector<Symbol*>::iterator m;
				for (m = this->symbolsallfunctionforabstruct.begin(); m != this->symbolsallfunctionforabstruct.end(); ++m)
				{
					Symbol* fj = *m;
						Function* funa = dynamic_cast<Function*>(fj) ;
					vector<Symbol*>::iterator n;
					bool found = false;
					bool overrided = false;
					for (n = symbols.begin(); n != symbols.end(); ++n)
					{

						Symbol* ffj = *n;
						
						int sType = ffj->getSymbolType();
						if (sType == FUNCTION)
						{
							Function* funb = dynamic_cast<Function*>(ffj);
							if (string(funa->getName()) == string(funb->getName()))
							{
								found = true;
								int *modiff = funb->getAccessModifier();
								int modiffcount = funb->getAccessModifiercount();
								for (int ii = 0; ii < modifcount; ii++)
								{
									int modi = modiff[ii];
									if (modi == OVERRIDET)
									{
										overrided = true;
									}
								}
							}
							if (found &&!overrided)
							{
								string errString = " expected an override keyword";
								this->errRecovery->errQ->enqueue(funb->getLineNo(), funb->getColNo(), errString.c_str(), "");
							}
						}

					}

					if (!found)
					{
						string errString = " not implement all method";
						this->errRecovery->errQ->enqueue(funa->getLineNo(), funa->getColNo(), errString.c_str(), "");
					}
			

				}
			
			}
		}
	}

}

void MyParser::checkInherOfInnerscomp(Class* inner)
{
	    string innername = string(inner->getName());
		vector<Symbol*>::iterator j;
		for (j = this->symbolsallclassforinner.begin(); j != this->symbolsallclassforinner.end(); ++j)
		{
			Symbol* ss = *j;
			Class* c = dynamic_cast<Class*>(ss);
			string one = c->getInhertedFrom();
			if (one == innername)
			{
				if (c->getOuterClass() == nullptr)
				{
					string errString = string(c->getName()) + "outer can't inhereted from inner " + inner->getName();
			     	this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), "");
				}
				else
				{
					if( (c->getOuterClass()->getInhertedFrom() )!= (string(inner->getOuterClass()->getName())))
					{
						string errString = string(c->getName()) + " outer can't inhereted from inner" + inner->getName() +"\n";
						string errString2 = "you can only if " + string(c->getOuterClass()->getName()) + " inhertad from " + string(inner->getOuterClass()->getName());
						this->errRecovery->errQ->enqueue(c->getLineNo(), c->getColNo(), errString.c_str(), errString2.c_str());
					}
				}
			}
	
		}
		symbolsallclassforinner.clear();
}
void MyParser::addclasstovectorinner(Scope * scope)
{
	if (scope == nullptr)
		return;
	vector<Symbol*> symbols = scope->getSymbolTable()->symbols();
	vector<Symbol*>::iterator i;
	for (i = symbols.begin(); i != symbols.end(); ++i)
	{
		Symbol* s = *i;
		int sType = s->getSymbolType();
		if (sType == CLASS)
		{
			this->symbolsallclassforinner.push_back(s);
		}

	}
}
void MyParser::travelallscopeforinner(Scope * scope, Scope* block){
	if (scope == nullptr)
		return;
	if (scope->GetScopeId() == block->GetScopeId())
	{   //return in begin
		if (scope->getNextScope() != nullptr)
			scope = scope->getNextScope();
		else
			return;
	}
	addclasstovectorinner(scope);

	Scope* innersWalker = scope->getInnerScope();
	if (innersWalker == nullptr)
		return;
	if (innersWalker->GetScopeId() == block->GetScopeId())
		innersWalker = innersWalker->getNextScope();
	while (innersWalker != nullptr){
		travelallscopeforinner(innersWalker,block);
		innersWalker = innersWalker->getNextScope();
	}
}

void MyParser::onepubliclasscheck(Scope* root)
{
	vector<Symbol*> symbols = root->getSymbolTable()->symbols();
	vector<Symbol*>::iterator i;
	bool classfound = false;
	int mycount = 0;
	for (i = symbols.begin(); i != symbols.end(); ++i)
	{
		Symbol* s = *i;
		int sType = s->getSymbolType();
		if (sType == CLASS)
		{
			classfound = true;
			Class* c = dynamic_cast<Class*>(s);
			int *modif = c->getAccessModifier();
			int modifcount = c->getAccessModifiercount();
			bool ispublic = false;
			for (int j = 0; j < modifcount; j++)
			{
				int modi = modif[j];
				if (modi == PUBLICT)
				{
					ispublic = true;
					mycount++;
				}
			}
			
		}
	}
	if (mycount>1)
	{
	 this->errRecovery->errQ->enqueue(0, 0, "more than one public class in input file ", "");
	}
	if (!classfound)
	{
		Scope* myinner = root->getInnerScope();
		vector<Symbol*> symbols = myinner->getSymbolTable()->symbols();
		vector<Symbol*>::iterator j;
		bool classfound = false;
		for (j = symbols.begin(); j != symbols.end(); ++j)
		{
			Symbol* s = *j;
			int sType = s->getSymbolType();
			if (sType == CLASS)
			{
				classfound = true;
				Class* c = dynamic_cast<Class*>(s);
				int *modif = c->getAccessModifier();
				int modifcount = c->getAccessModifiercount();
				bool ispublic = false;
				for (int j = 0; j < modifcount; j++)
				{
					int modi = modif[j];
					if (modi == PUBLICT)
					{
						ispublic = true;
						mycount++;
					}
				}
			}
		}
		if (mycount>1)
		{
			this->errRecovery->errQ->enqueue(0, 0, "more than one public class in input file ", "");
		}

	}
	}
	
	/*
	vector<Symbol*>::iterator j;
	int mycount = 0;
	for (j = this->symbolsallclass.begin(); j != this->symbolsallclass.end(); ++j)
	{
		Symbol* ss = *j;
		Class* c = dynamic_cast<Class*>(ss);
			int *modif = c->getAccessModifier();
			int modifcount = c->getAccessModifiercount();
			bool ispublic = false;
			for (int j = 0; j < modifcount; j++)
			{
				int modi = modif[j];
				if (modi == PUBLICT)
				{
					mycount++;
				}
			}
	}
	if (mycount>1)
	{
		this->errRecovery->errQ->enqueue(0,0, "more than one public class in input file ", "");
	}*/
