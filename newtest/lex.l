%{
   #include <iostream>
	#include <istream>
	#include <ostream>	
	#include <FlexLexer.h>	
    #include <string.h>
	#include <stdio.h>
    #include <stdlib.h>
	#include <string>
    #include "yacc.hpp"
	#include "ErrorRecovery.h"

	 #include <queue>
  #include <stack>
  void lexical_error(const char *);
  static int token_for(const char *);
  static int is_allowed_char(const char );
  int colNo=1;
  int lineNo =1;
  int invalidlineno = 0;
  char buf[100];
    char *s;
	int commentline =0;
	int commentcol =0;
	int chackbracet = 0;
	int chackbracetx = 0;
  extern ErrorRecovery errorRec;
  struct bracket {
    char br;
    int lineNo;
    int colNo;
	int mm;
	int valid;
  };
  stack<bracket> brackets;
  stack<bracket> bracketsn;
  stack<bracket> bracketsa;
  void return_to_yacc(char *yytext){
    yylval.r.myColno = colNo;
    yylval.r.myLineNo = lineNo;
    colNo += strlen(yytext);
          }

  int count_underscores(string s,char v) {
  int count = 0;

  for (int i = 0; i < s.size(); i++)
    if (s[i] == v) count++;

  return count;
                                   }
%}

%option c++
%option noyywrap    
%option yylineno
%x STRING
single_line_comment    "//".*

esc_seq_nn             [\\]*
white_space             [ \t]
newline                 [ \n\r]
preprocessor           ^[ \t]*#.*

dec_digit              [0-9]
hex_digit              [0-9A-Fa-f]
int_suffix             [UuLl]|[Uu][Ll]|[Ll][Uu]
dec_literal            {dec_digit}+{int_suffix}?
hex_literal            0[xX]{hex_digit}+{int_suffix}?
integer_literal        {dec_literal}|{hex_literal}

real_suffix            [FfDdMm]
sign                   [+\-]
exponent_part          [eE]{sign}?{dec_digit}+
whole_real1            {dec_digit}+{exponent_part}{real_suffix}?
whole_real2            {dec_digit}+{real_suffix}
part_real              {dec_digit}*\.{dec_digit}+{exponent_part}?{real_suffix}?
real_literal           {whole_real1}|{whole_real2}|{part_real}

single_char            [^\\\']
simple_esc_seq         \\[\'\"\\0abfnrtv]
uni_esc_seq1           \\u{hex_digit}{4}
uni_esc_seq2           \\U{hex_digit}{8}
uni_esc_seq            {uni_esc_seq1}|{uni_esc_seq2}
hex_esc_seq            \\x{hex_digit}{1,4}
character              {single_char}|{simple_esc_seq}|{hex_esc_seq}|{uni_esc_seq}
character_literal      \'{character}\'


single_string_char     [^\\\"]|{esc_seq_nn}
reg_string_char        {single_string_char}|{simple_esc_seq}|{hex_esc_seq}|{uni_esc_seq}
regular_string         \"{reg_string_char}*\"
single_verbatim_char   [^\"]
quote_esc_seq          \"\"
verb_string_char       {single_verbatim_char}|{quote_esc_seq}
verbatim_string        @\"{verb_string_char}*\"
string_literal         {regular_string}|{verbatim_string}
invalid_string        \"{reg_string_char}*
openblm                [\{]+
openblmm               {openblm}({white_space}*{newline}*)
openbl                 {openblmm}+

opennom                [\(]+
opennomm               {opennom}({white_space}*{newline}*)
openno                 {opennomm}+
letter_char            [A-Za-z]
ident_char             {dec_digit}|{letter_char}|"_"|"@"
identifier             ({letter_char}|"_"){ident_char}*
at_identifier          \@{identifier}
invalid_identifier     {dec_digit}+{identifier}*

rank_specifier         "["{white_space}* ( ","{white_space}* )* "]"


%x IN_COMMENT


%%

{preprocessor}        { ; /* ignore */ }
{white_space}         {colNo ++ ; /* ignore */ }
{newline}             {lineNo ++ ;colNo=1;}

"/*"                  { BEGIN IN_COMMENT;commentline=lineNo;commentcol=colNo;colNo+=2; }
<IN_COMMENT>.         { colNo += strlen(yytext); }
<IN_COMMENT>\n        {lineNo++ ;invalidlineno ++;colNo=1; }
<IN_COMMENT>"*/"      { BEGIN INITIAL;colNo+=2; cout<<"end of multy line comme"; }
<IN_COMMENT><<EOF>>   { errorRec.errQ->enqueue(commentline, commentcol, "end of file found */ expected unclosed multi-line comment", "");
                        errorRec.printErrQueue(); yyterminate(); }



{single_line_comment} {return_to_yacc(yytext) ; /* ignore */ }
                    
{integer_literal}     {return_to_yacc(yytext); yylval.r.i = atoi(yytext); return INTEGER_LITERAL; }
{real_literal}        {return_to_yacc(yytext); yylval.r.f = atof(yytext); return REAL_LITERAL; }
{character_literal}   {return_to_yacc(yytext); yylval.r.c = yytext[0]; return CHARACTER_LITERAL; }
{string_literal}      { 
						commentline=lineNo;commentcol=colNo;
						return_to_yacc(yytext);			
						string strn(yytext) ;		
						strn.erase(0,1);
						strn.erase(strn.size()-1,1);
						
                        int position =strn.find("\\t");
						 while (position != string::npos ) 
						 {
						 strn.replace(position ,2, "\\\t");
						 strn.erase(position ,1);
						  position =strn.find("\\t",position+2); 
						 } 
						 					 
                        int pos =strn.find("\\n");
						 while (pos != string::npos ) 
						 {
						 strn.replace(pos ,2, "\\\n");
						  strn.erase(pos ,1);
						  pos =strn.find("\\n",pos+2); 
						 } 

						    pos =strn.find("\\\"");
						   
						 while (pos != string::npos ) 
						 {
						 strn.replace(pos ,2, "\"");
						  pos =strn.find("\\\"",pos+2); 
						 } 						 
						  
						  pos =strn.find("\\\'");  
						 while (pos != string::npos ) 
						 {
						 strn.replace(pos ,2, "\'");
						  pos =strn.find("\\\'",pos+2); 
						 } 						  					 
                    
						 
						  pos =strn.find("\\");
						   while (pos != string::npos ) 
						 {
							if (strn[pos+1]!='\\'){
							  errorRec.errQ->enqueue(commentline,commentcol,"Unrecognized escape secuance in string", "");
                              errorRec.printErrQueue();
							                       }
				            else{
						    strn.erase(pos+1,1);
							}
						    pos =strn.find("\\",pos+1); 
						 }

						  const char *cstr = strn.c_str();
					     yylval.r.str = strdup(cstr);
						 return STRING_LITERAL ;
					
                      }  
{invalid_string} {
  errorRec.errQ->enqueue(lineNo, colNo, "unexpected end of file: unclosed string", "");
  errorRec.printErrQueue();
  return STRING_LITERAL;
} 

                     
","   {return_to_yacc(yytext); return COMMA; }

{rank_specifier}     { return RANK_SPECIFIER; }

                  
"+="  {return_to_yacc(yytext); return PLUSEQ; }
"-="  {return_to_yacc(yytext); return MINUSEQ; }
"*="  {return_to_yacc(yytext); return STAREQ; }
"/="  {return_to_yacc(yytext); return DIVEQ; }
"%="  {return_to_yacc(yytext); return MODEQ; }
"^="  {return_to_yacc(yytext); return XOREQ; }
"&="  {return_to_yacc(yytext); return ANDEQ; }
"|="  {return_to_yacc(yytext); return OREQ; }
"<<"  {return_to_yacc(yytext); return LTLT; }
">>"  {return_to_yacc(yytext); return GTGT; }
">>="  {return_to_yacc(yytext); return GTGTEQ; }
"<<="  {return_to_yacc(yytext); return LTLTEQ; }
"=="  {return_to_yacc(yytext); return EQEQ; }
"!="  {return_to_yacc(yytext); return NOTEQ; }
"<="  {return_to_yacc(yytext); return LEQ; }
">="  {return_to_yacc(yytext); return GEQ; }
"&&"  {return_to_yacc(yytext); return ANDAND; }
"||"  {return_to_yacc(yytext); return OROR; }
"++"  {return_to_yacc(yytext); return PLUSPLUS; }
"--"  {return_to_yacc(yytext); return MINUSMINUS; }
"->"  {return_to_yacc(yytext); return ARROW; }


{invalid_identifier} {  errorRec.errQ->enqueue(lineNo, colNo, "identifier can't begin with number ", "");
                              errorRec.printErrQueue();
							   yylval.r.str = new char[yyleng + 1];
                               strcpy(yylval.r.str, yytext);
							  return IDENTIFIER;
							  }

{identifier}         {   return_to_yacc(yytext);
						 yylval.r.str = new char[yyleng + 1];
					     strcpy(yylval.r.str, yytext);
					     return token_for(yytext); 
					 }
{at_identifier}       {  return_to_yacc(yytext);
						  yylval.r.str = new char[yyleng + 1];
						 strcpy(yylval.r.str, yytext);
						 return IDENTIFIER;
					  }


 {openbl} {
    
    string strn(yytext) ;
    int i= count_underscores(strn ,'{');
	size_t n = std::count(strn.begin(), strn.end(), '\n');
    bracket b = {'{', lineNo, colNo,i,0};
	if(b.mm>1){
	 errorRec.errQ->enqueue(lineNo, colNo, "WARNING!!!!!!!!!!!! multy open block Unxpected", "");
     errorRec.printErrQueue();
	 }
    brackets.push(b);
	return_to_yacc(yytext);
	lineNo+=n;
    return '{';
  }

  "}" {
    return_to_yacc(yytext);
    if (!brackets.empty()) {
      bracket b = brackets.top();
      brackets.pop();
	  if( b.valid==1 && b.mm>1) {chackbracetx=1;b.mm--;brackets.push(b);}
	  if( b.valid==1 && b.mm==1){chackbracetx=1;b.mm--;}
	  if(b.mm>1 && b.valid==0)  {b.mm--;b.valid =1;brackets.push(b);}
	 
    } 
	else {
      errorRec.errQ->enqueue(lineNo, colNo, "brackets } whithout {", "");
      errorRec.printErrQueue();
	  chackbracet = 1;
    }
  	if(chackbracet==0 && chackbracetx==0 )
    return yytext[0];
	else{
    chackbracet = 0;chackbracetx=0;}
  }


  {openno} {
    
    string strn(yytext) ;
    int i= count_underscores(strn ,'(');
	size_t n = std::count(strn.begin(), strn.end(), '\n');
    bracket b = {'(', lineNo, colNo,i,0};
	if(b.mm>1){
	 errorRec.errQ->enqueue(lineNo, colNo, "WARNING!!!!!!!!!!!! multy open block Unxpected", "");
     errorRec.printErrQueue();
	 }
    bracketsn.push(b);
	return_to_yacc(yytext);
	lineNo+=n;
    return '(';
  }

  ")" {
    return_to_yacc(yytext);
    if (!bracketsn.empty()) {
      bracket b = bracketsn.top();
      bracketsn.pop();
	  if( b.valid==1 && b.mm>1) {chackbracetx=1;b.mm--;bracketsn.push(b);}
	  if( b.valid==1 && b.mm==1){chackbracetx=1;b.mm--;}
	  if(b.mm>1 && b.valid==0)  {b.mm--;b.valid =1;bracketsn.push(b);}
	 
    } 
	else {
      errorRec.errQ->enqueue(lineNo, colNo, "brackets ) whithout (", "");
      errorRec.printErrQueue();
	  chackbracet = 1;
    }
  	if(chackbracet==0 && chackbracetx==0 )
    return yytext[0];
	else{
    chackbracet = 0;chackbracetx=0;}
  }



 "[" {
    
    bracket b = {'{', lineNo, colNo,strlen(yytext),0};
    bracketsa.push(b);
    return_to_yacc(yytext);
    return yytext[0];
  }

  "]" {
    return_to_yacc(yytext);
    if (!bracketsa.empty()) {
      bracket b = bracketsa.top();
      bracketsa.pop();
   
    } 
	else {
      errorRec.errQ->enqueue(lineNo, colNo, "brackets ] whithout [", "");
      errorRec.printErrQueue();
	  chackbracet = 1;
    }
  	if(chackbracet==0 )
    return yytext[0];
	else{
    chackbracet = 0;}
  }



  <<EOF>> {                        while(!brackets.empty()) {
                                    bracket b = brackets.top();
                                    brackets.pop();
                                    char msg[128];
                                    sprintf(msg ,"Unexpected }:%dbracket %c at line %d column %d should be matched",b.mm,b.br ,b.lineNo, b.colNo);
								    errorRec.errQ->enqueue(b.lineNo, b.colNo, msg , "");
                                    errorRec.printErrQueue(); 
									}
									while(!bracketsn.empty()) {
                                    bracket b = bracketsn.top();
                                    bracketsn.pop();
                                   char msg[128];
                                    sprintf(msg ,"Unexpected }:%dbracket %c at line %d column %d should be matched",b.mm,b.br ,b.lineNo, b.colNo);
								    errorRec.errQ->enqueue(b.lineNo, b.colNo, msg , "");
                                    errorRec.printErrQueue();}

									while(!bracketsa.empty()) {
                                    bracket b = bracketsa.top();
                                    bracketsa.pop();
                                   char msg[128];
                                    sprintf(msg ,"Unexpected ]:bracket %c at line %d column %d should be matched",b.br ,b.lineNo, b.colNo);
								    errorRec.errQ->enqueue(b.lineNo, b.colNo, msg , "");
                                    errorRec.printErrQueue();}
									
									
									yyterminate(); }

.     { 
        if (is_allowed_char(yytext[0])) {return_to_yacc(yytext); return yytext[0] ;}
        else { errorRec.errQ->enqueue(lineNo,colNo, "lexical ERROR" , "");
                                    errorRec.printErrQueue();}
      }
%%


static struct name_value {
  char *name;
  int value;
} name_value;
static struct name_value keywords [] = { 
 /* This list must remain sorted!!! */
    {"abstract", ABSTRACT},
    {"as", AS},
    {"base", BASE},
    {"bool", BOOL},
    {"break", BREAK},
    {"byte", BYTE},
    {"case", CASE},
    {"catch", CATCH},
    {"char", CHAR},
    {"checked", CHECKED},
    {"class", CLASSX},
    {"const", CONST},
    {"continue", CONTINUE},
    {"decimal", DECIMAL},
    {"default", DEFAULTX},
    {"delegate", DELEGATE},
    {"do", DO},
    {"double", DOUBLE},
    {"else", ELSE},
    {"enum", ENUM},
    {"event", EVENT},
    {"explicit", EXPLICIT},
    {"extern", EXTERN},
    {"false", FALSE},
	{"final", FINAL},
    {"finally", FINALLY},
    {"fixed", FIXED},
    {"float", FLOAT},
    {"for", FOR},
    {"foreach", FOREACH},
    {"goto", GOTO},
    {"if", IF},
    {"implicit", IMPLICIT},
    {"in", IN},
    {"int", INT},
    {"interface", INTERFACEX},
    {"internal", INTERNAL},
    {"is", IS},
    {"lock", LOCK},
    {"long", LONG},
    {"namespace", NAMESPACE},
    {"new", NEW},
    {"null", NULL_LITERAL},
    {"object", OBJECTX},
    {"operator", OPERATOR},
    {"out", OUT},
    {"override", OVERRIDE},
    {"params", PARAMS},
    {"private", PRIVATE},
    {"protected", PROTECTED},
    {"public", PUBLIC},
    {"readonly", READONLY},
    {"ref", REF},
    {"return", RETURN},
    {"sbyte", SBYTE},
    {"sealed", SEALED},
    {"short", SHORT},
    {"sizeof", SIZEOF},
    {"stackalloc", STACKALLOC},
    {"static", STATIC},
    {"string", STRING},
    {"struct", STRUCT},
    {"switch", SWITCH},
    {"this", THIS},
    {"throw", THROW},
    {"true", TRUE},
    {"try", TRY},
    {"typeof", TYPEOF},
    {"uint", UINT},
    {"ulong", ULONG},
    {"unchecked", UNCHECKED},
    {"unsafe", UNSAFE},
    {"ushort", USHORT},
    {"using", USING},
    {"virtual", VIRTUAL},
    {"void", VOID},
    {"volatile", VOLATILE},
    {"while", WHILE},
  };    


static int bin_search(const char *lexeme, int start, int finish)
{
  if (start >= finish)  /* Not found */
      return IDENTIFIER;
  else {
    int mid = (start+finish)/2;
    int cmp = strcmp(lexeme,keywords[mid].name);
    if (cmp == 0) {
      return keywords[mid].value;
	
	  }
    else if (cmp < 0)
      return bin_search(lexeme,start,mid);
	  
    else
      return bin_search(lexeme,mid+1,finish);
	  
  }
}

static int token_for(const char *lexeme)
{
  static int num_keywords = sizeof(keywords) / sizeof(name_value);
  int token = bin_search(lexeme,0,num_keywords);
  if (token == -1){
    token = IDENTIFIER;
	}
  return token;
}

static int is_allowed_char(const char c)
{
  static char allowed [] = {
    '.', ';', ':', '<', '>',
    '+', '-', '*', '/', '%', '&', '|', '!', '~', '^',
    '=', '?',
  };
  static int num_allowed = sizeof(allowed) / sizeof(char);
  int i;
  for (i=0; i<num_allowed; i++) {
    if (allowed[i]==c) 
      return 1;

  }
  return 0;
}

void lexical_error(const char *msg)
{
  fprintf(stderr,":invalid token ");
}

