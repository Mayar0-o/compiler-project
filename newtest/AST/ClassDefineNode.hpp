#pragma once

#include "Node.hpp"
#include "ListNode.hpp"
#include "../SymbolTable.h"
#include "ClassMemNode.hpp"

class ClassDefineNode : public Node {
public:
	ListNode* body;
	Class* classSymbol;
	//TODO: think about inner classes!

	ClassDefineNode(Symbol* classSym, Node* body, int line, int col);


	virtual void print(ostream &os);

	

};