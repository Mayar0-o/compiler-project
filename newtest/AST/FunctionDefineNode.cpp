#pragma once
#include "FunctionDefineNode.hpp"


FunctionDefineNode::FunctionDefineNode(Symbol* func, Node* bod, Node* paramsList, int line, int col) {
    functionSym = dynamic_cast<Function*>(func);
    bodySts = dynamic_cast<ListNode*>(bod);
	this->paramsList = dynamic_cast<ListNode*>(paramsList);
	this->line = line;
	this->col = col;
  }

void FunctionDefineNode::print(ostream &os) {
    int self = int(this);
    os << self << "[\""
       << functionSym->getName()
       << "\"]"
	   << "\ncontain :\n"
       <<endl;
	os << "\t\t";
    os << "(body)->" << int(bodySts) << endl;
	os << "\t\t";
	if (paramsList) {
		os << "->" << int(paramsList) << endl;
		self = (int)paramsList;
		os << self << "[\"Params\"]" << endl;
		for (auto &node : paramsList->nodes) {
			if (node == nullptr) continue;
			os << self << "->" << (int)node << endl;
		}
		for (auto &node : paramsList->nodes) {
			if (node == nullptr) continue;
			node->print(os);
		}
	}
	if (bodySts) bodySts->print(os);

 }
