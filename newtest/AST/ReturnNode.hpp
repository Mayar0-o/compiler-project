#pragma once

#include <iostream>
#include "Node.hpp"
class ReturnNode : public Node {
public:
	Node* return_node;

	ReturnNode(Node *expression, int line, int col);

	virtual void print(ostream &os);

};
