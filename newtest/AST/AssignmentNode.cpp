#pragma once

#include "AssignmentNode.hpp"
#include "VariableNode.hpp"

AssignmentNode::AssignmentNode(Node *l, Node *r, int line, int col) : lhs(l), rhs(r) {
	this->line = line;
	this->col = col;
}

void AssignmentNode::print(ostream &os) {
    int self = int(this);
    os << self
       << "[\"=assigment\"]"
       << endl;
	os << "contain :\n" ;
	os << "\t\t";
	os <<"->" << int(lhs) << endl;
	os << "\t\t";
	os <<"->" << int(rhs) << endl;

	if (lhs)  lhs->print(os);
    if (rhs) rhs->print(os);
  
  }
  
