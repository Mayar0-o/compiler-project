#include "Node.hpp"

class BreakNode : public Node {
public:

	BreakNode(int line, int col);

	virtual void print(ostream &os);


};
