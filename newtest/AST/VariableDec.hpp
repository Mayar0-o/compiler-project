#pragma once
#include "Node.hpp"
#include <iostream>
#include "../SymbolTable.h"


class VariableDec: public Node{
public:
	Node *VariableNode;
  Variable *variable;
  VariableDec(Symbol *v, Node *VariableNode, int line, int col);
  int variableType;
  string variableName;

	virtual void print(ostream &os);

};
