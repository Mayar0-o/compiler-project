#pragma once 

#include "ClassDefineNode.hpp"

ClassDefineNode::ClassDefineNode(Symbol* classSym, Node* classBody, int line, int col) {
	this->classSymbol = dynamic_cast<Class*>(classSym);
	this->body =  dynamic_cast<ListNode*>(classBody);// new ListNode();  //	
	this->line = line;
	this->col = col;
}

void ClassDefineNode::print(ostream &os) {
	int self = int(this);
	string classLabel = "class: ";
	classLabel.append(classSymbol ? classSymbol->getName() : "!!!");

	os <<"***"<< "[" << int(this) << "]  "
		<< classLabel << "***"
		<< endl;

	os << self << "->" << int(body) << endl;
	if (body) body->print(os);

}
