#pragma once

#include "Node.hpp"
#include <iostream>
#include "../SymbolTable.h"
#include <string>

class Scope;
class VariableNode : public Node {
public:
	Variable *variable;
	string variableName;

	int variableType;
	VariableNode(Symbol *var, int line, int col);

	//VariableNode(Scope* scope, string varName, int line, int col);

	virtual void print(ostream &os);

};
