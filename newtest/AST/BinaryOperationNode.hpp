#pragma once

#include "Node.hpp"
#include <iostream>


class BinaryOperationNode : public Node {
public:
  Node *left, *right;
  char* op_type;

  BinaryOperationNode(char* op, Node *l, Node *r, int line, int col);

  virtual void print(ostream &os);
};
