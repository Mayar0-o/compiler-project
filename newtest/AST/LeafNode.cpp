#pragma once
#include "LeafNode.hpp"


LeafNode::LeafNode(int i, int line, int col) {
	value.type = 1;
	value.int_val = i;
	this->line = line;
	this->col = col;
  }

LeafNode::LeafNode(float f, int line, int col){
	value.type = 2;
	value.float_val = f;
	this->line = line;
	this->col = col;
  }

LeafNode::LeafNode(bool b, int line, int col) {
	value.type = 3;
	value.bool_val = b;
	this->line = line;
	this->col = col;
  }

LeafNode::LeafNode(string s, int line, int col){
	value.type = 4;
	value.string_val = s;
	this->line = line;
	this->col = col;
  }


void LeafNode::print(ostream &os) {
	os << "\t\t";
	os << int(this)
       << "[LeafNode=\"";
	value.print(os);
       os<<"\"]"
       << endl;
  }
