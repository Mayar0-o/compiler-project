#pragma once 
#include "BreakNode.hpp"

BreakNode::BreakNode(int line, int col){
	this->line = line;
	this->col = col;
}

void BreakNode::print(ostream &os) {
    int self = int(this);
    os << self
       << "[BreakNode]"
       << endl;
  }
