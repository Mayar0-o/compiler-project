#pragma once
#include "BinaryOperationNode.hpp"


BinaryOperationNode::BinaryOperationNode(char* op, Node *l, Node *r, int line, int col) : left(l), right(r), op_type(op) {
	  this->line = line;
	  this->col = col;
}

void BinaryOperationNode::print(ostream &os) {
	int self = int(this);
	os << self
		<< "[\"BinaryOperationNode\"]" << "("<< op_type <<")"
	<< endl;
	os << "contain :\n";
	os << "\t\t";
	os << self << "(left)->" << int(left) << endl;
	os << "\t\t";
	os << self << "(right)->" << int(right) << endl;
	if (left) left->print(os);
	if (right) right->print(os);
	
}

