#pragma once

#include "tryNode.hpp"

tryNode::tryNode(Node *bod, Node *catc, int line, int col) : catchNod(dynamic_cast<catchNode*>(catc))
{
	body = bod;
	this->col = col;
	this->line = line;
}

void tryNode::print(ostream &os) {
	int self = int(this);
	os << self
		<< "[tryNode]"
		<< " contain:"
		<< "\t\t"
		<< endl;
	os << "\t\t";
	os << self << "(body)->" << int(body) << endl;
	os << "\t\t";
	if (catchNod) os << self << "(catchNod)->" << int(catchNod) << endl;
	if (body) body->print(os);
	if (catchNod) catchNod->print(os);
  }