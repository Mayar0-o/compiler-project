#include "Node.hpp"

class ContinueNode : public Node {
public:

	ContinueNode(int line, int col);

	virtual void print(ostream &os);


};
