#pragma once

#include "Node.hpp"
#include <iostream>
#include <string>     //  std::to_string



struct Value
{
	int		type;
	int		int_val;
	float	float_val;
	bool	bool_val;
	string  string_val;

	void print(ostream &os){
	switch (type)
		{
		case 1:
			os << int_val;
			break;
		case 2:
			os << float_val;
			break;
		case 3:
			os << bool_val;
			break;
		case 4:
			os << string_val;
			break;			
		}
	}
	string to_string(){
	switch (type)
		{
		case 1:
			return std::to_string(int_val);
			break;
		case 2:
			return std::to_string(float_val);
			break;
		case 3:
			return std::to_string(bool_val);
			break;
		case 4:
			return string_val;
			break;
		}
	}
};

class LeafNode : public Node {

public:
	Value  value;
	LeafNode(int i, int line, int col);
	LeafNode(float f, int line, int col);
	LeafNode(bool b, int line, int col);
	LeafNode(string s, int line, int col);

	virtual void print(ostream &os);


};
