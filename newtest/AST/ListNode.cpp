#pragma once

#include "ListNode.hpp"

ListNode::ListNode(char* namelist)
{
	this->name = namelist;
}

ListNode* ListNode::add_node(Node* node) {
    nodes.push_back(node);
    return this;
  }

  ListNode* ListNode::add_nodes(const vector<Node*>& nodes_list) {
    for (auto &node : nodes_list) {
      add_node(node);
    }
    return this;
  }



void ListNode::print(ostream& os) {
	int self = (int)this;
	char* namee = name;
	os << "*** [" << self << "]  "<< namee << "***"<<endl;
	os << "contain :\n";
	for (auto &node : nodes) {
		if (node == nullptr) continue;
		os<<"\t\t"<< "->" << (int)node << endl;
	}
	for (auto &node : nodes) {
		if (node == nullptr) continue;
		node->print(os);
		
	}
}
 ListNode* ListNode::joinNodeLists(Node* f, Node* s) {
	 ListNode* fl = dynamic_cast<ListNode*>(f);
	 ListNode* sl = dynamic_cast<ListNode*>(s);
	 for (auto &node : sl->nodes) {
		 if (node == nullptr) continue;
		 fl->nodes.push_back(node);
	 }
	 return fl;
 }
