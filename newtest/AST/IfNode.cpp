#pragma once

#include "IfNode.hpp"

IfNode::IfNode(Node *cond, Node *bod, Node *el, int line, int col) : condition(cond), else_node(dynamic_cast<ElseNode*>(el))
{
	body = bod;
	this->col = col;
	this->line = line;
}

   void IfNode::print(ostream &os) {
    int self = int(this);
    os << self
       << "[IfNode]"
	   << " contain:"
	   << "\t\t"
       << endl;
	os << "\t\t";
	os << self << "(condition)->" << int(condition) << endl;
	os << "\t\t";
	os << self << "(body)->" << int(body) << endl;
	os << "\t\t";
	if (else_node) os << self << "(else_node)->" << int(else_node) << endl;
	if (condition) condition->print(os);
	if (body) body->print(os);
    if (else_node) else_node->print(os);
  }

