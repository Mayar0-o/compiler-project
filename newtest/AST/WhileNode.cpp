#pragma once
#include "WhileNode.hpp"


WhileNode::WhileNode(Node *condition, Node *body, int line, int col) : condition(condition) {
	this->body = body;
//	this->body->hasScopeFrame = true;
	this->line = line;
	this->col = col;
}

 void WhileNode::print(ostream &os) {
    int self = int(this);
    os << self
       << "[\"while\"]"
       << endl;
	os << "contain :\n";
	os << "\t\t";
	os << self << "(condition)->" << int(condition) << endl;
	os << "\t\t";
	os << self << "(body)->" << int(body) << endl;
	if(condition) condition->print(os);
    if(body) body->print(os);
 
  }
