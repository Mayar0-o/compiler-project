#pragma once

#include "Node.hpp"
#include "VariableDec.hpp"
#include "ReturnNode.hpp"
#include "tryNode.hpp"
#include "ContinueNode.hpp"
#include "catchNode.hpp"
#include "LeafNode.hpp"
#include "BreakNode.hpp"
#include "ListNode.hpp"
#include "NewNode.hpp"
#include "IDNode.hpp" 
#include "FunctionDefineNode.hpp"
#include "BinaryOperationNode.hpp"
#include "ReturnNode.hpp"
#include "VariableNode.hpp"
#include "AssignmentNode.hpp"
#include "IfNode.hpp"
#include "ElseNode.hpp"
#include "WhileNode.hpp"
#include "ForNode.hpp"
#include "ParameterNode.hpp"
#include "ClassDefineNode.hpp"
#include "ClassMemNode.hpp"
#include "SwitchNode.hpp"
