#pragma once

#include "Node.hpp"
#include <iostream>

class catchNode : public Node {
public:
	Node *body;

	catchNode(Node *body, int line, int col);

	virtual void print(ostream &os);

};