#pragma once
#include "SwitchNode.hpp"


SwitchNode::SwitchNode(Node *expression, Node *body, int line, int col) : expression(expression) {
	this->body = body;
	this->line = line;
	this->col = col;
	this->expression = expression;
}

void SwitchNode::print(ostream &os) {
    int self = int(this);
	os << self
		<< "[SwitchNode]"
		<< " contain:"
		<< "\t\t"
		<< endl;
	os << "\t\t";
	os << self << "(expression)->" << int(expression) << endl;
	os << "\t\t";
	os << self << "(body)->" << int(body) << endl;

	if (expression) expression->print(os);
	if (body) body->print(os);
 
  }
