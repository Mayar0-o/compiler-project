#include "NewNode.hpp"



NewNode::NewNode(Node* args, int line, int col) {
	this->argumentsList	= dynamic_cast<ListNode*>(args);
	this->line = line;
	this->col = col;
 }

void NewNode::print(ostream &os) {
    int self = int(this);
    os << self <<"[NewNode] "
		<< " contain:"
		<< "\t\t"
       <<endl;
	if (argumentsList){
	os << self << "(argumentsList)->" << int(argumentsList) << endl;
	self = (int)argumentsList;
	os << self << "[Params]" 
	<< " contain:"
		<< "\t\t"
		<< endl;
	for (auto &node : argumentsList->nodes) {
		os << self << "->" << (int)node << endl;
		if (node == nullptr) continue;
		node->print(os);
	
	}
	}

 }
