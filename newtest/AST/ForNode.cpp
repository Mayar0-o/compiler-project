#pragma once 
#include "ForNode.hpp"


 ForNode::ForNode(Node *initializer, Node *condition, Node *post_statement, Node *body, int line, int col) :
    initializer(initializer),
    condition(condition),
    post_statement(post_statement)
     {
	 this->body = body;
	 this->line = line;
	 this->col = col; 
 }

 void ForNode::print(ostream &os) {
    int self = int(this);
	os << self
		<< "[\"for\"]"
		<< "\n"
		<< "contain :\n"
		<< "\t\t"
		<<  "(initializer)->" << int(initializer) << endl
		<< "\t\t"
		<<  "(condition)->" << int(condition) << endl
		<< "\t\t"
		<<  "(post_statement)->" << int(post_statement) << endl
		<< "\t\t"
	    <<  "(body)->" << int(body) << endl;
	if (initializer) initializer->print(os);
	if (condition)condition->print(os);
	if (post_statement)post_statement->print(os);
	if (body)body->print(os);
  }
