#pragma once 
#include "ElseNode.hpp"


ElseNode::ElseNode(Node *node, int line, int col){
	this->body = node;
	this->line = line;
	this->col = col;
}

 void ElseNode::print(ostream &os) {
    int self = int(this);
	os << self
		<< "[ElseNode]"
		<< " contain:"
		<< "\t\t"
		<< endl;
	if (body)  body->print(os);
	os << "\t\t";
    os << self << "->" << int(body) << endl;
  }
