#include "Node.hpp"
#include "ListNode.hpp"
#include <iostream>

using namespace std;

class NewNode : public Node {
public:
	ListNode* argumentsList;  
	NewNode(Node* args, int line, int col);

	virtual void print(ostream& os);
};