#pragma once
#include "ParameterNode.hpp"


ParameterNode::ParameterNode(Symbol* parSym, int line, int col) {
	this->parSym = dynamic_cast<Parameter*>(parSym);
	this->line = line;
	this->col = col;
}


void ParameterNode::print(ostream &os) {
	int self = int(this);
	string parLabel = parSym ? parSym->getName() : "!!!";
	os << int(this)
		<< "[ParameterNode= \""
		<< parLabel
		<< "\"]"
		<< endl;
}
