#pragma once

#include "ClassMemNode.hpp"

ClassMemNode::ClassMemNode(Symbol* memberSym,Node* assigm, int line, int col) {
	this->memberSym = dynamic_cast<DataMember*>(memberSym);
	if (assigm)
		this->assig = assigm;
	this->line = line;
	this->col = col;
}


void ClassMemNode::print(ostream &os) {
	int self = int(this);
	
	string memLabel = memberSym ? memberSym->getName() : "!!!";

	os << int(this)
		<< "[DataMmember = \""
		<< memLabel
		<< "\"]"
		<< endl;
	os << "contain : \n\t\t";
	os << "->" << int(assig) << endl;

	if (assig) assig->print(os);
}


string ClassMemNode::getName() {
	return this->memberSym->getName();
}

DataMember* ClassMemNode::getMemSymbol() {
	return this->memberSym;
}
