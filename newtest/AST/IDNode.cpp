#pragma once 
#include "IDNode.hpp"
#include <string>   

IDNode::IDNode(int line, int col,string nam1){
	this->line = line;
	this->col = col;
	this->name = nam1;
}

void IDNode::print(ostream &os) {
    int self = int(this);
	os << "\t\t";
	os << self
		<< "[\"IDNode\"]";
	os << "[" << name << "]"
       << endl;
  }
