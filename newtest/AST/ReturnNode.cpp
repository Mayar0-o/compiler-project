#pragma once
#include "ReturnNode.hpp"


ReturnNode::ReturnNode(Node *expression, int line, int col) {
	this->return_node = expression;
	this->line = line;
	this->col = col;
}

void ReturnNode::print(ostream &os) {
    auto self = int(this);
    os << self
       << "[ReturnNode]"
	 
       <<endl;
    if (return_node) {
		os << self << "(expression)->" << int(return_node) << endl;
		return_node->print(os);
     
    }
  }
