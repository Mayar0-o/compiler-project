#pragma once 
#include "ContinueNode.hpp"


ContinueNode::ContinueNode(int line, int col){
	this->line = line;
	this->col = col;
}

 void ContinueNode::print(ostream &os) {
    int self = int(this);
    os << self
       << "[ContinueNode]"
       << endl;
  }
