#pragma once

#include "Node.hpp"
#include <iostream>

class AssignmentNode : public Node {
public:
  Node *lhs, *rhs;
  AssignmentNode(Node *l, Node *r, int line , int col);

  virtual void print(ostream &os);

};
