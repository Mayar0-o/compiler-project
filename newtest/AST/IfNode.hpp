#pragma once

#include "ElseNode.hpp"
#include <iostream>

using namespace std;

class IfNode : public Node {
public:
	Node *condition, *body;
	Node *else_node;

	IfNode(Node *cond, Node *bod, Node *el, int line, int col);

	virtual void print(ostream &os);

  

};



