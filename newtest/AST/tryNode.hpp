#pragma once

#include "catchNode.hpp""
#include <iostream>

using namespace std;

class tryNode : public Node {
public:
	Node  *body;
	Node *catchNod;

	tryNode(Node *bod, Node *catchN, int line, int col);

	virtual void print(ostream &os);

};



