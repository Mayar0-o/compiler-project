#pragma once

#include "Node.hpp"
#include <iostream>

class SwitchNode : public Node {
public:
	Node *expression;
	Node *body;

	SwitchNode(Node *expression, Node *body, int line, int col);

	virtual void print(ostream &os);

};
