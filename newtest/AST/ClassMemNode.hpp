#pragma once 
#include "Node.hpp"
#include "ListNode.hpp"
#include "../SymbolTable.h"

class ClassMemNode : public Node {
public:
	Node* assig;
	ClassMemNode(Symbol* memSym,Node* assig, int line , int col);

	virtual void print(ostream &os);

	//Symbol interfacing methods:
	
	string getName();
	DataMember* getMemSymbol();

private:
	DataMember* memberSym;
};
