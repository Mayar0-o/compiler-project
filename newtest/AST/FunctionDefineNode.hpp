#pragma once

#include "Node.hpp"
#include "../SymbolTable.h"
#include "ListNode.hpp"

/*
	A node that represents a Function Defination.
*/
class FunctionDefineNode : public Node {
public:
  Function* functionSym;      // a pointer to the corresponding symbol in symbol table
  Node* bodySts;          // body statements nodes
  ListNode* paramsList;       // a list of parameter nodes

	FunctionDefineNode(Symbol* func, Node* bod, Node* paramsList, int line, int col);

	virtual void print(ostream &os);

};
