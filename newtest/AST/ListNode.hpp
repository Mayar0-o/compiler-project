#pragma once

#include "Node.hpp"
#include <vector>
#include <iostream>

using namespace std;

class ListNode : public Node {
public:
	char* name;
	vector<Node*> nodes;

	ListNode(char* namelist);

	ListNode* add_node(Node* node);

	ListNode* add_nodes(const vector<Node*>& nodes_list);


	virtual void print(ostream& os);

	static ListNode* joinNodeLists(Node* f, Node* s);



};
