#pragma once 
#include "catchNode.hpp"


catchNode::catchNode(Node *node, int line, int col){
	this->body = node;
	this->line = line;
	this->col = col;
}

void catchNode::print(ostream &os) {
	int self = int(this);
	os << self
		<< "[catchNode]"
		<< " contain:"
		<< "\t\t"
		<< endl;
	
	os << "\t\t";
	os << self << "(body)->" << int(body) << endl;
	if (body)  body->print(os);
}
