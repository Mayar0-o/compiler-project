#pragma once

#include "Node.hpp"
#include <iostream>

class ElseNode : public Node {
public:
	Node *body;

	ElseNode(Node *body, int line, int col);

	virtual void print(ostream &os);

};