#pragma once

#include "Node.hpp"
#include "../SymbolTable.h"


class ParameterNode : public Node {
public : 

	Parameter* parSym; 

	ParameterNode(Symbol* parSym, int line, int col);


	void print(ostream &os);


};