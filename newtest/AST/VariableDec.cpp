#pragma once
#include "VariableDec.hpp"
#include "../MyParser.h"

VariableDec::VariableDec(Symbol *v, Node * VariableNode, int line, int col) : variable(dynamic_cast<Variable*>(v)) {
	this->line = line;
	this->col = col;
	this->VariableNode = VariableNode;
}

void VariableDec::print(ostream &os) {
	string variableTypet;
	switch (variableType){
	case SBYTET:     variableTypet = "SBYTE "; break;
	case BYTET:     variableTypet = "BYTE "; break;
	case SHORTT:     variableTypet = "SHORT "; break;
	case USHORTT:     variableTypet = "USHORT "; break;
	case INTT:        variableTypet = "INT "; break;
	case UINTT:       variableTypet = "UINT "; break;
	case LONGT:       variableTypet = "LONG "; break;
	case ULONGT:      variableTypet = "ULONG "; break;
	case CHART:       variableTypet = "CHAR "; break;
	case FLOATT:       variableTypet = "FLOAT "; break;
	case DOUBLET:      variableTypet = "DOUBLE "; break;
	case POINTERT:     variableTypet = "POINTER "; break;
	case VOIDT:        variableTypet = "VOID "; break;
	case BOOLT:     variableTypet += "BOOL "; break;
	}
	os << int(this)
		<< "[VariableName=\""
		<< variableName
		<< "\"]"
		<< "[Type=\""
		<< variableTypet
		<< "\"]"
		<< endl;
	if (VariableNode)
	{
		VariableNode->print(os);

	}

}
