#include"MyScope.h"

Scope::Scope(Scope* parentScope,int scopeId){
	this->parentScope = parentScope;
	this->innerScope = nullptr; 
	this->nextScope = nullptr; 
	this->scopemap = new MyMap();
	this->scopemap->setOwnerScope(this);
	this->ownerSymbol = nullptr;
	this->scopeid = scopeId;
}


Scope::~Scope(){

}
int Scope::GetScopeId(){
	return this->scopeid ;
}
string Scope::getownername(){
	if (this->ownername != "x")
		return this->ownername;
	else
		return " programm";
}

void Scope::addInCuurent(Scope* scope){
	if (this->innerScope == nullptr){
		innerScope = scope;
		return;
	}
	Scope* lastInner = this->innerScope;
	while (lastInner->nextScope != nullptr){
		lastInner = lastInner->nextScope;
	}
	lastInner->nextScope = scope;
	return;
}
void Scope::setParentScope(Scope* scope){
	this->parentScope = scope;
}

Scope* Scope::getParentScope(){
	return this->parentScope;
}



Scope* Scope::getInnerScope(){
	return this->innerScope;
}

void Scope::setNextScope(Scope* scope){
	this->nextScope = scope;
}

Scope* Scope::getNextScope(){
	return this->nextScope;
}

MyMap* Scope::getSymbolTable(){
	return this->scopemap;
}

void Scope::setOwnerSymbol(Symbol* owner){
	this->ownerSymbol = owner;
}

Symbol* Scope::getOwnerSymbol(){
	return this->ownerSymbol;
}
 
void Scope::setownername(string name){
	this->ownername = name;
}

