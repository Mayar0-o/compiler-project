#ifndef SYMBOL_H
#define SYMBOL_H

#include <string>
#include <vector>
using namespace std;


class Symbol {
public:
	Symbol(char* name, int symbolType, int colNo, int lineNo);
	~Symbol();
	virtual string toString() = 0;
	virtual int getSymbolType() = 0;
	void setSymbolType(int type);
	void setName(string name);
	char* getName();
	Symbol* getNext();
	Symbol* setNext(Symbol* symbol);
	int getColNo();
	int getLineNo();
	Symbol* node;
	char * name;
	char *orginalname;
	char* getorignalName();

private:
	
	int symbolType;
	Symbol* next; 
	int colNo;
	int lineNo;
	
};

class Variable : public Symbol {
public:
	

	Variable(char * name, int symbolType, int colNo, int lineNo ,int vartype);

	int getSymbolType();


	int getVariableType();
	void setVariableType(int type);

	void setId(int id);
	int getId();

	string getNameWithout();

	string getUniqeName();

	string toString();
private:
	int variableType;
	int symbolType;	
};

class Scope; 
class Parameter;

class Function : public Symbol {
public:

	Function(char* name, int returnType, int colNo, int lineNo, Scope* bodyScope, int* mod_opt,int counter);
	Function(char* name, int returnType, int colNo, int lineNo,  int* mod_opt, int counter);
	Function(char* name,  int colNo, int lineNo, Scope* bodyScope, int* mod_opt, int counter);
	Function(char* name,  int colNo, int lineNo, int* mod_opt, int counter);
	~Function();
	int getSymbolType();

	int getReturnType();
	void setReturnType(int returnType);

	Scope* getBodyScope();
	void setBodyScope(Scope* scope);

	void setParams(Symbol* params);
	Symbol* getParams();

	string toString();

	int* getAccessModifier();
	void setAccessModifier(int accessModifier);
	bool isconstruct();
	bool isfinal();

	int getAccessModifiercount();
	bool isoverloaded();
	void setasoverloaded();
private:
	int returnType;
	Scope* bodyScope;
	Symbol* params;

	int modi[25];
	int counter;
	bool construct;
	bool isFinal=false;
	bool isoverload = false;
};

class DataMember;

class Class : public Symbol {
public:

	Class(char* name, int colNo, int lineNo, Scope* bodyScope, int* mod_opt, int counter);
	~Class();
	bool isFinal;
	void setAsFinal();
	void setInhertedFrom(string inhertedFrom);
	string getInhertedFrom();

	
	string toString();
	int getSymbolType();

	void setBodyScope(Scope* bodyScope);
	Scope* getBodyScope();

	Symbol* addToDataMembers(DataMember* dataMem);

	Class* getOuterClass();
	void setOuterClass(Class *outerClass);
	void set_struct();
	
	DataMember* getDataMember();

	int* getAccessModifier();
	int getAccessModifiercount();
private:
	string inhertedFrom; 
	Scope* bodyScope;
	DataMember* dataMembers;
	
	Class* baseClassSymbol;
	Class* outerClass;
	int modi[25];
	int modcounter;
	bool structt=false;
};


class DataMember : public Variable {
public:

	DataMember(char * name,  int colNo, int lineNo ,int returntype,int*accessmod,int countert);
	
	~DataMember();

	int* getAccessModifier();
	int getAccessModifiercount();
	void setAccessModifier(int accessModifier);


	
	int getSymbolType();
	string toString();
private:
	int accessModifier[25];
	int counter;
	int datamemberType;
	static int staticCounter;


};



class Parameter : public Variable {
public:
	Parameter(char * name, int colNo, int lineNo,int returntype);
	~Parameter();

	bool isDefault;
private:
	int paramType;
};

#endif
