#include"SymbolTable.h"
#include"myvalue.h"
#include <sstream>
#include <iostream>
using namespace std;

Symbol::Symbol(char* name, int type, int colNo, int lineNo){
	if (name == nullptr)
	this->name = "noname";
	else
	this->name = name;
	this->symbolType = type;
	this->next = nullptr;
	this->node = nullptr;
	this->colNo = colNo;
	this->lineNo = lineNo;
	this->orginalname = name;
}

Symbol::~Symbol(){

}


void Symbol::setSymbolType(int type){
	this->symbolType = type;
}

void Symbol::setName(string name){
	this->name = _strdup(name.c_str()); 
}

char* Symbol::getName(){
	return this->name;
}
char* Symbol::getorignalName(){
	return this->orginalname;
}


Symbol* Symbol::getNext(){
	return this->next;
}

Symbol* Symbol::setNext(Symbol* symbol){
	this->next = symbol;
	return symbol;
}

int Symbol::getColNo(){
	return this->colNo;
}

int Symbol::getLineNo(){
	return this->lineNo;
}

Variable::Variable(char * name, int symbolTypet, int colNo, int lineNo ,int vartype) : Symbol(name, symbolTypet, colNo, lineNo) {

	this->variableType = vartype;

	this->symbolType = symbolTypet;

}

int Variable::getSymbolType(){
	return this->symbolType;
}

int Variable::getVariableType(){
	return this->variableType;
}

void Variable::setVariableType(int type){
	this->variableType = type;
}

string Variable::toString(){
	string name, variableTypet,vartype;
	name = this->getName();
	switch (this->variableType){
	case SBYTET:     variableTypet = "SBYTE "; break;
	case BYTET:     variableTypet = "BYTE "; break;
	case SHORTT:     variableTypet = "SHORT "; break;
	case USHORTT:     variableTypet = "USHORT "; break;
	case INTT:        variableTypet = "INT "; break;
	case UINTT:       variableTypet = "UINT "; break;
	case LONGT:       variableTypet = "LONG "; break;
	case ULONGT:      variableTypet = "ULONG "; break;
	case CHART:       variableTypet = "CHAR "; break;
	case FLOATT:       variableTypet = "FLOAT "; break;
	case DOUBLET:      variableTypet = "DOUBLE "; break;
	case POINTERT:     variableTypet = "POINTER "; break;
	case VOIDT:        variableTypet = "VOID "; break;
	case BOOLT:     variableTypet += "BOOL "; break;
	}
	switch (this->symbolType){
	case VARIABLE:     vartype = "VARIABLE "; break;
	case PARAMETER:     vartype = "PARAMETER "; break;
	}
	if (vartype == "VARIABLE ")
	return vartype + " | " + variableTypet +" | " + name+ "   ";
	if (vartype == "PARAMETER ")
	return vartype + " " + variableTypet +" "+ name + "   ";
}


Function::Function(char* name, int returnType, int colNo, int lineNo, Scope* bodyScope, int* mod_opt , int countert) : Symbol(name, FUNCTION, colNo, lineNo) {
	this->returnType = returnType;
	this->bodyScope = bodyScope;
	this->params = nullptr;
	this->counter = countert;
	for (int i = 0; i < this->counter;i++)
	this->modi[i] = mod_opt[i];
	this->construct = false;
	for (int i = 0; i < this->counter; i++){
		if (this->modi[i] == FINALT)
			this->isFinal = true;
	}


}
Function::Function(char* name, int returnType, int colNo, int lineNo, int* mod_opt, int countert) : Symbol(name, FUNCTION, colNo, lineNo) {
	this->returnType = returnType;
	this->bodyScope = nullptr;
	this->params = nullptr;
	this->counter = countert;
	for (int i = 0; i < this->counter; i++)
		this->modi[i] = mod_opt[i];
	this->construct = false;
	for (int i = 0; i < this->counter; i++){
		if (this->modi[i] == FINALT)
			this->isFinal = true;
	}
}
Function::Function(char* name,  int colNo, int lineNo, Scope* bodyScope, int* mod_opt, int countert) : Symbol(name, FUNCTION, colNo, lineNo) {
	this->returnType = NOTYPE;
	this->bodyScope = bodyScope;
	this->params = nullptr;
	this->counter = countert;
	for (int i = 0; i < this->counter; i++)
		this->modi[i] = mod_opt[i];
	this->construct = true;

}
Function::Function(char* name,  int colNo, int lineNo, int* mod_opt, int countert) : Symbol(name, FUNCTION, colNo, lineNo) {
	this->returnType = NOTYPE;
	this->bodyScope = nullptr;
	this->params = nullptr;
	this->counter = countert;
	for (int i = 0; i < this->counter; i++)
		this->modi[i] = mod_opt[i];
	this->construct = true;
}


string Function::toString(){
	string name, accessModifier, returntypet, returnType,param,overload;
	this->returnType ? returnType = this->returnType : returnType = "NULL";
	this->getName() ? name = this->getName() : name = "NULL";
	for (int i = 0; i < this->counter; i++){
		if (i!=0)
		accessModifier += "-";
		switch (this->modi[i]){
		case ABSTRACTT:     accessModifier += "ABSTRACT "; break;
		case EXTERNT:     accessModifier += "EXTERN "; break;
		case INTERNALT:     accessModifier += "INTERNAL "; break;
		case NEWT:     accessModifier += "NEW "; break;
		case OVERRIDET:     accessModifier += "OVERRIDE "; break;
		case PRIVATET:     accessModifier += "PRIVATE"; break;
		case PROTECTEDT:     accessModifier += "PROTECTED"; break;
		case PUBLICT:     accessModifier += "PUBLIC"; break;
		case READONLYT:     accessModifier += "READONLY "; break;
		case SEALEDT:     accessModifier += "SEALED"; break;
		case STATICT:     accessModifier += "STATIC "; break;
		case UNSAFET:     accessModifier += "UNSAFE "; break;
		case VIRTUALT:     accessModifier += "VIRTUAL "; break;
		case VOLATILET:     accessModifier += "VOLATILE "; break;
		}
	}
	switch (this->returnType){
	case SBYTET:     returntypet = "SBYTE "; break;
	case BYTET:     returntypet = "BYTE "; break;
	case SHORTT:     returntypet = "SHORT "; break;
	case USHORTT:     returntypet = "USHORT "; break;
	case INTT:        returntypet = "INT "; break;
	case UINTT:       returntypet = "UINT "; break;
	case LONGT:       returntypet = "LONG "; break;
	case ULONGT:      returntypet = "ULONG "; break;
	case CHART:       returntypet = "CHAR "; break;
	case FLOATT:       returntypet = "FLOAT "; break;
	case DOUBLET:      returntypet = "DOUBLE "; break;
	case POINTERT:     returntypet = "POINTER "; break;
	case VOIDT:        returntypet = "VOID "; break;
	case BOOLT:     returntypet = "BOOL "; break;
	case NOTYPE:     returntypet = "nothing "; break;

	}
	if (this->params){
		Symbol* walker = this->params;
		while (walker != nullptr){
			param.append(" | ");
			param.append(walker->toString());
			walker = walker->node;
		}
	}
	else
		param = " ";
	if (this->isoverloaded())
		overload = "overloaded method";
	else
		overload = ".";
	if (this->returnType == NOTYPE)
		return " constructor | " + accessModifier + " | " + returntypet +"|" + name;
	else
		return " FUNCTION    | " + accessModifier + "|" + returntypet + name +"(" + param + ")";
}

int Function::getSymbolType(){
	return FUNCTION;
}

void Function::setReturnType(int returnType){
	this->returnType = returnType;
}

int Function::getReturnType(){
	return this->returnType;
}

Scope* Function::getBodyScope(){
	return this->bodyScope;
}

void Function::setBodyScope(Scope* bodyScope){
	this->bodyScope = bodyScope;
}

void Function::setParams(Symbol* params){
	this->params = params;
}
Symbol* Function::getParams(){
	return this->params;
}

int* Function::getAccessModifier(){
	return this->modi;
}
int Function::getAccessModifiercount(){
	return this->counter;
}

void Function::setAccessModifier(int accessModifier){
	this->modi[1] = accessModifier;
}
bool Function::isconstruct(){
	return this->construct;
}
bool Function::isfinal(){
	return this->isFinal;
}
bool Function::isoverloaded(){
	return this->isoverload;
}

void Function::setasoverloaded(){
	this->isoverload =true;
}


Class::Class(char* name, int colNo, int lineNo, Scope* bodyScope,int* mod_opt ,int counter) : Symbol(name, CLASS, colNo, lineNo){
	this->inhertedFrom = "Object";
	this->bodyScope = bodyScope;
	this->modcounter = counter;
	this->isFinal = false;
	for (int i = 0; i < this->modcounter; i++)
		this->modi[i] = mod_opt[i];
	for (int i = 0; i < this->modcounter; i++){
		if (this->modi[i] == FINALT)
			this->isFinal = true;
	}
}

void Class::setInhertedFrom(string inhertedFrom){
	this->inhertedFrom = inhertedFrom;
}

string Class::getInhertedFrom(){
	return this->inhertedFrom;
}

int Class::getSymbolType(){
	return CLASS;
}

void Class::set_struct(){
	this->structt = true;
}

string Class::toString(){
	string name, accessModifier;
	string outername = "not set";
	if (this->modcounter == 0)
		accessModifier = "PROTECTED CLASS";
	this->getName() ? name = this->getName() : name = "NULL";
	for (int i = 0; i < this->modcounter; i++){
		switch (this->modi[i]){
		case ABSTRACTT:  accessModifier += "ABSTRAC "; break;
		case EXTERNT:    accessModifier += "EXTERN"; break;
		case INTERNALT:  accessModifier += "INTERNAL"; break;
		case NEWT:       accessModifier += "NEW"; break;
		case OVERRIDET:  accessModifier += "OVERRIDE"; break;
		case PRIVATET:   accessModifier += "PRIVATE "; break;
		case PROTECTEDT: accessModifier += "PROTECTED "; break;
		case PUBLICT:    accessModifier += "PUBLIC ,"; break;
		case READONLYT:  accessModifier += "READONLYT "; break;
		case SEALEDT:    accessModifier += "SEALED "; break;
		case STATICT:    accessModifier += "STATIC "; break;
		case UNSAFET:    accessModifier += "UNSAFE "; break;
		case VIRTUALT:   accessModifier += "VIRTUAL "; break;
		case VOLATILET:  accessModifier += "VOLATILE "; break;
		}

	
	}
	    if (this->getOuterClass() != nullptr)
		outername = string(this->getOuterClass()->getName()); //if you want to print it ,do

	if (!this->structt)
		return " CLASS    | " + accessModifier + " | " + name + " : " + inhertedFrom ;
	else
	return " STRUCT    | " + name + " | "  + accessModifier;
}

Scope* Class::getBodyScope(){
	return this->bodyScope;
}

void Class::setBodyScope(Scope* scope){
	this->bodyScope = scope;
}

Symbol* Class::addToDataMembers(DataMember* dataMem){
	//double check @dataMem @node
	dataMem->node = nullptr;
	if (this->dataMembers == nullptr){
		this->dataMembers = dataMem;
		return dataMem;
	}

	DataMember* walker = this->dataMembers;
	while (walker->node != nullptr){
		walker = dynamic_cast<DataMember*>(walker->node);
	}
	walker->node = dataMem;
	return dataMem;
}




DataMember* Class::getDataMember(){
	return this->dataMembers;
}

Class* Class::getOuterClass(){
	return this->outerClass;
}

void Class::setOuterClass(Class* outerClass){
	this->outerClass = outerClass;
}
void Class::setAsFinal(){
	this->isFinal = true;
}

int* Class::getAccessModifier(){
	return this->modi;
}
int Class::getAccessModifiercount(){
	return this->modcounter;
}



DataMember::DataMember(char * name,  int colNo, int lineNo, int returntype,int* accessmod ,int countert) : Variable(name, DATA_MEMBER, colNo, lineNo, returntype){
	this->datamemberType = returntype;
	this->counter = countert;
	for (int i = 0; i < this->counter; i++)
		this->accessModifier[i] = accessmod[i];
};

int* DataMember::getAccessModifier(){
	return this->accessModifier;
}
int DataMember::getAccessModifiercount(){
	return this->counter;
}

void DataMember::setAccessModifier(int accessModifier){
	this->accessModifier[1] = accessModifier;
}


int DataMember::getSymbolType(){
	return DATA_MEMBER;
}

string DataMember::toString(){
	string name, dataMemberTypet, accessModifier;
	name = this->getName();
	if (this->counter == 0)
		accessModifier ="PROTECTED DATAMEMBER";
	else
		for (int i = 0; i < this->counter; i++){
			switch (this->accessModifier[i]){
			case ABSTRACTT:     accessModifier += "ABSTRACT "; break;
			case EXTERNT:     accessModifier += "EXTERN "; break;
			case INTERNALT:     accessModifier += "INTERNAL "; break;
			case NEWT:     accessModifier += "NEW "; break;
			case OVERRIDET:     accessModifier += "OVERRIDE "; break;
			case PRIVATET:     accessModifier += "PRIVATE"; break;
			case PROTECTEDT:     accessModifier += "PROTECTED"; break;
			case PUBLICT:     accessModifier += "PUBLIC"; break;
			case READONLYT:     accessModifier += "READONLY"; break;
			case SEALEDT:     accessModifier += "SEALED"; break;
			case STATICT:     accessModifier += "STATIC"; break;
			case UNSAFET:     accessModifier += "UNSAFE"; break;
			case VIRTUALT:     accessModifier += "VIRTUAL"; break;
			case VOLATILET:     accessModifier += "VOLATILE"; break;
			
			}
		}
	switch (this->datamemberType){
	case SBYTET:     dataMemberTypet = "SBYTE "; break;
	case BYTET:     dataMemberTypet = "BYTE "; break;
	case SHORTT:     dataMemberTypet = "SHORT "; break;
	case USHORTT:     dataMemberTypet = "USHORT "; break;
	case INTT:        dataMemberTypet = "INT "; break;
	case UINTT:       dataMemberTypet = "UINT "; break;
	case LONGT:       dataMemberTypet = "LONG "; break;
	case ULONGT:      dataMemberTypet = "ULONG "; break;
	case CHART:       dataMemberTypet = "CHAR "; break;
	case FLOATT:       dataMemberTypet = "FLOAT "; break;
	case DOUBLET:      dataMemberTypet = "DOUBLE "; break;
	case POINTERT:     dataMemberTypet = "POINTER "; break;
	case VOIDT:        dataMemberTypet = "VOID "; break;
	case BOOLT:     accessModifier += "BOOL "; break;
	}
	return
		" DATA MEMBER | " + name + " | " + dataMemberTypet + " | "  + accessModifier;
}


Parameter::Parameter(char * name, int colNo, int lineNo, int returntype) : Variable(name, PARAMETER, colNo, lineNo, returntype){ // a parameter is always inited
	this->paramType = returntype;
}


