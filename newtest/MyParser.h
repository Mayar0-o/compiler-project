#ifndef MYPARSER_H
#define MYPARSER_H

#include "ErrorRecovery.h"
#include "MyScope.h"
#include "myvalue.h"
#include <stack>
extern ErrorRecovery errorRec;
class Class;
class MyParser {
public :
	MyParser();
	~MyParser();

	void setCurrentScope(Scope * scope);
	Scope* getCurrentScope();

	void setCurrentClassSym(Class* classSym);
	Class* getCurrentClassSym();

	Scope* getRootScope();

	Symbol* insertSymbol(Symbol* symbol);

	Symbol* insertSymbol(Symbol* symbol, Scope* scope);

	Symbol* lookUpSymbol(char* name, int lineNo, int colNo);

	Symbol* lookUpSymbol(Scope* scope, char* name, int lineNo, int colNo);

	Symbol* lookUpSymbol(Scope* scope, char* name);

	void removev(char* name , Scope* scope);

	void finishscop();

	void printMyMap();

	Symbol* insertFunctionSymbol(char* name, int returnType, int colNo, int lineNo, Scope* scope, Symbol* params, int* mod_opt ,int counter);

	
	Symbol* insertFunctionSymbol(char* name, int returnType, int colNo, int lineNo, Symbol* params, int* mod_opt, int counter);

	Symbol* insertFunctionSymbol(char* name,  int colNo, int lineNo, Symbol* params, int* mod_opt, int counter);

	Symbol* insertFunctionSymbol(char* name,  int colNo, int lineNo, Scope* scope, Symbol* params, int* mod_opt, int counter);

	Symbol* setClass(char* inhertedFrom, Class* classSymbol, Scope* scope, Class* outerclass);


	Scope* createNewScope(int scopeId);


	//----------------------------------------------------------------------------------------------------
	//symbol table check
	//----------------------------------------------------------------------------------------------------

	//visit all scop and create vector
	void travelallscope(Scope * scope);
	//call all check function
	void checkclassdec();
	//add all class to vector
	void addclasstovector(Scope * scope);
	//class check
	void checkclassdecleration();
	void checkclassdeclerationname();
	void check_ring_inher();
	void check_ring_inher_for_one_class(Class* current, string basename);
	//class stack methods use for set outer class
	void pushToClassesStack(Class* classSym);
	void popFromClassesStack();
	stack<Class*> *classesStack;
	stack<Class*>* getClassstack();
	//vector class
	vector<Symbol*> symbolsallclass;
	vector<Symbol*> symbolsallclasscomp;
	//check inner class
	void checkInnerClasses();
	void checkNamingOfInners(Class* inner);
	void checkInherOfInners(Class* inner);
	//check constructor
	Symbol* checksetconstructer(Class* classSymbol);
	void constructerror();
	//add class function datamember to vector 
	void addsymboltovector(Scope * scope);
	vector<Symbol*> symbolsallsymbols;
	vector<Symbol*> symbolsallsymbolscomp;
	//use vector above to check modifer
	void modifiersget();
	void modifierscheck(int*n,int count,Class*c);
	void modifierscheck(int*n, int count, Function*c);
	void modifierscheck(int*n, int count, DataMember*c);
	//vector of function 
	vector<Symbol*> symbolsallfunction;
	vector<Symbol*> symbolsallfunctioncomp;
	void addfunctiontovector(Scope * scope);
	//check parametars
	void checkparametars();

	void checkfinaloverridingmethods();
//	void travelallscopeforinner(Scope * scope, Scope* curr, Class*inner);
//  void addclasstovectorinner(Scope * scope, Class* inner);
//	void checkclassforinnerinher(Class* inner);
	void checkmainmethod();
	void checkoverloadmethod();
	void checkabstructclass();
	vector<Symbol*> symbolsallfunctionforabstruct;
	void checkInherOfInnerscomp(Class* inner);
	void travelallscopeforinner(Scope * scope, Scope* block);
	void addclasstovectorinner(Scope * scope);
	vector<Symbol*> symbolsallclassforinner;
	void onepubliclasscheck(Scope* root);



private :
	ErrorRecovery *errRecovery;

	Scope * currScope;

	Scope * rootScope;
	
	Class* currClassSym;
	string buildTableString(Scope* scope);

};
#endif
