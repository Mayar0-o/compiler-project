#ifndef MYSCOPE_H
#define MYSCOPE_H
#include "MyMap.h"

class Scope {
public :
	Scope(Scope* parentScope, int scopeId);
	~Scope();
	int GetScopeId();
	Scope* getParentScope();
	void setNextScope(Scope * symbol);
	Scope * getNextScope();

	void setownername(string name);
	string getownername();
	void setParentScope(Scope* scope);

	void setOwnerSymbol(Symbol* owner);
	Symbol* getOwnerSymbol();
	void addInCuurent(Scope * scope);
	Scope * getInnerScope();
	MyMap* getSymbolTable();
	
private :
	int scopeid;
	Scope *  parentScope;
	Scope *  innerScope;
	MyMap *  scopemap;
	Scope *  nextScope; 
	Symbol*  ownerSymbol;
	string   ownername = "x";
};


#endif
