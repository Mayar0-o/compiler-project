/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     FINAL = 258,
     RANK_SPECIFIER = 259,
     IDENTIFIER = 260,
     INTEGER_LITERAL = 261,
     REAL_LITERAL = 262,
     CHARACTER_LITERAL = 263,
     STRING_LITERAL = 264,
     ABSTRACT = 265,
     AS = 266,
     BASE = 267,
     BOOL = 268,
     BREAK = 269,
     BYTE = 270,
     CASE = 271,
     CATCH = 272,
     CHAR = 273,
     CHECKED = 274,
     CLASSX = 275,
     CONST = 276,
     CONTINUE = 277,
     DECIMAL = 278,
     DEFAULTX = 279,
     DELEGATE = 280,
     DO = 281,
     DOUBLE = 282,
     ELSE = 283,
     ENUM = 284,
     EVENT = 285,
     EXPLICIT = 286,
     EXTERN = 287,
     FALSE = 288,
     FINALLY = 289,
     FIXED = 290,
     FLOAT = 291,
     FOR = 292,
     FOREACH = 293,
     GOTO = 294,
     IF = 295,
     IMPLICIT = 296,
     IN = 297,
     INT = 298,
     INTERFACEX = 299,
     INTERNAL = 300,
     IS = 301,
     LOCK = 302,
     LONG = 303,
     NAMESPACE = 304,
     NEW = 305,
     NULL_LITERAL = 306,
     OBJECTX = 307,
     OPERATOR = 308,
     OUT = 309,
     OVERRIDE = 310,
     PARAMS = 311,
     PRIVATE = 312,
     PROTECTED = 313,
     PUBLIC = 314,
     READONLY = 315,
     REF = 316,
     RETURN = 317,
     SBYTE = 318,
     SEALED = 319,
     SHORT = 320,
     SIZEOF = 321,
     STACKALLOC = 322,
     STATIC = 323,
     STRING = 324,
     STRUCT = 325,
     SWITCH = 326,
     THIS = 327,
     THROW = 328,
     TRUE = 329,
     TRY = 330,
     TYPEOF = 331,
     UINT = 332,
     ULONG = 333,
     UNCHECKED = 334,
     UNSAFE = 335,
     USHORT = 336,
     USING = 337,
     VIRTUAL = 338,
     VOID = 339,
     VOLATILE = 340,
     WHILE = 341,
     ASSEMBLY = 342,
     FIELD = 343,
     METHOD = 344,
     MODULE = 345,
     PARAM = 346,
     PROPERTY = 347,
     TYPEX = 348,
     GET = 349,
     SET = 350,
     ADD = 351,
     REMOVE = 352,
     COMMA = 353,
     LEFT_BRACKET = 354,
     RIGHT_BRACKET = 355,
     PLUSEQ = 356,
     MINUSEQ = 357,
     STAREQ = 358,
     DIVEQ = 359,
     MODEQ = 360,
     XOREQ = 361,
     ANDEQ = 362,
     OREQ = 363,
     LTLT = 364,
     GTGT = 365,
     GTGTEQ = 366,
     LTLTEQ = 367,
     EQEQ = 368,
     NOTEQ = 369,
     LEQ = 370,
     GEQ = 371,
     ANDAND = 372,
     OROR = 373,
     PLUSPLUS = 374,
     MINUSMINUS = 375,
     ARROW = 376,
     IFX = 377
   };
#endif
/* Tokens.  */
#define FINAL 258
#define RANK_SPECIFIER 259
#define IDENTIFIER 260
#define INTEGER_LITERAL 261
#define REAL_LITERAL 262
#define CHARACTER_LITERAL 263
#define STRING_LITERAL 264
#define ABSTRACT 265
#define AS 266
#define BASE 267
#define BOOL 268
#define BREAK 269
#define BYTE 270
#define CASE 271
#define CATCH 272
#define CHAR 273
#define CHECKED 274
#define CLASSX 275
#define CONST 276
#define CONTINUE 277
#define DECIMAL 278
#define DEFAULTX 279
#define DELEGATE 280
#define DO 281
#define DOUBLE 282
#define ELSE 283
#define ENUM 284
#define EVENT 285
#define EXPLICIT 286
#define EXTERN 287
#define FALSE 288
#define FINALLY 289
#define FIXED 290
#define FLOAT 291
#define FOR 292
#define FOREACH 293
#define GOTO 294
#define IF 295
#define IMPLICIT 296
#define IN 297
#define INT 298
#define INTERFACEX 299
#define INTERNAL 300
#define IS 301
#define LOCK 302
#define LONG 303
#define NAMESPACE 304
#define NEW 305
#define NULL_LITERAL 306
#define OBJECTX 307
#define OPERATOR 308
#define OUT 309
#define OVERRIDE 310
#define PARAMS 311
#define PRIVATE 312
#define PROTECTED 313
#define PUBLIC 314
#define READONLY 315
#define REF 316
#define RETURN 317
#define SBYTE 318
#define SEALED 319
#define SHORT 320
#define SIZEOF 321
#define STACKALLOC 322
#define STATIC 323
#define STRING 324
#define STRUCT 325
#define SWITCH 326
#define THIS 327
#define THROW 328
#define TRUE 329
#define TRY 330
#define TYPEOF 331
#define UINT 332
#define ULONG 333
#define UNCHECKED 334
#define UNSAFE 335
#define USHORT 336
#define USING 337
#define VIRTUAL 338
#define VOID 339
#define VOLATILE 340
#define WHILE 341
#define ASSEMBLY 342
#define FIELD 343
#define METHOD 344
#define MODULE 345
#define PARAM 346
#define PROPERTY 347
#define TYPEX 348
#define GET 349
#define SET 350
#define ADD 351
#define REMOVE 352
#define COMMA 353
#define LEFT_BRACKET 354
#define RIGHT_BRACKET 355
#define PLUSEQ 356
#define MINUSEQ 357
#define STAREQ 358
#define DIVEQ 359
#define MODEQ 360
#define XOREQ 361
#define ANDEQ 362
#define OREQ 363
#define LTLT 364
#define GTGT 365
#define GTGTEQ 366
#define LTLTEQ 367
#define EQEQ 368
#define NOTEQ 369
#define LEQ 370
#define GEQ 371
#define ANDAND 372
#define OROR 373
#define PLUSPLUS 374
#define MINUSMINUS 375
#define ARROW 376
#define IFX 377




/* Copy the first part of user declarations.  */
#line 2 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"

#define YYERROR_VERBOSE
#define YYDEBUG 1
	#include <iostream>
	#include "myvalue.h"
	using namespace std;
	#include <FlexLexer.h>
	#include <string.h>
	#include "ErrorRecovery.h"
	#include "AST/allnode.hpp"
	int yylex(void);
	int yyparse();
	 char msgt[128];
	void yyerror(char *);
	MyParser* Myparser = new MyParser();
	FlexLexer* lexer = new yyFlexLexer();
	ErrorRecovery errorRec;
	int* modi = new int[9];
	int modCounter = 0;
	int scopecount=1;
	int return_type;
	class Parser
	{
		public:
		int	parse()
		{
			return yyparse();
		}
	};

	ListNode *tree = new ListNode("StartNode");


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 81 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
typedef union YYSTYPE {
	struct R{
		
		int    myLineNo;
		int    myColno;
		int m; //num of return type
	    double value;
	    char*  str; //identefier 
		int    i; //integeral leteral
		float  f;
		char   c; //CHARACTER_LITERAL
		class Symbol * symbol; //polymorphism for all sympol type
		class Scope * scope; //just for {
		class Node* node;
		}r;

	} YYSTYPE;
/* Line 196 of yacc.c.  */
#line 380 "yacc.cpp"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 219 of yacc.c.  */
#line 392 "yacc.cpp"

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T) && (defined (__STDC__) || defined (__cplusplus))
# include <stddef.h> /* INFRINGES ON USER NAME SPACE */
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if defined (__STDC__) || defined (__cplusplus)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     define YYINCLUDED_STDLIB_H
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2005 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM ((YYSIZE_T) -1)
#  endif
#  ifdef __cplusplus
extern "C" {
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if (! defined (malloc) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if (! defined (free) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifdef __cplusplus
}
#  endif
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE))			\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  12
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3165

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  144
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  245
/* YYNRULES -- Number of rules. */
#define YYNRULES  553
/* YYNRULES -- Number of states. */
#define YYNSTATES  956

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   377

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   130,     2,     2,     2,   135,   129,     2,
     125,   127,   126,   132,     2,   133,   128,   134,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,   141,   143,
     136,   142,   137,   140,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,   138,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   123,   139,   124,   131,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     5,     8,    10,    12,    15,    17,    19,
      21,    23,    25,    27,    29,    31,    33,    35,    37,    39,
      41,    43,    45,    47,    49,    51,    53,    55,    57,    59,
      61,    63,    65,    67,    69,    71,    73,    75,    77,    79,
      81,    83,    85,    88,    91,    94,    97,   100,   101,   104,
     106,   108,   110,   114,   116,   119,   122,   124,   126,   128,
     130,   132,   134,   136,   138,   140,   142,   144,   146,   148,
     150,   154,   158,   162,   166,   171,   176,   177,   179,   184,
     189,   190,   192,   194,   198,   200,   204,   209,   212,   215,
     217,   223,   231,   235,   236,   238,   243,   248,   253,   258,
     262,   265,   270,   272,   274,   276,   278,   280,   282,   285,
     288,   290,   293,   296,   298,   301,   304,   307,   309,   311,
     313,   318,   324,   331,   337,   343,   349,   350,   352,   354,
     357,   359,   361,   363,   367,   371,   375,   377,   381,   385,
     387,   391,   395,   397,   401,   405,   409,   413,   417,   421,
     423,   427,   431,   433,   437,   439,   443,   445,   449,   451,
     455,   457,   461,   463,   469,   473,   477,   481,   483,   485,
     487,   489,   491,   493,   495,   497,   499,   501,   503,   505,
     507,   509,   511,   513,   515,   517,   519,   521,   523,   525,
     527,   529,   531,   533,   535,   537,   539,   543,   544,   546,
     548,   551,   553,   557,   560,   563,   566,   568,   572,   574,
     578,   580,   582,   584,   590,   594,   596,   600,   604,   606,
     609,   611,   613,   615,   617,   619,   621,   623,   625,   627,
     633,   639,   647,   653,   661,   662,   664,   666,   669,   672,
     674,   677,   681,   684,   686,   688,   690,   692,   695,   701,
     707,   715,   723,   733,   734,   736,   737,   739,   740,   742,
     744,   746,   748,   750,   752,   756,   765,   767,   769,   771,
     773,   775,   778,   781,   785,   790,   794,   798,   799,   801,
     805,   809,   811,   815,   820,   822,   825,   832,   839,   842,
     843,   845,   848,   851,   854,   860,   866,   868,   870,   877,
     879,   883,   887,   890,   893,   894,   896,   897,   899,   900,
     902,   908,   909,   911,   913,   916,   919,   923,   928,   930,
     933,   935,   937,   943,   947,   949,   952,   954,   956,   958,
     960,   962,   964,   966,   967,   969,   971,   974,   976,   978,
     980,   982,   984,   986,   988,   990,   992,   994,   996,   998,
    1000,  1002,  1004,  1005,  1016,  1017,  1027,  1028,  1039,  1040,
    1042,  1045,  1048,  1053,  1055,  1059,  1060,  1062,  1064,  1067,
    1069,  1071,  1073,  1075,  1077,  1079,  1081,  1083,  1085,  1087,
    1094,  1100,  1106,  1107,  1119,  1128,  1137,  1138,  1150,  1151,
    1153,  1155,  1157,  1159,  1163,  1165,  1167,  1172,  1173,  1175,
    1177,  1182,  1192,  1195,  1198,  1199,  1201,  1202,  1204,  1210,
    1216,  1218,  1220,  1227,  1238,  1241,  1244,  1250,  1256,  1265,
    1271,  1277,  1280,  1285,  1287,  1289,  1297,  1308,  1310,  1312,
    1314,  1316,  1318,  1320,  1322,  1324,  1326,  1328,  1330,  1332,
    1334,  1336,  1338,  1340,  1342,  1344,  1346,  1348,  1350,  1352,
    1360,  1368,  1379,  1388,  1389,  1391,  1397,  1403,  1411,  1413,
    1415,  1416,  1426,  1427,  1429,  1432,  1435,  1436,  1438,  1440,
    1443,  1445,  1447,  1449,  1451,  1453,  1455,  1457,  1459,  1461,
    1465,  1470,  1471,  1473,  1475,  1479,  1487,  1488,  1490,  1493,
    1497,  1498,  1500,  1502,  1505,  1507,  1509,  1511,  1513,  1522,
    1531,  1532,  1534,  1544,  1557,  1561,  1565,  1572,  1579,  1586,
    1588,  1591,  1599,  1600,  1602,  1605,  1609,  1614,  1615,  1617,
    1619,  1623,  1626,  1631,  1640,  1642,  1644,  1647,  1654,  1662,
    1663,  1665,  1668,  1670,  1672,  1674,  1676,  1678,  1680,  1682,
    1684,  1686,  1688,  1692,  1695,  1696,  1698,  1700,  1704,  1705,
    1706,  1707,  1708,  1709
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short int yyrhs[] =
{
     274,     0,    -1,   123,    -1,   123,   145,    -1,   124,    -1,
     125,    -1,   125,   147,    -1,   149,    -1,     6,    -1,     7,
      -1,     8,    -1,     9,    -1,    51,    -1,    74,    -1,    33,
      -1,   280,    -1,   280,    -1,   153,    -1,   161,    -1,   154,
      -1,   151,    -1,   155,    -1,   159,    -1,   160,    -1,   156,
      -1,    13,    -1,   157,    -1,   158,    -1,    23,    -1,    63,
      -1,    15,    -1,    65,    -1,    81,    -1,    43,    -1,    77,
      -1,    48,    -1,    78,    -1,    18,    -1,    36,    -1,    27,
      -1,    52,    -1,    69,    -1,   152,   126,    -1,    84,   126,
      -1,   161,   163,    -1,   154,   163,    -1,   280,   163,    -1,
      -1,   163,   162,    -1,     4,    -1,   212,    -1,   166,    -1,
     165,    98,   166,    -1,   212,    -1,    61,   164,    -1,    54,
     164,    -1,   169,    -1,   168,    -1,   148,    -1,   182,    -1,
     170,    -1,   171,    -1,   173,    -1,   176,    -1,   177,    -1,
     180,    -1,   184,    -1,   189,    -1,   185,    -1,   186,    -1,
     125,   212,   127,    -1,   167,   128,     5,    -1,   155,   128,
       5,    -1,   159,   128,     5,    -1,   168,   125,   172,   127,
      -1,   280,   125,   172,   127,    -1,    -1,   165,    -1,   167,
      99,   175,   100,    -1,   280,    99,   175,   100,    -1,    -1,
     175,    -1,   212,    -1,   175,    98,   212,    -1,    72,    -1,
      12,   128,     5,    -1,    12,    99,   175,   100,    -1,   190,
     119,    -1,   190,   120,    -1,   181,    -1,    50,   152,   125,
     172,   127,    -1,    50,   153,    99,   175,   100,   162,   183,
      -1,    50,   161,   347,    -1,    -1,   347,    -1,    76,   147,
     152,   127,    -1,    76,   125,    84,   127,    -1,    19,   125,
     212,   127,    -1,    79,   125,   212,   127,    -1,   190,   121,
       5,    -1,   129,   194,    -1,    66,   147,   152,   127,    -1,
     167,    -1,   280,    -1,   178,    -1,   179,    -1,   187,    -1,
     190,    -1,   130,   194,    -1,   131,   194,    -1,   195,    -1,
     119,   194,    -1,   120,   194,    -1,   191,    -1,   132,   194,
      -1,   133,   194,    -1,   126,   194,    -1,   192,    -1,   193,
      -1,   188,    -1,   125,   212,   127,   191,    -1,   125,   199,
     126,   127,   194,    -1,   125,   280,   163,   196,   127,   194,
      -1,   125,   155,   196,   127,   194,    -1,   125,   159,   196,
     127,   194,    -1,   125,    84,   196,   127,   194,    -1,    -1,
     197,    -1,   198,    -1,   197,   198,    -1,   163,    -1,   126,
      -1,   194,    -1,   199,   126,   194,    -1,   199,   134,   194,
      -1,   199,   135,   194,    -1,   199,    -1,   200,   132,   199,
      -1,   200,   133,   199,    -1,   200,    -1,   201,   109,   200,
      -1,   201,   110,   200,    -1,   201,    -1,   202,   136,   201,
      -1,   202,   137,   201,    -1,   202,   115,   201,    -1,   202,
     116,   201,    -1,   202,    46,   152,    -1,   202,    11,   152,
      -1,   202,    -1,   203,   113,   202,    -1,   203,   114,   202,
      -1,   203,    -1,   204,   129,   203,    -1,   204,    -1,   205,
     138,   204,    -1,   205,    -1,   206,   139,   205,    -1,   206,
      -1,   207,   117,   206,    -1,   207,    -1,   208,   118,   207,
      -1,   208,    -1,   208,   140,   212,   141,   212,    -1,   194,
     211,   212,    -1,   194,   101,   212,    -1,   194,   142,   212,
      -1,   102,    -1,   103,    -1,   104,    -1,   105,    -1,   106,
      -1,   107,    -1,   108,    -1,   111,    -1,   112,    -1,   209,
      -1,   210,    -1,   212,    -1,   212,    -1,   221,    -1,   222,
      -1,   216,    -1,   217,    -1,   220,    -1,   231,    -1,   233,
      -1,   241,    -1,   254,    -1,   261,    -1,   266,    -1,   267,
      -1,   268,    -1,   269,    -1,   242,    -1,   271,    -1,   145,
     218,   146,    -1,    -1,   219,    -1,   215,    -1,   219,   215,
      -1,   143,    -1,     5,   141,   215,    -1,   223,   143,    -1,
     228,   143,    -1,   152,   224,    -1,   225,    -1,   224,    98,
     225,    -1,     5,    -1,     5,   142,   226,    -1,   212,    -1,
     347,    -1,   227,    -1,    67,   152,    99,   212,   100,    -1,
      21,   152,   229,    -1,   230,    -1,   229,    98,   230,    -1,
       5,   142,   213,    -1,     5,    -1,   232,   143,    -1,   171,
      -1,   181,    -1,   210,    -1,   178,    -1,   179,    -1,   192,
      -1,   193,    -1,   234,    -1,   235,    -1,    40,   125,   214,
     127,   216,    -1,    40,   125,   214,   100,   216,    -1,    40,
     125,   214,   127,   216,    28,   216,    -1,    40,   125,   210,
     127,   216,    -1,    71,   147,   212,   127,   145,   236,   146,
      -1,    -1,   237,    -1,   238,    -1,   237,   238,    -1,   239,
     219,    -1,   240,    -1,   239,   240,    -1,    16,   213,   141,
      -1,    24,   141,    -1,   243,    -1,   244,    -1,   245,    -1,
     253,    -1,    80,   217,    -1,    86,   125,   214,   127,   216,
      -1,    86,   125,   210,   127,   216,    -1,    26,   216,    86,
     125,   214,   127,   143,    -1,    26,   216,    86,   125,   210,
     127,   143,    -1,    37,   147,   246,   143,   247,   143,   248,
     127,   216,    -1,    -1,   249,    -1,    -1,   250,    -1,    -1,
     251,    -1,   223,    -1,   252,    -1,   214,    -1,   252,    -1,
     232,    -1,   252,    98,   232,    -1,    38,   147,   152,     5,
      42,   212,   127,   216,    -1,   255,    -1,   256,    -1,   257,
      -1,   258,    -1,   260,    -1,    14,   143,    -1,    22,   143,
      -1,    39,     5,   143,    -1,    39,    16,   213,   143,    -1,
      39,    24,   143,    -1,    62,   259,   143,    -1,    -1,   212,
      -1,    73,   259,   143,    -1,    75,   217,   262,    -1,   262,
      -1,    75,   217,   265,    -1,    75,   217,   262,   265,    -1,
     263,    -1,   262,   263,    -1,    17,   147,   159,   264,   127,
     217,    -1,    17,   147,   151,   264,   127,   217,    -1,    17,
     217,    -1,    -1,     5,    -1,    34,   217,    -1,    19,   217,
      -1,    79,   217,    -1,    47,   125,   212,   127,   216,    -1,
      82,   125,   270,   127,   216,    -1,   223,    -1,   212,    -1,
      35,   125,   152,   272,   127,   216,    -1,   273,    -1,   272,
      98,   273,    -1,     5,   142,   212,    -1,   275,   276,    -1,
     275,   287,    -1,    -1,   283,    -1,    -1,   372,    -1,    -1,
     287,    -1,   276,    49,   280,   282,   279,    -1,    -1,   143,
      -1,     5,    -1,   281,     5,    -1,     5,   128,    -1,   281,
       5,   128,    -1,   145,   275,   277,   146,    -1,   284,    -1,
     283,   284,    -1,   285,    -1,   286,    -1,    82,     5,   142,
     280,   143,    -1,    82,   150,   143,    -1,   288,    -1,   287,
     288,    -1,   278,    -1,   289,    -1,   293,    -1,   339,    -1,
     350,    -1,   364,    -1,   371,    -1,    -1,   291,    -1,   292,
      -1,   291,   292,    -1,    10,    -1,    32,    -1,    45,    -1,
      50,    -1,    55,    -1,    57,    -1,    58,    -1,    59,    -1,
      60,    -1,    64,    -1,    68,    -1,    80,    -1,    83,    -1,
      85,    -1,     3,    -1,    -1,   276,   290,    20,     5,   297,
     145,   294,   300,   146,   279,    -1,    -1,   276,   290,    20,
     297,   145,   295,   300,   146,   279,    -1,    -1,   276,   290,
       5,     5,   297,   145,   296,   300,   146,   279,    -1,    -1,
     298,    -1,   141,   159,    -1,   141,   299,    -1,   141,   159,
      98,   299,    -1,   151,    -1,   299,    98,   151,    -1,    -1,
     301,    -1,   302,    -1,   301,   302,    -1,   303,    -1,   304,
      -1,   305,    -1,   315,    -1,   322,    -1,   326,    -1,   329,
      -1,   334,    -1,   337,    -1,   289,    -1,   276,   290,    21,
     152,   229,   143,    -1,   276,   290,    21,   229,   143,    -1,
     276,   290,   152,   224,   143,    -1,    -1,   276,   290,   152,
     280,   147,   308,   127,   145,   306,   218,   146,    -1,   276,
     290,   152,   280,   147,   308,   127,   143,    -1,   276,   290,
      84,   280,   147,   308,   127,   143,    -1,    -1,   276,   290,
      84,   280,   147,   308,   127,   145,   307,   218,   146,    -1,
      -1,   310,    -1,   152,    -1,    84,    -1,   311,    -1,   310,
      98,   311,    -1,   312,    -1,   314,    -1,   276,   313,   152,
       5,    -1,    -1,    61,    -1,    54,    -1,   276,    56,   152,
       5,    -1,   276,   290,   152,   280,   387,   145,   316,   146,
     388,    -1,   319,   317,    -1,   320,   318,    -1,    -1,   320,
      -1,    -1,   319,    -1,   276,    94,   388,   321,   387,    -1,
     276,    95,   388,   321,   387,    -1,   217,    -1,   143,    -1,
     276,   290,    30,   152,   224,   143,    -1,   276,   290,    30,
     152,   280,   385,   145,   323,   146,   386,    -1,   324,   325,
      -1,   325,   324,    -1,   276,    96,   386,   217,   385,    -1,
     276,    97,   386,   217,   385,    -1,   276,   290,   327,   387,
     145,   316,   146,   388,    -1,   152,    72,    99,   310,   100,
      -1,   152,   328,    99,   310,   100,    -1,   281,    72,    -1,
     276,   290,   330,   338,    -1,   331,    -1,   333,    -1,   152,
      53,   332,   125,   152,     5,   127,    -1,   152,    53,   332,
     125,   152,     5,    98,   152,     5,   127,    -1,   132,    -1,
     133,    -1,   130,    -1,   131,    -1,   119,    -1,   120,    -1,
      74,    -1,    33,    -1,   126,    -1,   134,    -1,   135,    -1,
     129,    -1,   139,    -1,   138,    -1,   109,    -1,   110,    -1,
     113,    -1,   114,    -1,   137,    -1,   136,    -1,   116,    -1,
     115,    -1,    41,    53,   152,   125,   152,     5,   127,    -1,
      31,    53,   152,   125,   152,     5,   127,    -1,   276,   290,
       5,   147,   308,   127,   335,   145,   218,   146,    -1,   276,
     290,     5,   147,   308,   127,   335,   143,    -1,    -1,   336,
      -1,   141,    12,   147,   172,   127,    -1,   141,    72,   147,
     172,   127,    -1,   276,   290,   131,     5,   125,   127,   217,
      -1,   217,    -1,   143,    -1,    -1,   276,   290,    70,     5,
     341,   145,   340,   343,   279,    -1,    -1,   342,    -1,   141,
     299,    -1,   344,   146,    -1,    -1,   345,    -1,   346,    -1,
     345,   346,    -1,   303,    -1,   304,    -1,   305,    -1,   315,
      -1,   322,    -1,   326,    -1,   329,    -1,   334,    -1,   289,
      -1,   145,   348,   146,    -1,   145,   349,    98,   146,    -1,
      -1,   349,    -1,   226,    -1,   349,    98,   226,    -1,   276,
     290,    44,     5,   351,   353,   279,    -1,    -1,   352,    -1,
     141,   299,    -1,   145,   354,   146,    -1,    -1,   355,    -1,
     356,    -1,   355,   356,    -1,   357,    -1,   359,    -1,   362,
      -1,   360,    -1,   276,   358,   152,     5,   125,   308,   127,
     363,    -1,   276,   358,    84,     5,   125,   308,   127,   363,
      -1,    -1,    50,    -1,   276,   358,   152,     5,   387,   145,
     361,   146,   388,    -1,   276,   358,   152,    72,    99,   310,
     100,   387,   145,   361,   146,   388,    -1,   276,    94,   363,
      -1,   276,    95,   363,    -1,   276,    94,   363,   276,    95,
     363,    -1,   276,    95,   363,   276,    94,   363,    -1,   276,
     358,    30,   152,     5,   363,    -1,   143,    -1,   145,   146,
      -1,   276,   290,    29,     5,   365,   367,   279,    -1,    -1,
     366,    -1,   141,   157,    -1,   145,   368,   146,    -1,   145,
     369,    98,   146,    -1,    -1,   369,    -1,   370,    -1,   369,
      98,   370,    -1,   276,     5,    -1,   276,     5,   142,   213,
      -1,   276,   290,    25,   309,     5,   125,   308,   127,    -1,
     373,    -1,   374,    -1,   373,   374,    -1,   383,    99,   375,
     378,   100,   384,    -1,   383,    99,   375,   378,    98,   100,
     384,    -1,    -1,   376,    -1,   377,   141,    -1,    87,    -1,
      88,    -1,    30,    -1,    89,    -1,    90,    -1,    91,    -1,
      92,    -1,    62,    -1,    93,    -1,   379,    -1,   378,    98,
     379,    -1,   381,   380,    -1,    -1,   382,    -1,   151,    -1,
     125,   174,   127,    -1,    -1,    -1,    -1,    -1,    -1,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,   101,   101,   102,   104,   106,   107,   109,   110,   114,
     116,   117,   119,   122,   123,   129,   132,   136,   137,   140,
     141,   144,   145,   146,   149,   150,   156,   157,   158,   161,
     165,   169,   173,   177,   181,   185,   189,   193,   199,   203,
     209,   209,   212,   216,   222,   223,   224,   227,   229,   232,
     236,   240,   244,   248,   249,   250,   253,   254,   257,   258,
     259,   260,   261,   262,   263,   264,   265,   266,   267,   268,
     271,   274,   275,   276,   279,   280,   283,   284,   287,   288,
     290,   292,   295,   296,   299,   302,   303,   306,   309,   312,
     315,   318,   319,   321,   323,   326,   327,   330,   333,   336,
     339,   342,   345,   346,   347,   348,   349,   352,   353,   354,
     355,   358,   364,   367,   368,   369,   370,   371,   372,   373,
     377,   378,   379,   380,   381,   382,   384,   386,   389,   390,
     393,   394,   397,   398,   399,   400,   403,   404,   415,   418,
     419,   420,   423,   424,   425,   426,   427,   428,   429,   432,
     433,   434,   437,   438,   441,   442,   445,   446,   449,   450,
     453,   454,   457,   458,   461,   468,   474,   481,   481,   481,
     481,   482,   482,   482,   482,   482,   485,   486,   489,   492,
     496,   497,   498,   501,   502,   503,   504,   505,   506,   507,
     508,   509,   510,   511,   512,   513,   516,   519,   520,   524,
     530,   536,   539,   542,   543,   546,   554,   560,   576,   583,
     588,   589,   590,   593,   596,   602,   603,   616,   617,   624,
     627,   628,   629,   630,   631,   632,   633,   636,   637,   640,
     647,   650,   656,   660,   667,   669,   672,   673,   676,   679,
     680,   683,   684,   687,   688,   689,   690,   693,   696,   702,
     705,   706,   709,   717,   719,   722,   723,   725,   727,   730,
     731,   734,   737,   740,   741,   744,   747,   748,   749,   750,
     751,   754,   757,   760,   761,   762,   765,   767,   769,   772,
     775,   779,   780,   781,   785,   786,   789,   790,   791,   793,
     795,   798,   801,   804,   807,   810,   813,   814,   818,   821,
     822,   825,   828,   829,   833,   835,   837,   839,   841,   843,
     846,   848,   850,   854,   859,   872,   873,   888,   893,   894,
     897,   898,   901,   904,   907,   914,   920,   921,   924,   925,
     926,   927,   928,   932,   934,   937,   938,   941,   944,   947,
     950,   953,   956,   959,   962,   965,   968,   971,   974,   977,
     980,   982,   987,   987,  1001,  1001,  1010,  1010,  1020,  1021,
    1024,  1025,  1026,  1029,  1030,  1033,  1034,  1038,  1042,  1045,
    1046,  1047,  1050,  1054,  1058,  1062,  1066,  1070,  1074,  1077,
    1084,  1087,  1097,  1096,  1108,  1114,  1121,  1120,  1131,  1132,
    1135,  1136,  1142,  1146,  1167,  1168,  1171,  1177,  1179,  1180,
    1183,  1190,  1196,  1197,  1199,  1201,  1203,  1205,  1208,  1214,
    1220,  1221,  1224,  1225,  1231,  1232,  1235,  1241,  1247,  1253,
    1255,  1258,  1262,  1265,  1266,  1269,  1270,  1273,  1273,  1274,
    1274,  1274,  1274,  1274,  1274,  1275,  1275,  1275,  1275,  1275,
    1275,  1276,  1276,  1276,  1276,  1276,  1276,  1276,  1276,  1279,
    1280,  1283,  1287,  1293,  1295,  1298,  1299,  1304,  1307,  1308,
    1314,  1314,  1317,  1319,  1322,  1325,  1327,  1329,  1332,  1333,
    1336,  1337,  1338,  1339,  1340,  1341,  1342,  1343,  1344,  1348,
    1349,  1351,  1353,  1356,  1357,  1362,  1364,  1366,  1369,  1372,
    1374,  1376,  1379,  1380,  1383,  1384,  1385,  1386,  1389,  1390,
    1392,  1394,  1397,  1403,  1411,  1412,  1413,  1414,  1417,  1421,
    1422,  1427,  1429,  1431,  1434,  1437,  1438,  1440,  1442,  1445,
    1446,  1449,  1450,  1455,  1460,  1463,  1464,  1467,  1468,  1470,
    1472,  1475,  1478,  1479,  1480,  1481,  1482,  1483,  1484,  1485,
    1486,  1489,  1490,  1493,  1495,  1497,  1500,  1503,  1510,  1513,
    1516,  1519,  1522,  1525
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "FINAL", "RANK_SPECIFIER", "IDENTIFIER",
  "INTEGER_LITERAL", "REAL_LITERAL", "CHARACTER_LITERAL", "STRING_LITERAL",
  "ABSTRACT", "AS", "BASE", "BOOL", "BREAK", "BYTE", "CASE", "CATCH",
  "CHAR", "CHECKED", "CLASSX", "CONST", "CONTINUE", "DECIMAL", "DEFAULTX",
  "DELEGATE", "DO", "DOUBLE", "ELSE", "ENUM", "EVENT", "EXPLICIT",
  "EXTERN", "FALSE", "FINALLY", "FIXED", "FLOAT", "FOR", "FOREACH", "GOTO",
  "IF", "IMPLICIT", "IN", "INT", "INTERFACEX", "INTERNAL", "IS", "LOCK",
  "LONG", "NAMESPACE", "NEW", "NULL_LITERAL", "OBJECTX", "OPERATOR", "OUT",
  "OVERRIDE", "PARAMS", "PRIVATE", "PROTECTED", "PUBLIC", "READONLY",
  "REF", "RETURN", "SBYTE", "SEALED", "SHORT", "SIZEOF", "STACKALLOC",
  "STATIC", "STRING", "STRUCT", "SWITCH", "THIS", "THROW", "TRUE", "TRY",
  "TYPEOF", "UINT", "ULONG", "UNCHECKED", "UNSAFE", "USHORT", "USING",
  "VIRTUAL", "VOID", "VOLATILE", "WHILE", "ASSEMBLY", "FIELD", "METHOD",
  "MODULE", "PARAM", "PROPERTY", "TYPEX", "GET", "SET", "ADD", "REMOVE",
  "\",\"", "\"[\"", "RIGHT_BRACKET", "PLUSEQ", "MINUSEQ", "STAREQ",
  "DIVEQ", "MODEQ", "XOREQ", "ANDEQ", "OREQ", "LTLT", "GTGT", "GTGTEQ",
  "LTLTEQ", "EQEQ", "NOTEQ", "LEQ", "GEQ", "ANDAND", "OROR", "PLUSPLUS",
  "MINUSMINUS", "ARROW", "IFX", "'{'", "'}'", "'('", "'*'", "')'", "'.'",
  "'&'", "'!'", "'~'", "'+'", "'-'", "'/'", "'%'", "'<'", "'>'", "'^'",
  "'|'", "'?'", "':'", "'='", "';'", "$accept", "open_bl", "close_bl",
  "open_ar", "literal", "boolean_literal", "namespace_name", "type_name",
  "type", "non_array_type", "simple_type", "primitive_type",
  "numeric_type", "integral_type", "floating_point_type", "class_type",
  "pointer_type", "array_type", "rank_specifiers_opt", "rank_specifier",
  "variable_reference", "argument_list", "argument", "primary_expression",
  "primary_expression_no_parenthesis", "parenthesized_expression",
  "member_access", "invocation_expression", "argument_list_opt",
  "element_access", "expression_list_opt", "expression_list",
  "this_access", "base_access", "post_increment_expression",
  "post_decrement_expression", "new_expression",
  "object_creation_expression", "array_creation_expression",
  "array_initializer_opt", "typeof_expression", "checked_expression",
  "unchecked_expression", "pointer_member_access", "addressof_expression",
  "sizeof_expression", "postfix_expression",
  "unary_expression_not_plusminus", "pre_increment_expression",
  "pre_decrement_expression", "unary_expression", "cast_expression",
  "type_quals_opt", "type_quals", "type_qual", "multiplicative_expression",
  "additive_expression", "shift_expression", "relational_expression",
  "equality_expression", "and_expression", "exclusive_or_expression",
  "inclusive_or_expression", "conditional_and_expression",
  "conditional_or_expression", "conditional_expression", "assignment",
  "assignment_operator", "expression", "constant_expression",
  "boolean_expression", "statement", "embedded_statement", "block",
  "statement_list_opt", "statement_list", "empty_statement",
  "labeled_statement", "declaration_statement",
  "local_variable_declaration", "variable_declarators",
  "variable_declarator", "variable_initializer", "stackalloc_initializer",
  "local_constant_declaration", "constant_declarators",
  "constant_declarator", "expression_statement", "statement_expression",
  "selection_statement", "if_statement", "switch_statement",
  "switch_sections_opt", "switch_sections", "switch_section",
  "switch_labels", "switch_label", "iteration_statement",
  "unsafe_statement", "while_statement", "do_statement", "for_statement",
  "for_initializer_opt", "for_condition_opt", "for_iterator_opt",
  "for_initializer", "for_condition", "for_iterator",
  "statement_expression_list", "foreach_statement", "jump_statement",
  "break_statement", "continue_statement", "goto_statement",
  "return_statement", "expression_opt", "throw_statement", "try_statement",
  "catch_clauses", "catch_clause", "identifier_opt", "finally_clause",
  "checked_statement", "unchecked_statement", "lock_statement",
  "using_statement", "resource_acquisition", "fixed_statement",
  "fixed_pointer_declarators", "fixed_pointer_declarator",
  "compilation_unit", "using_directives_opt", "attributes_opt",
  "namespace_member_declarations_opt", "namespace_declaration",
  "comma_opt", "qualified_identifier", "qualifier", "namespace_body",
  "using_directives", "using_directive", "using_alias_directive",
  "using_namespace_directive", "namespace_member_declarations",
  "namespace_member_declaration", "type_declaration", "modifiers_opt",
  "modifiers", "modifier", "class_declaration", "@1", "@2", "@3",
  "class_base_opt", "class_base", "interface_type_list",
  "class_member_declarations_opt", "class_member_declarations",
  "class_member_declaration", "constant_declaration", "field_declaration",
  "method_declaration", "@4", "@5", "formal_parameter_list_opt",
  "return_type", "formal_parameter_list", "formal_parameter",
  "fixed_parameter", "parameter_modifier_opt", "parameter_array",
  "property_declaration", "accessor_declarations",
  "set_accessor_declaration_opt", "get_accessor_declaration_opt",
  "get_accessor_declaration", "set_accessor_declaration", "accessor_body",
  "event_declaration", "event_accessor_declarations",
  "add_accessor_declaration", "remove_accessor_declaration",
  "indexer_declaration", "indexer_declarator", "qualified_this",
  "operator_declaration", "operator_declarator",
  "overloadable_operator_declarator", "overloadable_operator",
  "conversion_operator_declarator", "constructor_declaration",
  "constructor_initializer_opt", "constructor_initializer",
  "destructor_declaration", "operator_body", "struct_declaration", "@6",
  "struct_interfaces_opt", "struct_interfaces", "struct_body",
  "struct_member_declarations_opt", "struct_member_declarations",
  "struct_member_declaration", "array_initializer",
  "variable_initializer_list_opt", "variable_initializer_list",
  "interface_declaration", "interface_base_opt", "interface_base",
  "interface_body", "interface_member_declarations_opt",
  "interface_member_declarations", "interface_member_declaration",
  "interface_method_declaration", "new_opt",
  "interface_property_declaration", "interface_indexer_declaration",
  "interface_accessors", "interface_event_declaration",
  "interface_empty_body", "enum_declaration", "enum_base_opt", "enum_base",
  "enum_body", "enum_member_declarations_opt", "enum_member_declarations",
  "enum_member_declaration", "delegate_declaration", "attributes",
  "attribute_sections", "attribute_section",
  "attribute_target_specifier_opt", "attribute_target_specifier",
  "attribute_target", "attribute_list", "attribute",
  "attribute_arguments_opt", "attribute_name", "attribute_arguments",
  "ENTER_attrib", "EXIT_attrib", "ENTER_accessor_decl",
  "EXIT_accessor_decl", "ENTER_getset", "EXIT_getset", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   123,   125,    40,    42,    41,    46,    38,
      33,   126,    43,    45,    47,    37,    60,    62,    94,   124,
      63,    58,    61,    59
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned short int yyr1[] =
{
       0,   144,   145,   145,   146,   147,   147,   148,   148,   148,
     148,   148,   148,   149,   149,   150,   151,   152,   152,   153,
     153,   154,   154,   154,   155,   155,   156,   156,   156,   157,
     157,   157,   157,   157,   157,   157,   157,   157,   158,   158,
     159,   159,   160,   160,   161,   161,   161,   162,   162,   163,
     164,   165,   165,   166,   166,   166,   167,   167,   168,   168,
     168,   168,   168,   168,   168,   168,   168,   168,   168,   168,
     169,   170,   170,   170,   171,   171,   172,   172,   173,   173,
     174,   174,   175,   175,   176,   177,   177,   178,   179,   180,
     181,   182,   182,   183,   183,   184,   184,   185,   186,   187,
     188,   189,   190,   190,   190,   190,   190,   191,   191,   191,
     191,   192,   193,   194,   194,   194,   194,   194,   194,   194,
     195,   195,   195,   195,   195,   195,   196,   196,   197,   197,
     198,   198,   199,   199,   199,   199,   200,   200,   200,   201,
     201,   201,   202,   202,   202,   202,   202,   202,   202,   203,
     203,   203,   204,   204,   205,   205,   206,   206,   207,   207,
     208,   208,   209,   209,   210,   210,   210,   211,   211,   211,
     211,   211,   211,   211,   211,   211,   212,   212,   213,   214,
     215,   215,   215,   216,   216,   216,   216,   216,   216,   216,
     216,   216,   216,   216,   216,   216,   217,   218,   218,   219,
     219,   220,   221,   222,   222,   223,   224,   224,   225,   225,
     226,   226,   226,   227,   228,   229,   229,   230,   230,   231,
     232,   232,   232,   232,   232,   232,   232,   233,   233,   234,
     234,   234,   234,   235,   236,   236,   237,   237,   238,   239,
     239,   240,   240,   241,   241,   241,   241,   242,   243,   243,
     244,   244,   245,   246,   246,   247,   247,   248,   248,   249,
     249,   250,   251,   252,   252,   253,   254,   254,   254,   254,
     254,   255,   256,   257,   257,   257,   258,   259,   259,   260,
     261,   261,   261,   261,   262,   262,   263,   263,   263,   264,
     264,   265,   266,   267,   268,   269,   270,   270,   271,   272,
     272,   273,   274,   274,   275,   275,   276,   276,   277,   277,
     278,   279,   279,   280,   280,   281,   281,   282,   283,   283,
     284,   284,   285,   286,   287,   287,   288,   288,   289,   289,
     289,   289,   289,   290,   290,   291,   291,   292,   292,   292,
     292,   292,   292,   292,   292,   292,   292,   292,   292,   292,
     292,   292,   294,   293,   295,   293,   296,   293,   297,   297,
     298,   298,   298,   299,   299,   300,   300,   301,   301,   302,
     302,   302,   302,   302,   302,   302,   302,   302,   302,   303,
     303,   304,   306,   305,   305,   305,   307,   305,   308,   308,
     309,   309,   310,   310,   311,   311,   312,   313,   313,   313,
     314,   315,   316,   316,   317,   317,   318,   318,   319,   320,
     321,   321,   322,   322,   323,   323,   324,   325,   326,   327,
     327,   328,   329,   330,   330,   331,   331,   332,   332,   332,
     332,   332,   332,   332,   332,   332,   332,   332,   332,   332,
     332,   332,   332,   332,   332,   332,   332,   332,   332,   333,
     333,   334,   334,   335,   335,   336,   336,   337,   338,   338,
     340,   339,   341,   341,   342,   343,   344,   344,   345,   345,
     346,   346,   346,   346,   346,   346,   346,   346,   346,   347,
     347,   348,   348,   349,   349,   350,   351,   351,   352,   353,
     354,   354,   355,   355,   356,   356,   356,   356,   357,   357,
     358,   358,   359,   360,   361,   361,   361,   361,   362,   363,
     363,   364,   365,   365,   366,   367,   367,   368,   368,   369,
     369,   370,   370,   371,   372,   373,   373,   374,   374,   375,
     375,   376,   377,   377,   377,   377,   377,   377,   377,   377,
     377,   378,   378,   379,   380,   380,   381,   382,   383,   384,
     385,   386,   387,   388
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     2,     1,     1,     2,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     2,     2,     2,     2,     0,     2,     1,
       1,     1,     3,     1,     2,     2,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     3,     3,     3,     4,     4,     0,     1,     4,     4,
       0,     1,     1,     3,     1,     3,     4,     2,     2,     1,
       5,     7,     3,     0,     1,     4,     4,     4,     4,     3,
       2,     4,     1,     1,     1,     1,     1,     1,     2,     2,
       1,     2,     2,     1,     2,     2,     2,     1,     1,     1,
       4,     5,     6,     5,     5,     5,     0,     1,     1,     2,
       1,     1,     1,     3,     3,     3,     1,     3,     3,     1,
       3,     3,     1,     3,     3,     3,     3,     3,     3,     1,
       3,     3,     1,     3,     1,     3,     1,     3,     1,     3,
       1,     3,     1,     5,     3,     3,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     3,     0,     1,     1,
       2,     1,     3,     2,     2,     2,     1,     3,     1,     3,
       1,     1,     1,     5,     3,     1,     3,     3,     1,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     5,
       5,     7,     5,     7,     0,     1,     1,     2,     2,     1,
       2,     3,     2,     1,     1,     1,     1,     2,     5,     5,
       7,     7,     9,     0,     1,     0,     1,     0,     1,     1,
       1,     1,     1,     1,     3,     8,     1,     1,     1,     1,
       1,     2,     2,     3,     4,     3,     3,     0,     1,     3,
       3,     1,     3,     4,     1,     2,     6,     6,     2,     0,
       1,     2,     2,     2,     5,     5,     1,     1,     6,     1,
       3,     3,     2,     2,     0,     1,     0,     1,     0,     1,
       5,     0,     1,     1,     2,     2,     3,     4,     1,     2,
       1,     1,     5,     3,     1,     2,     1,     1,     1,     1,
       1,     1,     1,     0,     1,     1,     2,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,    10,     0,     9,     0,    10,     0,     1,
       2,     2,     4,     1,     3,     0,     1,     1,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     6,
       5,     5,     0,    11,     8,     8,     0,    11,     0,     1,
       1,     1,     1,     3,     1,     1,     4,     0,     1,     1,
       4,     9,     2,     2,     0,     1,     0,     1,     5,     5,
       1,     1,     6,    10,     2,     2,     5,     5,     8,     5,
       5,     2,     4,     1,     1,     7,    10,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     7,
       7,    10,     8,     0,     1,     5,     5,     7,     1,     1,
       0,     9,     0,     1,     2,     2,     0,     1,     1,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     3,
       4,     0,     1,     1,     3,     7,     0,     1,     2,     3,
       0,     1,     1,     2,     1,     1,     1,     1,     8,     8,
       0,     1,     9,    12,     3,     3,     6,     6,     6,     1,
       2,     7,     0,     1,     2,     3,     4,     0,     1,     1,
       3,     2,     4,     8,     1,     1,     2,     6,     7,     0,
       1,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     2,     0,     1,     1,     3,     0,     0,
       0,     0,     0,     0
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned short int yydefact[] =
{
     304,     0,     0,   306,   305,   318,   320,   321,   313,     0,
      15,     0,     1,   333,   326,   306,   324,   327,   328,   329,
     330,   331,   332,   307,   524,   525,     0,   319,   315,     0,
     323,   314,   351,   337,   338,   339,     0,   340,   341,   342,
     343,   344,   345,   346,   347,   348,   349,   350,     0,   334,
     335,   333,   325,   526,   529,   313,     0,   316,     0,     0,
     358,     0,     0,     0,     0,   336,   534,   539,   532,   533,
     535,   536,   537,   538,   540,     0,   530,     0,   322,     2,
     304,   311,   358,   358,     0,     0,   359,    25,    30,    37,
      28,    39,    38,    33,    35,    40,    29,    31,    41,    34,
      36,    32,   391,    20,   390,    17,    19,    21,    24,    26,
      27,    22,    23,    18,    16,     0,   512,   486,   462,   546,
      16,     0,   541,   544,   531,     3,   306,   312,   310,     0,
       0,   363,   360,   361,   354,    43,    42,    49,    45,    44,
      46,     0,     0,     0,   513,     0,     0,   487,     0,     0,
     463,     0,   549,    80,   543,   545,     0,   306,   356,   352,
       0,     0,   306,   306,   514,   306,   311,   488,   306,   311,
     464,   460,   549,   542,   527,     8,     9,    10,    11,     0,
       0,    14,     0,    12,     0,    84,    13,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    58,     7,
       0,     0,   102,    57,    56,    60,    61,    62,     0,    81,
      63,    64,   104,   105,    65,    89,    59,    66,    68,    69,
     106,   119,    67,   107,   113,   117,   118,   132,   110,   136,
     139,   142,   149,   152,   154,   156,   158,   160,   162,   176,
     177,    82,   103,     4,   317,   306,   306,   362,   364,   333,
     378,     0,   306,   367,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   397,     0,   389,   392,   394,   395,     0,
       0,   518,   519,   511,   500,     0,   306,   492,   494,   495,
     497,   496,   485,   306,   528,     0,     0,     0,     0,     0,
      17,    18,     5,     0,     5,     0,     0,   111,   112,   126,
     126,   126,   136,     0,   103,   116,   100,   108,   109,   114,
     115,     0,     0,     0,     0,    76,   547,     0,    87,    88,
       0,     0,   167,   168,   169,   170,   171,   172,   173,   174,
     175,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    76,     0,     0,     0,   311,
     368,   399,     0,   398,     0,   523,   306,   521,   515,   306,
     501,     0,   489,   493,   333,   478,   470,   471,   472,   473,
     474,   475,   476,   477,   311,     0,   306,   468,     0,    85,
       0,    76,     0,   481,    92,     6,     0,     0,     0,     0,
     131,   130,     0,   127,   128,     0,     0,     0,    70,   126,
      72,    73,     0,    71,     0,     0,    77,    51,     0,    53,
      83,    99,   165,   166,   164,   133,   134,   135,   132,   137,
     138,   140,   141,   148,   147,   145,   146,   143,   144,   150,
     151,   153,   155,   157,   159,   161,     0,     0,     0,   311,
     311,   313,     0,     0,     0,     0,     0,     0,     0,   552,
       0,   423,   424,   355,     0,     0,   393,     0,   516,   520,
       0,     0,     0,     0,   461,   465,   469,    86,    97,     0,
       0,     0,   210,   483,   212,   211,     0,   482,   101,    96,
      95,    98,     0,   129,     0,     0,     0,   120,     0,    78,
      55,    50,    54,     0,    74,     0,    79,    75,   357,   353,
     306,   313,     0,     0,   215,     0,     0,     0,     0,     0,
     208,     0,     0,     0,   206,   552,     0,     0,     0,   459,
     197,   458,   422,   400,   396,   178,   522,     0,     0,   552,
       0,    90,    47,     0,   479,     0,   125,   123,   124,   121,
       0,    52,   163,     0,     0,   218,     0,     0,   380,     0,
     550,     0,     0,   306,     0,     0,   434,   433,   441,   442,
     443,   444,   448,   447,   431,   432,   435,   438,   429,   430,
     427,   428,   436,   437,   446,   445,   440,   439,     0,   306,
       0,   381,   306,     0,   421,   306,   306,   313,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     277,     0,   277,     0,     0,     0,     0,     0,   201,     0,
      21,    22,    61,   104,   105,    89,   117,   118,     0,   222,
     199,   182,   183,     0,   198,   184,   180,   181,     0,     0,
     185,     0,   186,   227,   228,   187,   194,   243,   244,   245,
     246,   188,   266,   267,   268,   269,   270,   189,   281,   284,
     190,   191,   192,   193,   195,   103,     0,   306,   306,     0,
     306,    93,    47,     0,   480,   484,   122,   453,   217,   379,
     216,   412,     0,     0,     0,     0,     0,   209,     0,     0,
     208,   207,     0,   306,     0,     0,     0,   306,   306,     0,
     271,     0,   288,   292,     0,   272,     0,     0,   253,     0,
       0,     0,     0,     0,     0,   278,     0,     0,     0,     0,
     293,   247,     0,     0,   205,   196,   200,   203,   204,   219,
     285,   509,     0,   508,     0,     0,   306,     0,    91,    94,
      48,     0,     0,     0,   454,   306,     0,     0,     0,   457,
       0,   419,     0,     0,   420,   553,   553,   553,     0,   402,
     405,     0,   403,   407,   202,   289,   289,   214,     0,     0,
     259,   263,     0,   254,   260,     0,   273,     0,   275,   177,
     179,     0,     0,   276,     0,   279,     0,   280,   282,   297,
     296,     0,     0,     0,   510,     0,     0,     0,     0,   552,
     213,     0,     0,   452,   197,     0,     0,   306,   306,     0,
       0,   385,   386,     0,   384,   382,   553,     0,     0,   418,
     290,     0,     0,     0,     0,     0,   299,   255,     0,     0,
     274,     0,     0,     0,     0,     0,   291,   283,     0,     0,
       0,   499,   498,     0,     0,   553,     0,    76,    76,     0,
     551,   551,   551,     0,   414,     0,   415,   450,   449,   197,
       0,   425,   197,   401,   411,   410,   552,   552,     0,     0,
       0,     0,     0,     0,     0,   261,     0,   256,   264,     0,
     232,   230,   229,   294,   234,   295,   249,   248,   306,   306,
     502,   306,     0,     0,   451,     0,     0,   413,     0,     0,
       0,   408,   409,   287,   286,     0,     0,   301,   300,   298,
     257,     0,     0,     0,     0,     0,   235,   236,     0,   239,
       0,     0,     0,   455,   456,   550,   550,   387,     0,   383,
     251,   250,     0,   258,   262,     0,   231,     0,   242,   233,
     237,   238,   240,     0,     0,   553,   416,   417,   426,     0,
     265,   241,   506,   507,   503,   252
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,   530,   244,   395,   198,   199,     9,   103,   619,   105,
     106,   200,   108,   109,   110,   201,   112,   113,   671,   401,
     500,   416,   417,   202,   203,   204,   205,   206,   418,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   738,
     217,   218,   219,   220,   221,   222,   223,   224,   225,   226,
     227,   228,   402,   403,   404,   229,   230,   231,   232,   233,
     234,   235,   236,   237,   238,   239,   240,   332,   419,   536,
     781,   630,   631,   632,   633,   634,   635,   636,   637,   638,
     523,   524,   483,   484,   639,   513,   514,   640,   641,   642,
     643,   644,   915,   916,   917,   918,   919,   645,   646,   647,
     648,   649,   772,   876,   932,   773,   877,   933,   774,   650,
     651,   652,   653,   654,   655,   716,   656,   657,   658,   659,
     821,   788,   660,   661,   662,   663,   791,   664,   825,   826,
       2,     3,   263,   156,    14,   128,   242,    11,    81,     4,
       5,     6,     7,    15,    16,    17,    48,    49,    50,    18,
     246,   162,   245,    85,    86,   133,   251,   252,   253,   254,
     255,   256,   862,   859,   264,   115,   265,   266,   267,   364,
     268,   257,   696,   759,   762,   697,   698,   866,   258,   806,
     807,   808,   259,   459,   527,   260,   460,   461,   588,   462,
     261,   743,   744,   262,   532,    19,   283,   149,   150,   384,
     385,   386,   387,   485,   486,   487,    20,   146,   147,   169,
     275,   276,   277,   278,   371,   279,   280,   798,   281,   733,
      21,   143,   144,   166,   270,   271,   272,    22,    23,    24,
      25,    75,    76,    77,   121,   122,   154,   123,   155,    26,
     174,   682,   895,   528,   817
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -765
static const short int yypact[] =
{
     -23,   112,   122,    97,   -23,  -765,  -765,  -765,   -72,   -16,
    -765,   188,  -765,   901,  -765,    43,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,    97,  -765,   150,  -765,  -765,   288,
    -765,   184,  -765,  -765,  -765,  -765,   288,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,   255,  2080,
    -765,  1563,  -765,  -765,   240,   301,   200,  -765,   225,   350,
      17,  2993,   378,   448,   484,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,   288,  -765,   324,  -765,   225,
     -23,   328,   361,   361,   179,   225,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,   373,  -765,   380,  -765,   509,  -765,  -765,  -765,
    -765,  -765,  -765,   509,   509,   517,   386,   389,   391,  -765,
    -765,   358,  -765,   411,  -765,  -765,   129,  -765,  -765,   225,
     225,  -765,   440,   443,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,   421,   220,   225,  -765,   288,   225,  -765,   288,   225,
    -765,    41,  -765,  2677,  -765,  -765,   427,   217,  -765,  -765,
     288,   288,   227,   110,  -765,   245,   328,   443,   247,   328,
     443,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,   128,
     431,  -765,  3037,  -765,   433,  -765,  -765,   437,   441,  2677,
    2677,  2366,  2677,  2677,  2677,  2677,  2677,  2677,  -765,  -765,
     420,   455,   141,   446,  -765,  -765,  -765,  -765,   457,   489,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,   366,  -765,  -765,  -765,   299,  -765,   333,
     187,   279,    39,   325,   463,   456,   454,   478,   163,  -765,
    -765,  -765,   189,  -765,  -765,   227,   227,   443,  -765,  2080,
    -765,   427,   248,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,     1,   469,   499,  -765,  -765,  -765,   593,
     427,   501,  -765,  -765,   551,   427,   250,  -765,  -765,  -765,
    -765,  -765,  -765,   253,  -765,  2677,   599,  2677,   373,   323,
     506,    25,   433,  3037,     0,  3037,  2677,  -765,  -765,    23,
      44,    57,   344,   480,    67,  -765,  -765,  -765,  -765,  -765,
    -765,   603,   604,  2677,   605,  2184,  -765,  2677,  -765,  -765,
     606,  2677,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  2677,  2677,  2677,  2677,  2677,  2677,  2677,  2677,  2677,
    3037,  3037,  2677,  2677,  2677,  2677,  2677,  2677,  2677,  2677,
    2677,  2677,  2677,  2677,  2677,  2184,   427,   427,  2838,   328,
    -765,  -765,  3037,  -765,  3037,  -765,    97,   470,  -765,   254,
    -765,  2949,  -765,  -765,  2080,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,   328,   427,   261,  -765,   377,  -765,
     486,  2184,  2677,  2275,  -765,  -765,   408,   488,   417,   492,
    -765,  -765,   493,    23,  -765,   494,   495,  2457,  2768,    23,
    -765,  -765,   390,  -765,  2677,  2677,   518,  -765,   496,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,   333,
     333,   187,   187,   380,   380,   279,   279,   279,   279,    39,
      39,   325,   463,   456,   454,   478,   483,   398,   498,   328,
     328,    28,  3081,  3037,   573,   574,    60,   626,    63,  -765,
     -13,  -765,  -765,  -765,    71,    76,  -765,  2677,  -765,  -765,
    3037,    77,    47,  2905,  -765,  -765,  -765,  -765,  -765,   513,
     405,  3037,  -765,  -765,  -765,  -765,   427,   538,  -765,  -765,
    -765,  -765,  2677,  -765,  2677,  2677,  2677,  -765,   514,  -765,
    -765,  -765,  -765,  2184,  -765,  2677,  -765,  -765,  -765,  -765,
     110,   101,    81,    26,  -765,    84,  3037,  3037,   433,   519,
     322,  1371,   543,   103,  -765,   433,    49,   544,   225,  -765,
    1794,  -765,  -765,  -765,  -765,  -765,  -765,    85,   521,   527,
     559,  -765,   509,   192,  -765,  2055,  -765,  -765,  -765,  -765,
    2677,  -765,  -765,   532,  2677,   520,   116,   655,  -765,   157,
    -765,   367,   424,   110,   537,  2275,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,   536,    97,
     660,  -765,   110,   225,  -765,    97,    97,   305,   523,   387,
     394,  3037,   529,  1926,   545,   433,   433,   416,   549,   550,
    2677,   433,  2677,   225,   395,   225,   552,   553,  -765,    86,
     420,   455,   134,   135,   147,   207,   211,   215,   299,  -765,
    -765,  -765,  -765,   427,  1794,  -765,  -765,  -765,   533,   541,
    -765,   542,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,   662,  -765,
    -765,  -765,  -765,  -765,  -765,    62,   233,   110,   110,   225,
      97,   225,   509,  2677,  -765,  -765,  -765,   540,  -765,  -765,
    -765,  -765,   225,  3037,  3037,   561,   225,  -765,  3037,   409,
     547,  -765,   563,    97,   423,   460,   427,   263,   285,  1794,
    -765,   179,  -765,  -765,    81,  -765,   600,  3037,  2586,  3037,
     548,  2677,   567,  2677,  2677,  -765,   568,  2677,   569,   362,
    -765,  -765,  2586,  2677,   594,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,   427,  -765,   566,   575,    97,   428,  -765,  -765,
    -765,   595,    35,   243,  -765,    97,    87,    93,   271,  -765,
      94,  -765,   274,   427,  -765,  -765,  -765,  -765,   601,  -765,
    -765,   619,  -765,  -765,  -765,   689,   689,   621,   592,    96,
    -765,  -765,   577,  -765,   624,    98,  -765,   582,  -765,   608,
    -765,   115,   609,  -765,   612,  -765,   225,   362,  -765,  -765,
    -765,   613,   614,   616,  -765,   233,   233,   466,   427,  -765,
    -765,   433,   433,  -765,  1794,   467,   427,    97,    97,   617,
     618,  -765,  -765,   149,  -765,  -765,  -765,   292,   292,  -765,
    -765,   620,   622,  2677,   590,   152,  -765,  2677,  2677,   704,
    -765,  1926,  1926,  1926,  1926,   225,  -765,  -765,  1926,  1926,
    1926,  -765,  -765,   233,   233,  -765,   225,  2184,  2184,   427,
    -765,  -765,  -765,   636,  -765,   652,  -765,  -765,  -765,  1794,
    3037,  -765,  1794,  -765,  -765,  -765,  -765,  -765,   225,   225,
     625,   627,  2677,   748,  1926,  -765,   623,  -765,  -765,  2677,
    -765,  -765,   729,  -765,   407,  -765,  -765,  -765,   294,   296,
    -765,    97,   632,   637,  -765,   225,   225,  -765,   427,    99,
     427,  -765,  -765,  -765,  -765,   628,   629,  -765,  -765,  -765,
    2677,   638,  1926,  2677,   633,   427,   407,  -765,  1662,  -765,
     668,   675,   427,  -765,  -765,  -765,  -765,  -765,   643,  -765,
    -765,  -765,   646,  -765,   624,  1926,  -765,   634,  -765,  -765,
    -765,  1794,  -765,   233,   233,  -765,  -765,  -765,  -765,  1926,
    -765,  -765,  -765,  -765,  -765,  -765
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
    -765,   -34,   282,  -145,  -765,  -765,  -765,   -73,    72,   596,
    -765,   133,  -765,   640,  -765,   -54,  -765,   598,   111,  -100,
     369,  -765,   283,  -765,  -765,  -765,  -765,   228,  -347,  -765,
    -765,  -156,  -765,  -765,   412,   445,  -765,   602,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,   379,   691,   724,
     534,  -765,  -263,  -765,   382,  -172,   229,   139,   223,   442,
     439,   444,   438,   450,  -765,  -765,   290,  -765,   384,  -549,
    -689,  -630,  -567,  -278,  -751,  -127,  -765,  -765,  -765,  -582,
    -474,   202,  -121,  -765,  -765,  -509,   236,  -765,  -692,  -765,
    -765,  -765,  -765,  -765,  -120,  -765,  -123,  -765,  -765,  -765,
    -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -113,  -765,
    -765,  -765,  -765,  -765,  -765,   195,  -765,  -765,    92,  -635,
      34,    31,  -765,  -765,  -765,  -765,  -765,  -765,  -765,   -69,
    -765,   735,     6,  -765,  -765,  -154,    -1,   364,  -765,  -765,
     815,  -765,  -765,   697,    10,  -152,  -223,  -765,   775,  -765,
    -765,  -765,  -765,   491,  -765,   277,   331,  -765,   576,  -243,
    -186,  -178,  -765,  -765,  -490,  -765,  -531,   459,  -765,  -765,
    -765,  -170,   136,  -765,  -765,   132,   137,     8,  -169,  -765,
      27,    29,  -165,  -765,  -765,  -163,  -765,  -765,  -765,  -765,
    -160,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,   447,  -290,  -765,  -765,  -765,  -765,  -765,  -765,
    -765,  -765,   562,  -765,  -765,  -765,  -765,   -52,  -765,  -764,
    -765,  -765,  -765,  -765,  -765,  -765,   468,  -765,  -765,  -765,
     819,  -765,  -765,  -765,  -765,   693,  -765,  -765,  -765,  -765,
     673,  -344,  -273,  -476,  -739
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -549
static const short int yytable[] =
{
      10,   394,   119,   556,   726,   678,   138,   111,   448,    13,
     250,   131,   273,   139,   140,   282,   771,   818,   819,   302,
     553,    51,    83,   730,    80,    52,   358,   137,    56,   137,
     132,   841,   842,    82,   793,    58,   706,   405,   406,   293,
     376,   559,   295,  -303,   479,   125,    55,   801,   137,   593,
     340,   134,   539,   849,    31,   361,    28,   362,   689,     1,
     114,   137,   363,   669,   694,    55,   137,   -16,   520,   764,
      29,   137,   131,   685,   120,   131,   533,   863,   119,   888,
     889,   534,   538,   120,   397,   341,   555,   131,   248,   520,
     666,   690,   809,   250,   250,   158,   159,   377,   810,   813,
     250,   824,   692,   829,   928,   378,   890,   802,   898,   165,
      79,   900,   168,   379,   380,   171,   521,     8,   381,   540,
     382,   594,    12,   383,   557,   292,   770,    30,   111,   388,
     529,   375,    51,   104,   871,   522,   878,   301,   875,   737,
     790,   172,  -548,   376,   120,   724,   498,   120,    79,   400,
     120,   473,   730,   292,   342,   343,    28,   412,    84,   120,
     120,   354,   777,    51,   429,   430,   354,    52,   249,   558,
     400,   269,   311,   136,   274,   344,   345,   734,   735,   952,
     953,   114,   531,   400,    55,   312,   135,   355,   -16,   136,
     304,   139,   355,    31,   107,   767,  -548,   136,   447,  -218,
     377,   590,   136,   135,   409,   463,   954,   136,   378,  -548,
     136,   136,   136,   136,   557,   832,   379,   380,   771,   136,
     136,   381,   136,   382,   136,   136,   383,   285,  -548,    28,
     474,    95,  -220,  -223,   375,    88,   480,  -388,    89,   111,
     313,   111,   833,   554,  -218,  -224,   591,   860,    98,    54,
     873,   249,   249,  -308,   289,   590,   286,   393,   249,   679,
      59,  -220,  -223,    93,   880,   881,   882,   883,    94,   314,
      66,   885,   886,   887,  -224,    60,   861,  -220,  -223,   874,
      61,   352,   274,    96,    62,    97,   111,   111,   354,   374,
    -224,   673,   114,    55,   114,   508,   509,    99,   100,    63,
     681,   101,    67,   353,   111,  -221,   510,   909,   111,  -225,
     111,   726,    57,  -226,   355,   107,  -548,   111,   136,   336,
     337,   702,   703,   846,   300,    64,  -548,    68,    69,    70,
      71,    72,    73,    74,  -221,   719,   720,   721,  -225,   114,
     114,  -309,  -226,    78,  -548,   936,  -548,  -548,    79,  -548,
    -221,  -365,  -548,  -548,  -225,    82,    79,   114,  -226,   393,
    -548,   114,  -548,   114,   937,   396,    79,   398,   950,  -517,
     114,  -490,  -366,   563,  -491,   269,   731,  -466,   243,   599,
     592,   739,   955,   116,  -548,  -467,   803,  -404,   338,   339,
     901,   902,   374,  -548,    79,  -548,   786,    79,   111,   111,
     321,   322,   323,   324,   325,   326,   327,   328,   749,  -406,
     329,   330,   433,   434,   811,    79,   111,   814,  -504,   111,
    -505,   710,   167,   913,   675,   170,   107,   111,   107,    28,
     458,   914,   711,    28,   464,   864,   465,   247,   346,   347,
     712,   331,   672,   472,   687,  -313,   699,  -313,   391,   136,
      28,   114,   114,   117,   701,   518,   151,   525,   152,   333,
     708,   709,   111,   111,   565,   124,   717,   334,   335,   114,
     407,   127,   114,   107,   107,   317,   621,   477,   334,   335,
     114,   435,   436,   437,   438,   318,   319,   320,   317,   118,
     499,   107,   683,   136,   596,   107,   317,   107,   506,   135,
     892,   893,    84,   317,   107,   542,   136,   366,   836,   751,
      79,   393,   292,   137,   560,   114,   114,    79,    79,   287,
     296,   366,   141,   754,   512,   515,   366,   142,   799,   665,
     145,   393,   148,   359,   136,   488,   153,   241,   160,   865,
     865,   161,   537,   136,   490,   458,   163,   111,   311,   684,
     136,   243,   368,   543,   755,   756,   287,   372,   292,   693,
     843,   844,   294,   850,   851,   140,   296,   431,   432,   439,
     440,   315,   672,   129,   130,   303,   356,   357,   896,   897,
     621,   946,   947,   312,   316,   107,   107,   317,   561,   562,
     903,   904,   348,   350,   349,   351,   365,   366,   367,   369,
     114,   370,   695,   107,   389,   392,   107,   408,   410,   411,
     413,   421,   467,   478,   107,   489,   503,   925,   926,   491,
     492,   494,   495,   504,   505,   507,   516,   517,   765,   111,
     111,   519,   732,   665,   111,   736,   545,   393,   449,   450,
     541,   550,   589,   595,   564,   621,   667,   766,   745,   107,
     107,   468,   668,   111,   621,   111,   847,   848,   670,   677,
     555,   688,   554,   620,   686,   690,   700,   475,   621,   241,
     707,   390,   705,   704,   713,   714,   727,   722,   723,   599,
     399,   742,   114,   114,   728,   729,   768,   114,   748,   565,
     752,   776,   590,   795,   820,   800,   756,   241,   665,   695,
     120,   420,   796,   758,   761,   422,   114,   665,   114,   804,
     778,   783,   785,   755,   812,   423,   424,   823,   815,   557,
     827,   665,   828,   297,   298,   830,   305,   306,   307,   308,
     309,   310,   872,   851,   107,   831,   834,   446,   241,   835,
     838,   839,   797,   840,   857,   858,   879,   868,   850,   869,
     621,   805,   905,   824,   906,   746,   747,   912,   622,   923,
     750,   732,   732,   943,   924,   935,   910,   620,   544,   944,
     948,   930,   931,   949,   938,   951,   241,   482,   290,   769,
     291,   775,   164,   740,   502,   493,   551,   497,   442,   444,
     441,   941,   691,   680,   443,   942,   940,   934,   501,   501,
     822,   884,   445,   665,   908,   621,   111,   718,   621,   732,
     732,   787,   891,   853,   855,   126,   107,   107,   837,    27,
     629,   107,   526,   157,    65,   466,   867,   674,   360,   753,
     763,   622,   620,   476,   760,   856,   854,   469,   373,   922,
     107,   620,   107,    53,   173,   284,     0,     0,     0,     0,
       0,   535,     0,     0,     0,   620,     0,     0,   665,   114,
       0,   665,   622,     0,   621,     0,     0,   425,   426,   427,
     428,   428,   428,   428,     0,     0,   428,   428,   428,   428,
     428,   428,   428,   428,   428,   428,   428,   621,     0,   552,
       0,     0,     0,   629,   920,   921,     0,   797,     0,     0,
       0,  -302,     0,     0,    32,     0,     0,     0,     0,   732,
     732,    33,     0,     0,     0,   725,     0,   665,     0,     0,
       0,     0,     0,     0,   629,     0,     0,   622,     0,   482,
       0,     0,   899,    34,     0,     0,   622,   620,   535,     0,
     665,   425,   623,     0,     0,     0,    35,     0,     0,   482,
      36,    37,     0,     0,     0,     0,    38,     0,    39,    40,
      41,    42,     0,     0,     0,    43,     0,     0,     0,    44,
       0,     0,     0,     0,     0,   624,     0,     0,   757,     0,
       0,    45,     0,     0,    46,     0,    47,     0,     0,   629,
       0,     0,   620,   107,   715,   620,   715,     0,   629,     0,
       0,     0,     0,   779,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   792,   794,   623,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   546,     0,   547,   548,
     549,     0,   622,     0,     0,   816,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   623,     0,   624,     0,
       0,   620,     0,     0,     0,     0,   622,   741,     0,   622,
     622,   622,   622,     0,   628,     0,   622,   622,   622,     0,
       0,     0,     0,     0,   620,     0,     0,     0,     0,   624,
     845,     0,     0,     0,   676,     0,     0,   622,   852,     0,
     622,     0,     0,     0,   629,   535,     0,   780,   782,     0,
       0,   784,   622,     0,     0,     0,   789,   780,     0,     0,
       0,   623,     0,   870,     0,     0,     0,     0,   629,     0,
     623,   629,   629,   629,   629,     0,     0,     0,   629,   629,
     629,   894,   625,     0,     0,     0,     0,   628,   622,     0,
     622,     0,     0,     0,   624,     0,   622,     0,     0,   629,
       0,     0,   629,   624,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   622,   629,     0,     0,     0,   628,   622,
       0,     0,     0,     0,     0,     0,     0,   622,     0,     0,
     927,     0,   929,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   939,     0,     0,
     629,     0,   629,     0,   945,   625,     0,   780,   629,     0,
       0,   780,     0,     0,     0,     0,   623,     0,     0,     0,
       0,   626,     0,     0,     0,   629,     0,     0,     0,     0,
       0,   629,     0,   628,     0,     0,   625,     0,     0,   629,
     623,     0,   628,   623,   623,   623,   623,     0,     0,   624,
     623,   623,   623,     0,   627,     0,   907,     0,     0,     0,
       0,     0,     0,   911,     0,     0,     0,     0,     0,     0,
       0,   623,     0,   624,   623,     0,   624,   624,   624,   624,
       0,     0,     0,   624,   624,   624,   623,     0,     0,     0,
       0,     0,     0,     0,   626,     0,     0,   535,     0,     0,
       0,   625,     0,     0,   624,     0,     0,   624,     0,     0,
     625,     0,     0,     0,     0,     0,     0,     0,     0,   624,
       0,     0,   623,     0,   623,   626,     0,   627,     0,     0,
     623,     0,     0,     0,     0,     0,     0,     0,   628,     0,
       0,     0,     0,     0,     0,     0,     0,   623,     0,     0,
       0,     0,     0,   623,     0,   624,     0,   624,   627,     0,
       0,   623,   628,   624,     0,   628,   628,   628,   628,     0,
       0,     0,   628,   628,   628,     0,     0,     0,     0,     0,
     624,     0,     0,     0,     0,     0,   624,     0,     0,     0,
     626,     0,     0,   628,   624,     0,   628,     0,     0,   626,
       0,     0,     0,     0,   566,     0,   625,     0,   628,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   627,     0,     0,     0,     0,     0,     0,
     625,     0,   627,   625,   625,   625,   625,     0,     0,     0,
     625,   625,   625,     0,   628,   567,   628,     0,     0,     0,
       0,     0,   628,     0,     0,     0,     0,     0,     0,     0,
       0,   625,     0,     0,   625,     0,     0,     0,     0,   628,
       0,     0,     0,     0,     0,   628,   625,     0,     0,     0,
     568,   569,     0,   628,   570,   571,   572,   573,     0,     0,
     574,   575,     0,     0,     0,   626,     0,   576,     0,     0,
     577,   578,   579,   580,   581,   582,   583,   584,   585,   586,
     587,     0,   625,     0,   625,     0,     0,     0,     0,   626,
     625,     0,   626,   626,   626,   626,     0,     0,   627,   626,
     626,   626,     0,     0,     0,     0,     0,   625,     0,     0,
       0,     0,     0,   625,     0,     0,     0,     0,     0,     0,
     626,   625,   627,   626,     0,   627,   627,   627,   627,     0,
       0,     0,   627,   627,   627,   626,    32,     0,     0,     0,
       0,     0,     0,    33,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   627,     0,     0,   627,     0,     0,     0,
       0,     0,     0,     0,     0,    34,     0,     0,   627,     0,
       0,   626,     0,   626,     0,     0,     0,     0,    35,   626,
       0,     0,    36,    37,     0,     0,     0,     0,    38,     0,
      39,    40,    41,    42,     0,     0,   626,    43,     0,     0,
       0,    44,   626,     0,   627,     0,   627,     0,     0,     0,
     626,     0,   627,    45,     0,     0,    46,     0,    47,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   627,
       0,     0,     0,     0,     0,   627,     0,   597,   175,   176,
     177,   178,     0,   627,   179,    87,   598,    88,   913,   599,
      89,   600,     0,   601,   602,    90,   914,     0,   603,    91,
       0,     0,     0,     0,     0,   181,     0,   604,    92,   605,
     606,   607,   608,     0,     0,    93,     0,     0,     0,   609,
      94,     0,   182,   183,    95,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   610,    96,     0,    97,   184,     0,
       0,    98,     0,   611,   185,   612,   186,   613,   187,    99,
     100,   614,   615,   101,   616,     0,   288,     0,   617,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   189,   190,     0,     0,    79,     0,   191,   192,     0,
       0,   193,   194,   195,   196,   197,     0,     0,     0,   597,
     175,   176,   177,   178,     0,   618,   179,    87,   598,    88,
       0,   599,    89,   600,     0,   601,   602,    90,     0,     0,
     603,    91,     0,     0,     0,     0,     0,   181,     0,   604,
      92,   605,   606,   607,   608,     0,     0,    93,     0,     0,
       0,   609,    94,     0,   182,   183,    95,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   610,    96,     0,    97,
     184,     0,     0,    98,     0,   611,   185,   612,   186,   613,
     187,    99,   100,   614,   615,   101,   616,     0,   288,     0,
     617,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   189,   190,     0,     0,    79,     0,   191,
     192,     0,     0,   193,   194,   195,   196,   197,     0,     0,
       0,    55,   175,   176,   177,   178,     0,   618,   179,    87,
     598,    88,     0,   599,    89,   600,     0,     0,   602,    90,
       0,     0,   603,    91,     0,     0,     0,     0,     0,   181,
       0,   604,    92,   605,   606,   607,   608,     0,     0,    93,
       0,     0,     0,   609,    94,     0,   182,   183,    95,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   610,    96,
       0,    97,   184,     0,     0,    98,     0,   611,   185,   612,
     186,   613,   187,    99,   100,   614,   615,   101,   616,     0,
       0,     0,   617,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   189,   190,     0,     0,    79,
       0,   191,   192,     0,     0,   193,   194,   195,   196,   197,
      55,   175,   176,   177,   178,     0,     0,   179,    87,   618,
      88,     0,     0,    89,   180,     0,     0,     0,    90,     0,
       0,     0,    91,    32,     0,     0,     0,     0,   181,     0,
      33,    92,     0,     0,     0,     0,     0,     0,    93,     0,
       0,     0,     0,    94,     0,   182,   183,    95,     0,     0,
       0,     0,    34,     0,     0,     0,     0,     0,    96,     0,
      97,   184,   481,     0,    98,    35,     0,   185,     0,   186,
      37,   187,    99,   100,   188,    38,   101,    39,    40,    41,
      42,     0,     0,     0,    43,     0,     0,     0,    44,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      45,     0,     0,    46,     0,    47,     0,     0,     0,     0,
       0,     0,     0,     0,   189,   190,     0,     0,    79,   243,
     191,   192,     0,     0,   193,   194,   195,   196,   197,    55,
     175,   176,   177,   178,     0,     0,   179,    87,     0,    88,
       0,     0,    89,   180,     0,     0,     0,    90,     0,     0,
       0,    91,     0,     0,     0,     0,     0,   181,     0,     0,
      92,     0,     0,     0,     0,     0,     0,    93,     0,     0,
       0,     0,    94,     0,   182,   183,    95,     0,   414,     0,
       0,     0,     0,     0,     0,   415,     0,    96,     0,    97,
     184,     0,     0,    98,     0,     0,   185,     0,   186,     0,
     187,    99,   100,   188,     0,   101,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      55,   175,   176,   177,   178,     0,     0,   179,    87,     0,
      88,     0,     0,    89,   180,     0,     0,     0,    90,     0,
       0,     0,    91,   189,   190,     0,     0,     0,   181,   191,
     192,    92,     0,   193,   194,   195,   196,   197,    93,     0,
       0,     0,     0,    94,     0,   182,   183,    95,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    96,     0,
      97,   184,   481,     0,    98,     0,     0,   185,     0,   186,
       0,   187,    99,   100,   188,     0,   101,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    55,   175,   176,   177,   178,     0,     0,   179,    87,
       0,    88,     0,     0,    89,   180,     0,     0,     0,    90,
       0,     0,     0,    91,   189,   190,     0,     0,    79,   181,
     191,   192,    92,     0,   193,   194,   195,   196,   197,    93,
       0,     0,     0,     0,    94,     0,   182,   183,    95,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    96,
       0,    97,   184,     0,     0,    98,     0,     0,   185,     0,
     186,     0,   187,    99,   100,   188,     0,   101,     0,     0,
     299,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    55,   175,   176,   177,   178,     0,     0,   179,
      87,     0,    88,     0,     0,    89,   180,     0,     0,     0,
      90,     0,     0,     0,    91,   189,   190,     0,     0,     0,
     181,   191,   192,    92,     0,   193,   194,   195,   196,   197,
      93,     0,     0,     0,     0,    94,     0,   182,   183,    95,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      96,     0,    97,   184,     0,     0,    98,     0,     0,   185,
       0,   186,     0,   187,    99,   100,   188,     0,   101,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   189,   190,     0,     0,
       0,     0,   191,   192,   496,     0,   193,   194,   195,   196,
     197,    55,   175,   176,   177,   178,     0,     0,   179,    87,
       0,    88,     0,     0,    89,   180,     0,     0,     0,    90,
       0,     0,     0,    91,     0,     0,     0,     0,     0,   181,
       0,     0,    92,     0,     0,     0,     0,     0,     0,    93,
       0,     0,     0,     0,    94,     0,   182,   183,    95,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    96,
       0,    97,   184,     0,     0,    98,     0,     0,   185,     0,
     186,     0,   187,    99,   100,   188,     0,   101,     0,     0,
     288,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    55,   175,   176,   177,   178,     0,     0,   179,
      87,     0,    88,     0,     0,    89,   180,     0,     0,     0,
      90,     0,     0,     0,    91,   189,   190,     0,     0,     0,
     181,   191,   192,    92,     0,   193,   194,   195,   196,   197,
      93,     0,     0,     0,     0,    94,     0,   182,   183,    95,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      96,     0,    97,   184,     0,     0,    98,     0,     0,   185,
       0,   186,     0,   187,    99,   100,   188,     0,   101,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    55,   175,   176,   177,   178,     0,     0,
     179,    87,     0,    88,     0,     0,    89,   180,     0,     0,
       0,    90,     0,     0,     0,    91,   189,   190,     0,     0,
       0,   181,   191,   192,    92,     0,   193,   194,   195,   196,
     197,    93,     0,     0,     0,     0,    94,     0,   182,   183,
      95,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    96,     0,    97,   184,     0,     0,    98,     0,     0,
     185,     0,   186,   451,   187,    99,   100,   188,     0,   101,
       0,    87,     0,    88,     0,     0,    89,     0,    60,   452,
       0,    90,     0,    61,     0,    91,     0,    62,   453,   454,
       0,     0,     0,     0,    92,     0,     0,     0,     0,   455,
       0,    93,    63,     0,     0,     0,    94,     0,     0,     0,
      95,     0,     0,   191,     0,     0,     0,     0,   194,   195,
       0,    96,     0,    97,     0,     0,     0,    98,    64,     0,
     451,     0,     0,     0,     0,    99,   100,     0,    87,   101,
      88,     0,   456,    89,     0,    60,   452,     0,    90,     0,
      61,     0,    91,     0,    62,   453,   454,     0,     0,     0,
       0,    92,     0,     0,     0,     0,   455,     0,    93,    63,
       0,     0,     0,    94,    55,     0,     0,    95,     0,     0,
       0,     0,    87,     0,    88,     0,     0,    89,    96,   457,
      97,     0,    90,     0,    98,    64,    91,     0,     0,   470,
       0,     0,    99,   100,     0,    92,   101,     0,     0,   456,
       0,     0,    93,     0,     0,     0,     0,    94,    55,     0,
       0,    95,     0,     0,     0,     0,    87,     0,    88,     0,
       0,    89,    96,     0,    97,     0,    90,     0,    98,     0,
      91,     0,     0,     0,     0,     0,    99,   100,     0,    92,
     101,     0,     0,   471,     0,     0,    93,     0,     0,     0,
       0,    94,    55,     0,     0,    95,     0,     0,     0,     0,
      87,     0,    88,     0,     0,    89,    96,     0,    97,     0,
      90,     0,    98,     0,    91,     0,     0,     0,     0,     0,
      99,   100,     0,    92,   101,     0,     0,   102,     0,     0,
      93,     0,     0,     0,     0,    94,   511,     0,     0,    95,
       0,     0,     0,     0,    87,     0,    88,     0,     0,    89,
      96,     0,    97,     0,    90,     0,    98,     0,    91,     0,
       0,     0,     0,     0,    99,   100,     0,    92,   101,     0,
       0,   288,     0,     0,    93,     0,     0,     0,     0,    94,
       0,     0,     0,    95,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    96,     0,    97,     0,     0,     0,
      98,     0,     0,     0,     0,     0,     0,     0,    99,   100,
       0,     0,   101,     0,     0,   288
};

static const short int yycheck[] =
{
       1,   291,    75,   512,   634,   554,   106,    61,   355,     3,
     162,    84,   166,   113,   114,   169,   708,   756,   757,   191,
     510,    15,     5,   658,    58,    15,   249,     4,    29,     4,
      84,   795,   796,     5,   723,    36,   603,   300,   301,   184,
     283,   515,   187,     0,   391,    79,     5,    12,     4,   525,
      11,    85,     5,   804,     5,    54,   128,    56,   589,    82,
      61,     4,    61,   539,   595,     5,     4,     5,     5,   699,
     142,     4,   145,   563,    75,   148,     5,   816,   151,   843,
     844,     5,     5,    84,    84,    46,     5,   160,   161,     5,
       5,     5,     5,   245,   246,   129,   130,   283,     5,     5,
     252,     5,   592,     5,     5,   283,   845,    72,   859,   143,
     123,   862,   146,   283,   283,   149,    53,     5,   283,    72,
     283,    72,     0,   283,    98,   125,   708,   143,   182,   285,
     143,   283,   126,    61,   823,    72,   828,   191,   827,   670,
     722,   100,    99,   386,   145,   619,   409,   148,   123,   126,
     151,   374,   787,   125,   115,   116,   128,   313,   141,   160,
     161,    99,   711,   157,   336,   337,    99,   157,   162,   143,
     126,   165,   128,   126,   168,   136,   137,   667,   668,   943,
     944,   182,   460,   126,     5,   128,   126,   125,   126,   126,
     191,   291,   125,     5,    61,   704,    99,   126,   354,    98,
     386,    98,   126,   126,   304,   359,   945,   126,   386,    99,
     126,   126,   126,   126,    98,   100,   386,   386,   910,   126,
     126,   386,   126,   386,   126,   126,   386,    99,    99,   128,
     384,    52,    98,    98,   386,    15,   392,   127,    18,   293,
      99,   295,   127,   142,   143,    98,   143,    98,    69,    99,
      98,   245,   246,   124,   182,    98,   128,   291,   252,   143,
       5,   127,   127,    43,   831,   832,   833,   834,    48,   128,
      30,   838,   839,   840,   127,    20,   127,   143,   143,   127,
      25,   118,   276,    63,    29,    65,   340,   341,    99,   283,
     143,    99,   293,     5,   295,   449,   450,    77,    78,    44,
     143,    81,    62,   140,   358,    98,   451,   874,   362,    98,
     364,   941,   128,    98,   125,   182,    99,   371,   126,   132,
     133,   599,   600,   799,   191,    70,    99,    87,    88,    89,
      90,    91,    92,    93,   127,   613,   614,   615,   127,   340,
     341,   124,   127,   143,    99,   912,    99,    99,   123,    99,
     143,   124,    99,    99,   143,     5,   123,   358,   143,   393,
      99,   362,    99,   364,   913,   293,   123,   295,   935,   124,
     371,   124,   124,   518,   124,   369,   143,   124,   124,    17,
     525,   671,   949,     5,    99,   124,   143,   124,   109,   110,
     866,   867,   386,    99,   123,    99,    34,   123,   452,   453,
     101,   102,   103,   104,   105,   106,   107,   108,   686,   124,
     111,   112,   340,   341,   143,   123,   470,   143,   124,   473,
     124,     5,   145,    16,   545,   148,   293,   481,   295,   128,
     358,    24,    16,   128,   362,   143,   364,   160,   113,   114,
      24,   142,   542,   371,   565,   123,   141,   125,   125,   126,
     128,   452,   453,     5,   599,   456,    98,   458,   100,   126,
     605,   606,   516,   517,   142,   141,   611,   134,   135,   470,
     126,   143,   473,   340,   341,    98,   530,   100,   134,   135,
     481,   342,   343,   344,   345,   119,   120,   121,    98,     5,
     100,   358,   125,   126,   528,   362,    98,   364,   100,   126,
     847,   848,   141,    98,   371,   100,   126,    98,   786,   100,
     123,   545,   125,     4,   515,   516,   517,   123,   123,   125,
     125,    98,     5,   100,   452,   453,    98,   141,   100,   530,
     141,   565,   141,   251,   126,   127,   125,   153,    98,   817,
     818,    98,   470,   126,   127,   473,   125,   601,   128,   125,
     126,   124,   270,   481,    94,    95,   125,   275,   125,   593,
      94,    95,   125,    96,    97,   665,   125,   338,   339,   346,
     347,   125,   672,    82,    83,   191,   245,   246,   851,   852,
     634,   925,   926,   128,   127,   452,   453,    98,   516,   517,
     868,   869,   129,   139,   138,   117,   127,    98,     5,    98,
     601,    50,   596,   470,     5,    99,   473,   127,     5,     5,
       5,     5,   142,   127,   481,   127,    98,   895,   896,   127,
     127,   127,   127,   127,   141,   127,    53,    53,   701,   683,
     684,     5,   666,   634,   688,   669,    98,   671,   356,   357,
     127,   127,    99,    99,   125,   699,   125,   701,   682,   516,
     517,   369,   125,   707,   708,   709,   801,   802,    99,   127,
       5,   125,   142,   530,   127,     5,   143,   385,   722,   285,
     125,   287,   143,   601,   125,   125,   143,   125,   125,    17,
     296,   141,   683,   684,   143,   143,    86,   688,   127,   142,
     127,   143,    98,   127,     5,   100,    95,   313,   699,   693,
     701,   317,   127,   697,   698,   321,   707,   708,   709,   743,
     143,   143,   143,    94,   748,   331,   332,   125,   752,    98,
     143,   722,    98,   189,   190,   143,   192,   193,   194,   195,
     196,   197,   142,    97,   601,   127,   127,   353,   354,   127,
     127,   127,   736,   127,   127,   127,    42,   127,    96,   127,
     804,   745,   127,     5,   127,   683,   684,    28,   530,   127,
     688,   795,   796,    95,   127,   127,   143,   634,   486,    94,
     127,   143,   143,   127,   141,   141,   392,   393,   182,   707,
     182,   709,   142,   672,   415,   403,   503,   408,   349,   351,
     348,   918,   590,   557,   350,   918,   916,   910,   414,   415,
     766,   835,   352,   804,   873,   859,   860,   612,   862,   843,
     844,   719,   846,   807,   808,    80,   683,   684,   787,     4,
     530,   688,   458,   126,    49,   366,   818,   545,   252,   693,
     698,   603,   699,   386,   697,   808,   807,   369,   276,   891,
     707,   708,   709,    24,   151,   172,    -1,    -1,    -1,    -1,
      -1,   467,    -1,    -1,    -1,   722,    -1,    -1,   859,   860,
      -1,   862,   634,    -1,   918,    -1,    -1,   333,   334,   335,
     336,   337,   338,   339,    -1,    -1,   342,   343,   344,   345,
     346,   347,   348,   349,   350,   351,   352,   941,    -1,   505,
      -1,    -1,    -1,   603,   888,   889,    -1,   891,    -1,    -1,
      -1,     0,    -1,    -1,     3,    -1,    -1,    -1,    -1,   943,
     944,    10,    -1,    -1,    -1,   633,    -1,   918,    -1,    -1,
      -1,    -1,    -1,    -1,   634,    -1,    -1,   699,    -1,   545,
      -1,    -1,   860,    32,    -1,    -1,   708,   804,   554,    -1,
     941,   407,   530,    -1,    -1,    -1,    45,    -1,    -1,   565,
      49,    50,    -1,    -1,    -1,    -1,    55,    -1,    57,    58,
      59,    60,    -1,    -1,    -1,    64,    -1,    -1,    -1,    68,
      -1,    -1,    -1,    -1,    -1,   530,    -1,    -1,   696,    -1,
      -1,    80,    -1,    -1,    83,    -1,    85,    -1,    -1,   699,
      -1,    -1,   859,   860,   610,   862,   612,    -1,   708,    -1,
      -1,    -1,    -1,   713,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   723,   732,   603,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   492,    -1,   494,   495,
     496,    -1,   804,    -1,    -1,   753,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   634,    -1,   603,    -1,
      -1,   918,    -1,    -1,    -1,    -1,   828,   673,    -1,   831,
     832,   833,   834,    -1,   530,    -1,   838,   839,   840,    -1,
      -1,    -1,    -1,    -1,   941,    -1,    -1,    -1,    -1,   634,
     798,    -1,    -1,    -1,   550,    -1,    -1,   859,   806,    -1,
     862,    -1,    -1,    -1,   804,   711,    -1,   713,   714,    -1,
      -1,   717,   874,    -1,    -1,    -1,   722,   723,    -1,    -1,
      -1,   699,    -1,   823,    -1,    -1,    -1,    -1,   828,    -1,
     708,   831,   832,   833,   834,    -1,    -1,    -1,   838,   839,
     840,   849,   530,    -1,    -1,    -1,    -1,   603,   910,    -1,
     912,    -1,    -1,    -1,   699,    -1,   918,    -1,    -1,   859,
      -1,    -1,   862,   708,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   935,   874,    -1,    -1,    -1,   634,   941,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   949,    -1,    -1,
     898,    -1,   900,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   915,    -1,    -1,
     910,    -1,   912,    -1,   922,   603,    -1,   823,   918,    -1,
      -1,   827,    -1,    -1,    -1,    -1,   804,    -1,    -1,    -1,
      -1,   530,    -1,    -1,    -1,   935,    -1,    -1,    -1,    -1,
      -1,   941,    -1,   699,    -1,    -1,   634,    -1,    -1,   949,
     828,    -1,   708,   831,   832,   833,   834,    -1,    -1,   804,
     838,   839,   840,    -1,   530,    -1,   872,    -1,    -1,    -1,
      -1,    -1,    -1,   879,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   859,    -1,   828,   862,    -1,   831,   832,   833,   834,
      -1,    -1,    -1,   838,   839,   840,   874,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   603,    -1,    -1,   913,    -1,    -1,
      -1,   699,    -1,    -1,   859,    -1,    -1,   862,    -1,    -1,
     708,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   874,
      -1,    -1,   910,    -1,   912,   634,    -1,   603,    -1,    -1,
     918,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   804,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   935,    -1,    -1,
      -1,    -1,    -1,   941,    -1,   910,    -1,   912,   634,    -1,
      -1,   949,   828,   918,    -1,   831,   832,   833,   834,    -1,
      -1,    -1,   838,   839,   840,    -1,    -1,    -1,    -1,    -1,
     935,    -1,    -1,    -1,    -1,    -1,   941,    -1,    -1,    -1,
     699,    -1,    -1,   859,   949,    -1,   862,    -1,    -1,   708,
      -1,    -1,    -1,    -1,    33,    -1,   804,    -1,   874,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   699,    -1,    -1,    -1,    -1,    -1,    -1,
     828,    -1,   708,   831,   832,   833,   834,    -1,    -1,    -1,
     838,   839,   840,    -1,   910,    74,   912,    -1,    -1,    -1,
      -1,    -1,   918,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   859,    -1,    -1,   862,    -1,    -1,    -1,    -1,   935,
      -1,    -1,    -1,    -1,    -1,   941,   874,    -1,    -1,    -1,
     109,   110,    -1,   949,   113,   114,   115,   116,    -1,    -1,
     119,   120,    -1,    -1,    -1,   804,    -1,   126,    -1,    -1,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,    -1,   910,    -1,   912,    -1,    -1,    -1,    -1,   828,
     918,    -1,   831,   832,   833,   834,    -1,    -1,   804,   838,
     839,   840,    -1,    -1,    -1,    -1,    -1,   935,    -1,    -1,
      -1,    -1,    -1,   941,    -1,    -1,    -1,    -1,    -1,    -1,
     859,   949,   828,   862,    -1,   831,   832,   833,   834,    -1,
      -1,    -1,   838,   839,   840,   874,     3,    -1,    -1,    -1,
      -1,    -1,    -1,    10,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   859,    -1,    -1,   862,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    32,    -1,    -1,   874,    -1,
      -1,   910,    -1,   912,    -1,    -1,    -1,    -1,    45,   918,
      -1,    -1,    49,    50,    -1,    -1,    -1,    -1,    55,    -1,
      57,    58,    59,    60,    -1,    -1,   935,    64,    -1,    -1,
      -1,    68,   941,    -1,   910,    -1,   912,    -1,    -1,    -1,
     949,    -1,   918,    80,    -1,    -1,    83,    -1,    85,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   935,
      -1,    -1,    -1,    -1,    -1,   941,    -1,     5,     6,     7,
       8,     9,    -1,   949,    12,    13,    14,    15,    16,    17,
      18,    19,    -1,    21,    22,    23,    24,    -1,    26,    27,
      -1,    -1,    -1,    -1,    -1,    33,    -1,    35,    36,    37,
      38,    39,    40,    -1,    -1,    43,    -1,    -1,    -1,    47,
      48,    -1,    50,    51,    52,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    62,    63,    -1,    65,    66,    -1,
      -1,    69,    -1,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    -1,    84,    -1,    86,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   119,   120,    -1,    -1,   123,    -1,   125,   126,    -1,
      -1,   129,   130,   131,   132,   133,    -1,    -1,    -1,     5,
       6,     7,     8,     9,    -1,   143,    12,    13,    14,    15,
      -1,    17,    18,    19,    -1,    21,    22,    23,    -1,    -1,
      26,    27,    -1,    -1,    -1,    -1,    -1,    33,    -1,    35,
      36,    37,    38,    39,    40,    -1,    -1,    43,    -1,    -1,
      -1,    47,    48,    -1,    50,    51,    52,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    62,    63,    -1,    65,
      66,    -1,    -1,    69,    -1,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    -1,    84,    -1,
      86,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   119,   120,    -1,    -1,   123,    -1,   125,
     126,    -1,    -1,   129,   130,   131,   132,   133,    -1,    -1,
      -1,     5,     6,     7,     8,     9,    -1,   143,    12,    13,
      14,    15,    -1,    17,    18,    19,    -1,    -1,    22,    23,
      -1,    -1,    26,    27,    -1,    -1,    -1,    -1,    -1,    33,
      -1,    35,    36,    37,    38,    39,    40,    -1,    -1,    43,
      -1,    -1,    -1,    47,    48,    -1,    50,    51,    52,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    62,    63,
      -1,    65,    66,    -1,    -1,    69,    -1,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    -1,
      -1,    -1,    86,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   119,   120,    -1,    -1,   123,
      -1,   125,   126,    -1,    -1,   129,   130,   131,   132,   133,
       5,     6,     7,     8,     9,    -1,    -1,    12,    13,   143,
      15,    -1,    -1,    18,    19,    -1,    -1,    -1,    23,    -1,
      -1,    -1,    27,     3,    -1,    -1,    -1,    -1,    33,    -1,
      10,    36,    -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,
      -1,    -1,    -1,    48,    -1,    50,    51,    52,    -1,    -1,
      -1,    -1,    32,    -1,    -1,    -1,    -1,    -1,    63,    -1,
      65,    66,    67,    -1,    69,    45,    -1,    72,    -1,    74,
      50,    76,    77,    78,    79,    55,    81,    57,    58,    59,
      60,    -1,    -1,    -1,    64,    -1,    -1,    -1,    68,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      80,    -1,    -1,    83,    -1,    85,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   119,   120,    -1,    -1,   123,   124,
     125,   126,    -1,    -1,   129,   130,   131,   132,   133,     5,
       6,     7,     8,     9,    -1,    -1,    12,    13,    -1,    15,
      -1,    -1,    18,    19,    -1,    -1,    -1,    23,    -1,    -1,
      -1,    27,    -1,    -1,    -1,    -1,    -1,    33,    -1,    -1,
      36,    -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,    -1,
      -1,    -1,    48,    -1,    50,    51,    52,    -1,    54,    -1,
      -1,    -1,    -1,    -1,    -1,    61,    -1,    63,    -1,    65,
      66,    -1,    -1,    69,    -1,    -1,    72,    -1,    74,    -1,
      76,    77,    78,    79,    -1,    81,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       5,     6,     7,     8,     9,    -1,    -1,    12,    13,    -1,
      15,    -1,    -1,    18,    19,    -1,    -1,    -1,    23,    -1,
      -1,    -1,    27,   119,   120,    -1,    -1,    -1,    33,   125,
     126,    36,    -1,   129,   130,   131,   132,   133,    43,    -1,
      -1,    -1,    -1,    48,    -1,    50,    51,    52,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    63,    -1,
      65,    66,    67,    -1,    69,    -1,    -1,    72,    -1,    74,
      -1,    76,    77,    78,    79,    -1,    81,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     5,     6,     7,     8,     9,    -1,    -1,    12,    13,
      -1,    15,    -1,    -1,    18,    19,    -1,    -1,    -1,    23,
      -1,    -1,    -1,    27,   119,   120,    -1,    -1,   123,    33,
     125,   126,    36,    -1,   129,   130,   131,   132,   133,    43,
      -1,    -1,    -1,    -1,    48,    -1,    50,    51,    52,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    63,
      -1,    65,    66,    -1,    -1,    69,    -1,    -1,    72,    -1,
      74,    -1,    76,    77,    78,    79,    -1,    81,    -1,    -1,
      84,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     5,     6,     7,     8,     9,    -1,    -1,    12,
      13,    -1,    15,    -1,    -1,    18,    19,    -1,    -1,    -1,
      23,    -1,    -1,    -1,    27,   119,   120,    -1,    -1,    -1,
      33,   125,   126,    36,    -1,   129,   130,   131,   132,   133,
      43,    -1,    -1,    -1,    -1,    48,    -1,    50,    51,    52,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      63,    -1,    65,    66,    -1,    -1,    69,    -1,    -1,    72,
      -1,    74,    -1,    76,    77,    78,    79,    -1,    81,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   119,   120,    -1,    -1,
      -1,    -1,   125,   126,   127,    -1,   129,   130,   131,   132,
     133,     5,     6,     7,     8,     9,    -1,    -1,    12,    13,
      -1,    15,    -1,    -1,    18,    19,    -1,    -1,    -1,    23,
      -1,    -1,    -1,    27,    -1,    -1,    -1,    -1,    -1,    33,
      -1,    -1,    36,    -1,    -1,    -1,    -1,    -1,    -1,    43,
      -1,    -1,    -1,    -1,    48,    -1,    50,    51,    52,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    63,
      -1,    65,    66,    -1,    -1,    69,    -1,    -1,    72,    -1,
      74,    -1,    76,    77,    78,    79,    -1,    81,    -1,    -1,
      84,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     5,     6,     7,     8,     9,    -1,    -1,    12,
      13,    -1,    15,    -1,    -1,    18,    19,    -1,    -1,    -1,
      23,    -1,    -1,    -1,    27,   119,   120,    -1,    -1,    -1,
      33,   125,   126,    36,    -1,   129,   130,   131,   132,   133,
      43,    -1,    -1,    -1,    -1,    48,    -1,    50,    51,    52,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      63,    -1,    65,    66,    -1,    -1,    69,    -1,    -1,    72,
      -1,    74,    -1,    76,    77,    78,    79,    -1,    81,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,     5,     6,     7,     8,     9,    -1,    -1,
      12,    13,    -1,    15,    -1,    -1,    18,    19,    -1,    -1,
      -1,    23,    -1,    -1,    -1,    27,   119,   120,    -1,    -1,
      -1,    33,   125,   126,    36,    -1,   129,   130,   131,   132,
     133,    43,    -1,    -1,    -1,    -1,    48,    -1,    50,    51,
      52,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    63,    -1,    65,    66,    -1,    -1,    69,    -1,    -1,
      72,    -1,    74,     5,    76,    77,    78,    79,    -1,    81,
      -1,    13,    -1,    15,    -1,    -1,    18,    -1,    20,    21,
      -1,    23,    -1,    25,    -1,    27,    -1,    29,    30,    31,
      -1,    -1,    -1,    -1,    36,    -1,    -1,    -1,    -1,    41,
      -1,    43,    44,    -1,    -1,    -1,    48,    -1,    -1,    -1,
      52,    -1,    -1,   125,    -1,    -1,    -1,    -1,   130,   131,
      -1,    63,    -1,    65,    -1,    -1,    -1,    69,    70,    -1,
       5,    -1,    -1,    -1,    -1,    77,    78,    -1,    13,    81,
      15,    -1,    84,    18,    -1,    20,    21,    -1,    23,    -1,
      25,    -1,    27,    -1,    29,    30,    31,    -1,    -1,    -1,
      -1,    36,    -1,    -1,    -1,    -1,    41,    -1,    43,    44,
      -1,    -1,    -1,    48,     5,    -1,    -1,    52,    -1,    -1,
      -1,    -1,    13,    -1,    15,    -1,    -1,    18,    63,   131,
      65,    -1,    23,    -1,    69,    70,    27,    -1,    -1,    30,
      -1,    -1,    77,    78,    -1,    36,    81,    -1,    -1,    84,
      -1,    -1,    43,    -1,    -1,    -1,    -1,    48,     5,    -1,
      -1,    52,    -1,    -1,    -1,    -1,    13,    -1,    15,    -1,
      -1,    18,    63,    -1,    65,    -1,    23,    -1,    69,    -1,
      27,    -1,    -1,    -1,    -1,    -1,    77,    78,    -1,    36,
      81,    -1,    -1,    84,    -1,    -1,    43,    -1,    -1,    -1,
      -1,    48,     5,    -1,    -1,    52,    -1,    -1,    -1,    -1,
      13,    -1,    15,    -1,    -1,    18,    63,    -1,    65,    -1,
      23,    -1,    69,    -1,    27,    -1,    -1,    -1,    -1,    -1,
      77,    78,    -1,    36,    81,    -1,    -1,    84,    -1,    -1,
      43,    -1,    -1,    -1,    -1,    48,     5,    -1,    -1,    52,
      -1,    -1,    -1,    -1,    13,    -1,    15,    -1,    -1,    18,
      63,    -1,    65,    -1,    23,    -1,    69,    -1,    27,    -1,
      -1,    -1,    -1,    -1,    77,    78,    -1,    36,    81,    -1,
      -1,    84,    -1,    -1,    43,    -1,    -1,    -1,    -1,    48,
      -1,    -1,    -1,    52,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    63,    -1,    65,    -1,    -1,    -1,
      69,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    77,    78,
      -1,    -1,    81,    -1,    -1,    84
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned short int yystos[] =
{
       0,    82,   274,   275,   283,   284,   285,   286,     5,   150,
     280,   281,     0,   276,   278,   287,   288,   289,   293,   339,
     350,   364,   371,   372,   373,   374,   383,   284,   128,   142,
     143,     5,     3,    10,    32,    45,    49,    50,    55,    57,
      58,    59,    60,    64,    68,    80,    83,    85,   290,   291,
     292,   276,   288,   374,    99,     5,   280,   128,   280,     5,
      20,    25,    29,    44,    70,   292,    30,    62,    87,    88,
      89,    90,    91,    92,    93,   375,   376,   377,   143,   123,
     145,   282,     5,     5,   141,   297,   298,    13,    15,    18,
      23,    27,    36,    43,    48,    52,    63,    65,    69,    77,
      78,    81,    84,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   280,   309,     5,     5,     5,   151,
     280,   378,   379,   381,   141,   145,   275,   143,   279,   297,
     297,   151,   159,   299,   145,   126,   126,     4,   163,   163,
     163,     5,   141,   365,   366,   141,   351,   352,   141,   341,
     342,    98,   100,   125,   380,   382,   277,   287,   145,   145,
      98,    98,   295,   125,   157,   145,   367,   299,   145,   353,
     299,   145,   100,   379,   384,     6,     7,     8,     9,    12,
      19,    33,    50,    51,    66,    72,    74,    76,    79,   119,
     120,   125,   126,   129,   130,   131,   132,   133,   148,   149,
     155,   159,   167,   168,   169,   170,   171,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   199,
     200,   201,   202,   203,   204,   205,   206,   207,   208,   209,
     210,   212,   280,   124,   146,   296,   294,   299,   151,   276,
     289,   300,   301,   302,   303,   304,   305,   315,   322,   326,
     329,   334,   337,   276,   308,   310,   311,   312,   314,   276,
     368,   369,   370,   279,   276,   354,   355,   356,   357,   359,
     360,   362,   279,   340,   384,    99,   128,   125,    84,   152,
     153,   161,   125,   147,   125,   147,   125,   194,   194,    84,
     155,   159,   199,   212,   280,   194,   194,   194,   194,   194,
     194,   128,   128,    99,   128,   125,   127,    98,   119,   120,
     121,   101,   102,   103,   104,   105,   106,   107,   108,   111,
     112,   142,   211,   126,   134,   135,   132,   133,   109,   110,
      11,    46,   115,   116,   136,   137,   113,   114,   129,   138,
     139,   117,   118,   140,    99,   125,   300,   300,   290,   146,
     302,    54,    56,    61,   313,   127,    98,     5,   146,    98,
      50,   358,   146,   356,   276,   289,   303,   304,   305,   315,
     322,   326,   329,   334,   343,   344,   345,   346,   175,     5,
     212,   125,    99,   145,   347,   147,   152,    84,   152,   212,
     126,   163,   196,   197,   198,   196,   196,   126,   127,   163,
       5,     5,   175,     5,    54,    61,   165,   166,   172,   212,
     212,     5,   212,   212,   212,   194,   194,   194,   194,   199,
     199,   200,   200,   152,   152,   201,   201,   201,   201,   202,
     202,   203,   204,   205,   206,   207,   212,   175,   172,   146,
     146,     5,    21,    30,    31,    41,    84,   131,   152,   327,
     330,   331,   333,   279,   152,   152,   311,   142,   146,   370,
      30,    84,   152,   290,   279,   146,   346,   100,   127,   172,
     175,    67,   212,   226,   227,   347,   348,   349,   127,   127,
     127,   127,   127,   198,   127,   127,   127,   191,   196,   100,
     164,   212,   164,    98,   127,   141,   100,   127,   279,   279,
     147,     5,   152,   229,   230,   152,    53,    53,   280,     5,
       5,    53,    72,   224,   225,   280,   281,   328,   387,   143,
     145,   217,   338,     5,     5,   212,   213,   152,     5,     5,
      72,   127,   100,   152,   146,    98,   194,   194,   194,   194,
     127,   166,   212,   308,   142,     5,   229,    98,   143,   224,
     280,   152,   152,   147,   125,   142,    33,    74,   109,   110,
     113,   114,   115,   116,   119,   120,   126,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   332,    99,
      98,   143,   147,   387,    72,    99,   145,     5,    14,    17,
      19,    21,    22,    26,    35,    37,    38,    39,    40,    47,
      62,    71,    73,    75,    79,    80,    82,    86,   143,   152,
     155,   159,   171,   178,   179,   181,   192,   193,   194,   210,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   228,
     231,   232,   233,   234,   235,   241,   242,   243,   244,   245,
     253,   254,   255,   256,   257,   258,   260,   261,   262,   263,
     266,   267,   268,   269,   271,   280,     5,   125,   125,   387,
      99,   162,   163,    99,   146,   226,   194,   127,   213,   143,
     230,   143,   385,   125,   125,   308,   127,   226,   125,   310,
       5,   225,   308,   145,   310,   276,   316,   319,   320,   141,
     143,   147,   217,   217,   152,   143,   216,   125,   147,   147,
       5,    16,    24,   125,   125,   212,   259,   147,   259,   217,
     217,   217,   125,   125,   224,   146,   215,   143,   143,   143,
     263,   143,   145,   363,   308,   308,   145,   310,   183,   347,
     162,   212,   141,   335,   336,   145,   152,   152,   127,   217,
     152,   100,   127,   316,   100,    94,    95,   146,   276,   317,
     320,   276,   318,   319,   215,   151,   159,   229,    86,   152,
     223,   232,   246,   249,   252,   152,   143,   213,   143,   210,
     212,   214,   212,   143,   212,   143,    34,   262,   265,   212,
     223,   270,   210,   214,   146,   127,   127,   276,   361,   100,
     100,    12,    72,   143,   145,   276,   323,   324,   325,     5,
       5,   143,   145,     5,   143,   145,   146,   388,   388,   388,
       5,   264,   264,   125,     5,   272,   273,   143,    98,     5,
     143,   127,   100,   127,   127,   127,   217,   265,   127,   127,
     127,   363,   363,    94,    95,   146,   387,   147,   147,   218,
      96,    97,   146,   276,   325,   276,   324,   127,   127,   307,
      98,   127,   306,   388,   143,   217,   321,   321,   127,   127,
     210,   214,   142,    98,   127,   214,   247,   250,   232,    42,
     216,   216,   216,   216,   145,   216,   216,   216,   363,   363,
     388,   145,   172,   172,   146,   386,   386,   386,   218,   152,
     218,   387,   387,   217,   217,   127,   127,   212,   273,   216,
     143,   212,    28,    16,    24,   236,   237,   238,   239,   240,
     276,   276,   361,   127,   127,   217,   217,   146,     5,   146,
     143,   143,   248,   251,   252,   127,   216,   213,   141,   146,
     238,   219,   240,    95,    94,   146,   385,   385,   127,   127,
     216,   141,   363,   363,   388,   216
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (0)


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (N)								\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (0)
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
              (Loc).first_line, (Loc).first_column,	\
              (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname[yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);


# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()
    ;
#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a look-ahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to look-ahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 101 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.scope) = Myparser->createNewScope(scopecount);scopecount++;;}
    break;

  case 4:
#line 104 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {Myparser->finishscop();;}
    break;

  case 7:
#line 109 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ; ;}
    break;

  case 8:
#line 110 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  //class Node* tnode = new Node() ; $<r.node>$ = tnode ;  
 (yyval.r.i) = (yyvsp[0].r.i);
(yyval.r.node) = new LeafNode((yyvsp[0].r.i), (yyvsp[0].r.myLineNo), (yyvsp[0].r.myColno)); 
   cout<<"****** INTEGER_LITERALliteral*******"; ;}
    break;

  case 9:
#line 114 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {					(yyval.r.f) = (yyvsp[0].r.f);
   (yyval.r.node) = new LeafNode((yyvsp[0].r.f), (yyvsp[0].r.myLineNo), (yyvsp[0].r.myColno)); ;}
    break;

  case 11:
#line 117 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {					(yyval.r.str) = (yyvsp[0].r.str);
  (yyval.r.node) = new LeafNode(std::string((yyvsp[0].r.str)), (yyvsp[0].r.myLineNo), (yyvsp[0].r.myColno));;}
    break;

  case 13:
#line 122 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = new LeafNode(bool(true), (yyvsp[0].r.myLineNo), (yyvsp[0].r.myColno));;}
    break;

  case 14:
#line 123 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = new LeafNode(bool(false), (yyvsp[0].r.myLineNo), (yyvsp[0].r.myColno));;}
    break;

  case 16:
#line 132 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.str)=(yyvsp[0].r.str);;}
    break;

  case 17:
#line 136 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.m) = (yyvsp[0].r.m); ;}
    break;

  case 19:
#line 140 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.m) = (yyvsp[0].r.m); ;}
    break;

  case 20:
#line 141 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.m) = TYPENAME; ;}
    break;

  case 21:
#line 144 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.m) = (yyvsp[0].r.m); ;}
    break;

  case 23:
#line 146 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.m) = (yyvsp[0].r.m); ;}
    break;

  case 24:
#line 149 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.m) = (yyvsp[0].r.m); ;}
    break;

  case 25:
#line 150 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = BOOLT; 
		 return_type = BOOLT;
		 ;}
    break;

  case 26:
#line 156 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.m) = (yyvsp[0].r.m); ;}
    break;

  case 27:
#line 157 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = (yyvsp[0].r.m);;}
    break;

  case 28:
#line 158 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.m) = DECIMALT; ;}
    break;

  case 29:
#line 161 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = SBYTET; 
		 return_type = SBYTET;
		 ;}
    break;

  case 30:
#line 165 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = BYTET; 
		 return_type = BYTET;
		 ;}
    break;

  case 31:
#line 169 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = SHORTT; 
		 return_type = SHORTT;
		 ;}
    break;

  case 32:
#line 173 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = USHORTT; 
		 return_type = USHORTT;
		 ;}
    break;

  case 33:
#line 177 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = INTT; 
		 return_type = INTT;
		 ;}
    break;

  case 34:
#line 181 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = UINTT; 
		 return_type = UINTT;
		 ;}
    break;

  case 35:
#line 185 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = LONGT; 
		 return_type = LONGT;
		 ;}
    break;

  case 36:
#line 189 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = ULONGT; 
		 return_type = ULONGT;
		 ;}
    break;

  case 37:
#line 193 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = CHART; 
		 return_type = CHART;
		 ;}
    break;

  case 38:
#line 199 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = FLOATT; 
		 return_type = FLOATT;
		 ;}
    break;

  case 39:
#line 203 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = DOUBLET; 
		 return_type = DOUBLET;
		 ;}
    break;

  case 42:
#line 212 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = POINTERT; 
		 return_type = POINTERT;
		 ;}
    break;

  case 43:
#line 216 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = POINTERT; 
		 return_type = POINTERT;
		 ;}
    break;

  case 51:
#line 240 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {ListNode* list = new ListNode("argument_list");
			   list->add_node((yyvsp[0].r.node));
                (yyval.r.node) = list;
				;}
    break;

  case 52:
#line 244 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  dynamic_cast<ListNode*>((yyvsp[-2].r.node))->add_node((yyvsp[0].r.node));;}
    break;

  case 53:
#line 248 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 57:
#line 254 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 58:
#line 257 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;  cout<<"******literal*******";;}
    break;

  case 59:
#line 258 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 61:
#line 260 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;cout<<"******invocation_expression*******";;}
    break;

  case 64:
#line 263 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 65:
#line 264 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 66:
#line 265 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 67:
#line 266 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 68:
#line 267 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 76:
#line 283 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = nullptr;;}
    break;

  case 77:
#line 284 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 87:
#line 306 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[-1].r.node) ;;}
    break;

  case 89:
#line 312 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 90:
#line 315 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {	 (yyval.r.node) = new NewNode((yyvsp[-1].r.node), (yyvsp[-4].r.myLineNo), (yyvsp[-4].r.myColno));;}
    break;

  case 102:
#line 345 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 103:
#line 346 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 104:
#line 347 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 107:
#line 352 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 111:
#line 358 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
    class LeafNode* tnode = new LeafNode(1,0,0) ;
  (yyval.r.node) = new AssignmentNode((yyvsp[0].r.node), new BinaryOperationNode("+", tnode, (yyvsp[0].r.node), (yyvsp[-1].r.myLineNo), (yyvsp[-1].r.myColno)), (yyvsp[-1].r.myLineNo), (yyvsp[-1].r.myColno)); 
    cout<<"******  pre_increment_expression*******\n";;}
    break;

  case 112:
#line 364 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 113:
#line 367 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;  cout<<"******  unary_expression_not_plusminus*******\n";;}
    break;

  case 114:
#line 368 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ; cout<<"****** '+' unary_expression*******\n";;}
    break;

  case 117:
#line 371 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ; cout<<"******  pre_increment_expression*******\n";;}
    break;

  case 118:
#line 372 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 132:
#line 397 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ; cout<<"******  multiplicative_expression*******\n";;}
    break;

  case 133:
#line 398 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) =	new BinaryOperationNode("*", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 134:
#line 399 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) =	new BinaryOperationNode("/", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 135:
#line 400 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) =	new BinaryOperationNode("%", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 136:
#line 403 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 137:
#line 404 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
          //    class Node* tnode = new Node() ;
		//	class Node* tnode2 = new Node();

            //  ListNode* list = new ListNode("BinaryOperation");
									
		  (yyval.r.node) =	new BinaryOperationNode("+", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));
               // $<r.node>$ = list;
				cout<<"****** adddddddddddddddd additive_expression*******\n";
		
				;}
    break;

  case 138:
#line 415 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {	 (yyval.r.node) =	new BinaryOperationNode("-", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 139:
#line 418 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  (yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 142:
#line 423 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  (yyval.r.node) = (yyvsp[0].r.node); cout<<"****** shift_expression *******\n";;}
    break;

  case 143:
#line 424 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {	(yyval.r.node) =	new BinaryOperationNode("<", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 144:
#line 425 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {	(yyval.r.node) =	new BinaryOperationNode(">", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 149:
#line 432 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"****** relational_expression *******\n";;}
    break;

  case 150:
#line 433 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) =new BinaryOperationNode("==", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 151:
#line 434 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) =new BinaryOperationNode("!=", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 152:
#line 437 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"****** equality_expression *******\n";;}
    break;

  case 154:
#line 441 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"****** and_expression *******\n";;}
    break;

  case 156:
#line 445 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  (yyval.r.node) = (yyvsp[0].r.node); cout<<"****** exclusive_or_expression *******\n";;}
    break;

  case 158:
#line 449 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"****** inclusive_or_expression *******\n";;}
    break;

  case 159:
#line 450 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) =	new BinaryOperationNode("&&", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 160:
#line 453 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"****** conditional_and_expression *******\n";;}
    break;

  case 161:
#line 454 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) =	new BinaryOperationNode("||", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 162:
#line 457 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  (yyval.r.node) = (yyvsp[0].r.node); cout<<"****** conditional_or_expression *******\n";;}
    break;

  case 164:
#line 461 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"********* assignment (expression) ************\n ";
  class Node* tnode = new Node() ;
class Node* tnode2 = new Node();
  (yyval.r.node) =new AssignmentNode( (yyvsp[-2].r.node) , (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));	
   ;}
    break;

  case 165:
#line 468 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"********* assignment (expression) ************\n ";
   (yyval.r.node) = new AssignmentNode((yyvsp[-2].r.node), new BinaryOperationNode("+", (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno)), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno)); 
	
   ;}
    break;

  case 166:
#line 474 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"********* assignment 2 ************\n ";
  (yyval.r.node) =new AssignmentNode( (yyvsp[-2].r.node) , (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));

   ;}
    break;

  case 176:
#line 485 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"********* conditional_expression************\n "; ;}
    break;

  case 177:
#line 486 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); cout<<"********* expression************\n "; ;}
    break;

  case 179:
#line 492 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 180:
#line 496 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); cout<<"********* labeled_statement************\n "; ;}
    break;

  case 181:
#line 497 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); cout<<"*********  declaration_statement************\n "; ;}
    break;

  case 182:
#line 498 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node);cout<<"********* embedded_statement************\n ";  ;}
    break;

  case 183:
#line 501 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"********* block************\n ";  ;}
    break;

  case 184:
#line 502 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"********* empty_statement************\n ";  ;}
    break;

  case 185:
#line 503 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); cout<<"********* expression_statement************\n ";  ;}
    break;

  case 186:
#line 504 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 187:
#line 505 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 188:
#line 506 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 189:
#line 507 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 196:
#line 516 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[-1].r.node);  (yyvsp[-2].r.scope)->setownername("block");;}
    break;

  case 197:
#line 519 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = nullptr; ;}
    break;

  case 198:
#line 520 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); cout<<"********* statement_list_opt************\n ";  ;}
    break;

  case 199:
#line 524 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {	
               ListNode* list = new ListNode("statement_list_Node");
			   list->add_node((yyvsp[0].r.node));
                (yyval.r.node) = list;
			
					;}
    break;

  case 200:
#line 530 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {		
									dynamic_cast<ListNode*>((yyvsp[-1].r.node))->add_node((yyvsp[0].r.node));
									
							;}
    break;

  case 201:
#line 536 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = nullptr; cout<<"********* empty_statement************\n ";  ;}
    break;

  case 203:
#line 542 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[-1].r.node) ;;}
    break;

  case 204:
#line 543 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[-1].r.node) ;;}
    break;

  case 205:
#line 546 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { Myparser->removev ((yyvsp[0].r.symbol)->name ,Myparser->getCurrentScope() );
							    (yyval.r.symbol) = Myparser->insertSymbol(new Variable((yyvsp[0].r.symbol)->name,VARIABLE, (yyvsp[-1].r.myColno), (yyvsp[-1].r.myLineNo),(yyvsp[-1].r.m))); 
							  (yyval.r.node) = (yyvsp[0].r.node);
							     //    $<r.node>$ = new VariableDec($<r.symbol>$,$<r.node>2, $<r.myLineNo>1, $<r.myColno>1);
							 //not sure
							  ;}
    break;

  case 206:
#line 554 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {	ListNode* list = new ListNode("VariablelistNode");
						  list->add_node((yyvsp[0].r.node));
						 (yyval.r.node) = list;
					   ;}
    break;

  case 207:
#line 560 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {

			Symbol* trav = (yyvsp[-2].r.symbol);
			Symbol* travtemp = trav;
			while(trav != nullptr){
			    travtemp = trav;
				trav = trav->node;
		    }
			travtemp->node = (yyvsp[0].r.symbol);
			(yyval.r.symbol) = (yyvsp[-2].r.symbol);
			///////////////////////////////
		dynamic_cast<ListNode*>((yyvsp[-2].r.node))->add_node((yyvsp[0].r.node));

				               	;}
    break;

  case 208:
#line 576 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.symbol) = Myparser->insertSymbol(new Variable((yyvsp[0].r.str),VARIABLE,  (yyvsp[0].r.myColno), (yyvsp[0].r.myLineNo),return_type));
                       (yyval.r.node) = new VariableNode((yyval.r.symbol), (yyvsp[0].r.myLineNo), (yyvsp[0].r.myColno));
				//  $<r.node>$ =	new IDNode( $<r.myLineNo>1, $<r.myColno>1,$<r.str>1);

				 ;}
    break;

  case 209:
#line 583 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.symbol) = Myparser->insertSymbol(new Variable((yyvsp[-2].r.str),VARIABLE, (yyvsp[-2].r.myColno), (yyvsp[-2].r.myLineNo),return_type));
			                             (yyval.r.node) =new AssignmentNode(new VariableNode((yyval.r.symbol), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno)), (yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));	
										;}
    break;

  case 210:
#line 588 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 211:
#line 589 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node)=new Node();;}
    break;

  case 212:
#line 590 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node)=new Node();;}
    break;

  case 214:
#line 596 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  Myparser->removev ((yyvsp[0].r.symbol)->name ,Myparser->getCurrentScope() );
									 (yyval.r.symbol) = Myparser->insertSymbol(new Variable((yyvsp[0].r.symbol)->name,VARIABLE, (yyvsp[-2].r.myColno), (yyvsp[-2].r.myLineNo),(yyvsp[-1].r.m))); 
									;}
    break;

  case 216:
#line 603 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {

			Symbol* trav = (yyvsp[-2].r.symbol);
			Symbol* travtemp = trav;
			while(trav != nullptr){
			    travtemp = trav;
				trav = trav->node;
		    }
			travtemp->node = (yyvsp[0].r.symbol);
			(yyval.r.symbol) = (yyvsp[-2].r.symbol);
											;}
    break;

  case 217:
#line 616 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  (yyval.r.symbol) = Myparser->insertSymbol(new Variable((yyvsp[-2].r.str),VARIABLE, (yyvsp[-2].r.myColno), (yyvsp[-2].r.myLineNo),return_type)); ;}
    break;

  case 218:
#line 617 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {errorRec.errQ->enqueue((yyvsp[0].r.myLineNo),(yyvsp[0].r.myColno),"A const field requaires a valu to be provided",""); (yyval.r.symbol) = Myparser->insertSymbol(new Variable((yyvsp[0].r.str),VARIABLE, (yyvsp[0].r.myColno), (yyvsp[0].r.myLineNo),return_type));
																													
  ;}
    break;

  case 219:
#line 624 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[-1].r.node);;}
    break;

  case 220:
#line 627 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"******invocation_expression1*******\n";;}
    break;

  case 221:
#line 628 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);cout<<"******object_creation_expression*******\n";;}
    break;

  case 222:
#line 629 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); cout<<"******assignment1*******\n";;}
    break;

  case 224:
#line 631 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 226:
#line 633 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 227:
#line 636 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 228:
#line 637 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 229:
#line 640 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {		
			Node *else_node = nullptr;
			class Node* tnode = new Node() ;
			class Node* tnode2 = new Node() ;	
			 (yyval.r.node) = new IfNode((yyvsp[-2].r.node), (yyvsp[0].r.node), else_node, (yyvsp[-4].r.myLineNo), (yyvsp[-4].r.myColno)); 
			 cout<<"*********IF 1 ************\n ";
;}
    break;

  case 230:
#line 647 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
  			 cout<<"*********IF 2 ************\n ";
  errorRec.errQ->enqueue((yyvsp[-2].r.myLineNo),(yyvsp[-2].r.myColno),"Expected syntex ERROR in if statement","");;}
    break;

  case 231:
#line 650 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
								Node *else_node = new ElseNode((yyvsp[0].r.node), (yyvsp[-1].r.myLineNo), (yyvsp[-1].r.myColno));
								//	class Node* tnode = new Node() ;
								//	class Node* tnode2 = new Node();
								(yyval.r.node) = new IfNode((yyvsp[-4].r.node),(yyvsp[-2].r.node), else_node, (yyvsp[-6].r.myLineNo), (yyvsp[-6].r.myColno));
								cout<<"******if else statementx*******";;}
    break;

  case 232:
#line 656 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
  errorRec.errQ->enqueue((yyvsp[-3].r.myLineNo),(yyvsp[-3].r.myColno),"Expected syntex ERROR in if condition (not bool expression)","");;}
    break;

  case 233:
#line 660 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"*********switch statement************\n ";
																					(yyvsp[-2].r.scope)->setownername("SWITCH");
																				//	class Node* tnode = new Node() ;
																				//	class Node* tnode2 = new Node();
																					(yyval.r.node) = new SwitchNode((yyvsp[-4].r.node),(yyvsp[-2].r.node), (yyvsp[-6].r.myLineNo),(yyvsp[-6].r.myColno));
																					;}
    break;

  case 243:
#line 687 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 245:
#line 689 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 248:
#line 696 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
														cout<<"*********while_statement*********\n";
												//	class Node* tnode = new Node() ;
												//	class Node* tnode2 = new Node() ;
		                                           (yyval.r.node) = new WhileNode((yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-4].r.myLineNo), (yyvsp[-4].r.myColno)); 
														;}
    break;

  case 249:
#line 702 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {errorRec.errQ->enqueue((yyvsp[-3].r.myLineNo),(yyvsp[-3].r.myColno),"Expected syntex ERROR in while condition (not bool expression)","");;}
    break;

  case 250:
#line 705 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"*********do while atatement*********\n";;}
    break;

  case 251:
#line 706 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {errorRec.errQ->enqueue((yyvsp[-3].r.myLineNo),(yyvsp[-3].r.myColno),"Expected syntex ERROR in do_while condition (not bool expression)","");;}
    break;

  case 252:
#line 709 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { 
																		cout<<"*********for_statement**********\n";
																		class Node* tnode = new Node() ;
												                    	class Node* tnode2 = new Node() ;
																		class Node* tnode3 = new Node();
								(yyval.r.node) = new ForNode((yyvsp[-6].r.node), (yyvsp[-4].r.node), (yyvsp[-2].r.node), (yyvsp[0].r.node), (yyvsp[-8].r.myLineNo), (yyvsp[-8].r.myColno));	
								;}
    break;

  case 254:
#line 719 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 255:
#line 722 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = nullptr;;}
    break;

  case 256:
#line 723 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 258:
#line 727 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 259:
#line 730 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 261:
#line 734 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 262:
#line 737 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 263:
#line 740 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 266:
#line 747 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 267:
#line 748 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 269:
#line 750 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 271:
#line 754 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = new BreakNode((yyvsp[-1].r.myLineNo), (yyvsp[-1].r.myColno));;}
    break;

  case 272:
#line 757 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = new ContinueNode((yyvsp[-1].r.myLineNo), (yyvsp[-1].r.myColno));;}
    break;

  case 276:
#line 765 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = new ReturnNode((yyvsp[-1].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));;}
    break;

  case 280:
#line 775 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {    
 class catchNode *cat =  new catchNode((yyvsp[0].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));
  (yyval.r.node) = new tryNode((yyvsp[-1].r.node), cat, (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));
  cout<<"*********try_catch********\n";;}
    break;

  case 281:
#line 779 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {errorRec.errQ->enqueue((yyvsp[0].r.myLineNo),(yyvsp[0].r.myColno),"Unexpected catch without try","");;}
    break;

  case 284:
#line 785 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 288:
#line 791 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {  (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 291:
#line 798 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 292:
#line 801 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 293:
#line 804 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 302:
#line 828 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"*********starrrrrrrrrtttttttttt\n";;}
    break;

  case 303:
#line 829 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"*********starrrrrrrrrtttttttttt\n";
														tree->add_nodes(dynamic_cast<ListNode*>((yyvsp[0].r.node))->nodes);
                                                        ;}
    break;

  case 309:
#line 843 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 310:
#line 846 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[-1].r.node);;}
    break;

  case 313:
#line 854 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.str) = (yyvsp[0].r.str) ; 
//class Node* tnode = new Node() ;

  (yyval.r.node) =	new IDNode( (yyvsp[0].r.myLineNo), (yyvsp[0].r.myColno),(yyvsp[0].r.str));
   ;}
    break;

  case 314:
#line 859 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { 

           char* trav = (yyvsp[-1].r.str);
		   string strn(trav);
		   string strn2((yyvsp[0].r.str));
		   strn.append(strn2);
		    const char *cstr = strn.c_str();
			trav = strdup(cstr);
			(yyval.r.str) = trav;

	;}
    break;

  case 315:
#line 872 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.str) = (yyvsp[-1].r.str) ;;}
    break;

  case 316:
#line 873 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
  
           char* trav = (yyvsp[-2].r.str);
		   string strn(trav);
		   string strn2((yyvsp[-1].r.str));
		    strn.append(".");
		   strn.append(strn2);
		    const char *cstr = strn.c_str();
			trav = strdup(cstr);
			(yyval.r.str) = trav;


	;}
    break;

  case 317:
#line 888 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyvsp[-3].r.scope)->setownername("namespace");
                                                                              { (yyval.r.node) = (yyvsp[-1].r.node);}
																			 ;}
    break;

  case 324:
#line 907 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {                 // $<r.node>$ =$<r.node>1;
  	                                         	//	dynamic_cast<ListNode*>($<r.node>$)->add_node($<r.node>1); 

														ListNode* list = new ListNode("NameSpaceNode");
														list->add_node((yyvsp[0].r.node));
														 (yyval.r.node) = list;																				
			;}
    break;

  case 325:
#line 914 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { 
															 dynamic_cast<ListNode*>((yyvsp[-1].r.node))->add_node((yyvsp[0].r.node));
															  ;}
    break;

  case 326:
#line 920 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 327:
#line 921 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node);;}
    break;

  case 328:
#line 924 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node) = (yyvsp[0].r.node); ;}
    break;

  case 337:
#line 941 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = ABSTRACTT;
				modi[modCounter++] = ABSTRACTT;;}
    break;

  case 338:
#line 944 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = EXTERNT;
				modi[modCounter++] = EXTERNT;;}
    break;

  case 339:
#line 947 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = INTERNALT;
				modi[modCounter++] = INTERNALT;;}
    break;

  case 340:
#line 950 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = NEWT;
				modi[modCounter++] = NEWT;;}
    break;

  case 341:
#line 953 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = OVERRIDET;
				modi[modCounter++] = OVERRIDET;;}
    break;

  case 342:
#line 956 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = PRIVATET;
				modi[modCounter++] = PRIVATET;;}
    break;

  case 343:
#line 959 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = PROTECTEDT;
				modi[modCounter++] = PROTECTEDT;;}
    break;

  case 344:
#line 962 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = PUBLICT;
				modi[modCounter++] = PUBLICT;;}
    break;

  case 345:
#line 965 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = READONLYT;
				modi[modCounter++] = READONLYT;;}
    break;

  case 346:
#line 968 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = SEALEDT;
				modi[modCounter++] = SEALEDT;;}
    break;

  case 347:
#line 971 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = STATICT;
				modi[modCounter++] = STATICT;;}
    break;

  case 348:
#line 974 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = UNSAFET;
				modi[modCounter++] = UNSAFET;;}
    break;

  case 349:
#line 977 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = VIRTUALT;
				modi[modCounter++] = VIRTUALT;;}
    break;

  case 350:
#line 980 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = VOLATILET;
				modi[modCounter++] = VOLATILET;;}
    break;

  case 351:
#line 982 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.m) = FINALT;
				modi[modCounter++] = FINALT;;}
    break;

  case 352:
#line 987 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {

  Class* Myclass = new Class((yyvsp[-2].r.str), (yyvsp[-3].r.myColno), (yyvsp[-3].r.myLineNo),dynamic_cast<Scope*>((yyvsp[0].r.scope)),modi,modCounter);
  Myparser->setClass((yyvsp[-1].r.str), Myclass, dynamic_cast<Scope*>((yyvsp[0].r.scope)),Myparser->getCurrentClassSym());
  (yyval.r.symbol) = Myparser->insertSymbol(Myclass, dynamic_cast<Scope*>((yyvsp[0].r.scope))->getParentScope());	
  modCounter=0;	
   Myparser->pushToClassesStack(Myclass);							 
																	     ;}
    break;

  case 353:
#line 995 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
   (yyval.r.symbol)=Myparser->getCurrentClassSym();Myparser->checksetconstructer(dynamic_cast<Class*>((yyval.r.symbol)));Myparser->popFromClassesStack();
   // ListNode* list = new ListNode();
   (yyval.r.node) = new ClassDefineNode((yyval.r.symbol), (yyvsp[-2].r.node), (yyvsp[-4].r.myLineNo), (yyvsp[-4].r.myColno)); 
   cout<<"*********class_declerationssss***********\n"; ;}
    break;

  case 354:
#line 1001 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
   Class* Myclass = new Class(nullptr, (yyvsp[-2].r.myColno), (yyvsp[-2].r.myLineNo),dynamic_cast<Scope*>((yyvsp[0].r.scope)),modi,modCounter);
  Myparser->setClass((yyvsp[-1].r.str), Myclass, dynamic_cast<Scope*>((yyvsp[0].r.scope)),Myparser->getCurrentClassSym());
  (yyval.r.symbol) = Myparser->insertSymbol(Myclass, dynamic_cast<Scope*>((yyvsp[0].r.scope))->getParentScope());	
  modCounter=0;	
   Myparser->pushToClassesStack(Myclass);
																  ;}
    break;

  case 355:
#line 1008 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.symbol)=Myparser->getCurrentClassSym();Myparser->checksetconstructer(dynamic_cast<Class*>((yyval.r.symbol)));Myparser->popFromClassesStack();errorRec.errQ->enqueue((yyvsp[-6].r.myLineNo),(yyvsp[-6].r.myColno),"Unexpected class without name","");	;}
    break;

  case 356:
#line 1010 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
    Class* Myclass = new Class((yyvsp[-2].r.str), (yyvsp[-3].r.myColno), (yyvsp[-3].r.myLineNo),dynamic_cast<Scope*>((yyvsp[0].r.scope)),modi,modCounter);
  Myparser->setClass((yyvsp[-1].r.str), Myclass, dynamic_cast<Scope*>((yyvsp[0].r.scope)),Myparser->getCurrentClassSym());
  (yyval.r.symbol) = Myparser->insertSymbol(Myclass, dynamic_cast<Scope*>((yyvsp[0].r.scope))->getParentScope());	
  modCounter=0;	
   Myparser->pushToClassesStack(Myclass);
																			 ;}
    break;

  case 357:
#line 1017 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.symbol)=Myparser->getCurrentClassSym();Myparser->checksetconstructer(dynamic_cast<Class*>((yyval.r.symbol)));Myparser->popFromClassesStack();errorRec.errQ->enqueue((yyvsp[-7].r.myLineNo),(yyvsp[-7].r.myColno),"Unexpected keyword class wrong ","");modCounter=0;	;}
    break;

  case 358:
#line 1020 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.str) = nullptr;;}
    break;

  case 359:
#line 1021 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.str) = (yyvsp[0].r.str);;}
    break;

  case 361:
#line 1025 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.str) = (yyvsp[0].r.str);;}
    break;

  case 363:
#line 1029 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.str) = (yyvsp[0].r.str);;}
    break;

  case 365:
#line 1033 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {   (yyval.r.node) = nullptr ;;}
    break;

  case 366:
#line 1034 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) =(yyvsp[0].r.node);;}
    break;

  case 367:
#line 1038 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {ListNode* list = new ListNode("ClassMemberlistNode");
								 list->add_node((yyvsp[0].r.node));
								  (yyval.r.node) = list
								  ;}
    break;

  case 368:
#line 1042 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { dynamic_cast<ListNode*>((yyvsp[-1].r.node))->add_node((yyvsp[0].r.node)); ;}
    break;

  case 369:
#line 1045 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node)  = (yyvsp[0].r.node) ; ;}
    break;

  case 370:
#line 1046 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node)  = (yyvsp[0].r.node) ; ;}
    break;

  case 371:
#line 1047 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node)  = (yyvsp[0].r.node) ;//cout<< $<r.symbol>$->getName();
   ;}
    break;

  case 372:
#line 1050 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {ListNode* list = new ListNode("property_declaration");
                          list->add_node(new Node() );
	                      (yyval.r.node) = list;
						 ;}
    break;

  case 373:
#line 1054 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {ListNode* list = new ListNode("event_declaration");
                         list->add_node(new Node() );
	                     (yyval.r.node) = list;
						;}
    break;

  case 374:
#line 1058 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {ListNode* list = new ListNode("indexer_declaration");
                         list->add_node(new Node() );
	                     (yyval.r.node) = list;
						;}
    break;

  case 375:
#line 1062 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { ListNode* list = new ListNode("operator_declaration");
                         list->add_node(new Node() );
	                     (yyval.r.node) = list;
						;}
    break;

  case 376:
#line 1066 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {ListNode* list = new ListNode("constructor_declaration");
                         list->add_node(new Node() );
	                     (yyval.r.node) = list;
						;}
    break;

  case 377:
#line 1070 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {ListNode* list = new ListNode("destructor_declaration");
                         list->add_node(new Node() );
	                     (yyval.r.node) = list;
						;}
    break;

  case 378:
#line 1074 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { (yyval.r.node)  = (yyvsp[0].r.node) ; ;}
    break;

  case 379:
#line 1077 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
  Myparser->removev ((yyvsp[-1].r.symbol)->name ,Myparser->getCurrentScope() );
  (yyval.r.symbol) = Myparser->insertSymbol(new DataMember((yyvsp[-1].r.symbol)->name, (yyvsp[-3].r.myColno), (yyvsp[-3].r.myLineNo),(yyvsp[-2].r.m),modi,modCounter));
  	modCounter=0;
	// $<r.node>$ = new ClassMemNode($<r.symbol>$,$<r.node>4, $<r.myLineNo>3, $<r.myColno>3);

  ;}
    break;

  case 380:
#line 1084 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {errorRec.errQ->enqueue((yyvsp[-2].r.myLineNo),(yyvsp[-2].r.myColno),"constant without type\n.......>not inserted in SymbolTable","");modCounter=0;	;}
    break;

  case 381:
#line 1087 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
  Myparser->removev ((yyvsp[-1].r.symbol)->name ,Myparser->getCurrentScope() );
  (yyval.r.symbol) = Myparser->insertSymbol(new DataMember((yyvsp[-1].r.symbol)->name, (yyvsp[-2].r.myColno), (yyvsp[-2].r.myLineNo),(yyvsp[-2].r.m),modi,modCounter));
  modCounter=0;	
  (yyval.r.node) = new ClassMemNode((yyval.r.symbol),(yyvsp[-1].r.node), (yyvsp[-2].r.myLineNo), (yyvsp[-2].r.myColno));
  ;}
    break;

  case 382:
#line 1097 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
 (yyval.r.symbol) = Myparser->insertFunctionSymbol((yyvsp[-4].r.str),(yyvsp[-5].r.m),(yyvsp[-5].r.myColno), (yyvsp[-5].r.myLineNo) ,dynamic_cast<Scope*>((yyvsp[0].r.scope)) ,(yyvsp[-2].r.symbol),modi,modCounter);
  modCounter=0;	
  ;}
    break;

  case 383:
#line 1101 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
								cout<<"*********method declaration1**********\n";
(yyval.r.node) = new FunctionDefineNode(Myparser->lookUpSymbol((yyvsp[-3].r.scope)->getParentScope(),(yyvsp[-7].r.str)), (yyvsp[-1].r.node), (yyvsp[-5].r.node), (yyvsp[-4].r.myLineNo), (yyvsp[-4].r.myColno));

                              ;}
    break;

  case 384:
#line 1109 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
  Myparser->insertFunctionSymbol((yyvsp[-4].r.str),(yyvsp[-5].r.m),(yyvsp[-5].r.myColno), (yyvsp[-5].r.myLineNo) ,(yyvsp[-2].r.symbol),modi,modCounter);
   modCounter=0;cout<<"*********method declaration2**********\n";	;}
    break;

  case 385:
#line 1115 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
  Myparser->insertFunctionSymbol((yyvsp[-4].r.str),(yyvsp[-5].r.m),(yyvsp[-5].r.myColno), (yyvsp[-5].r.myLineNo) ,(yyvsp[-2].r.symbol),modi,modCounter);
   modCounter=0;cout<<"*********method declaration3**********\n";	;}
    break;

  case 386:
#line 1121 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
   Myparser->insertFunctionSymbol((yyvsp[-4].r.str),(yyvsp[-5].r.m),(yyvsp[-5].r.myColno), (yyvsp[-5].r.myLineNo) ,dynamic_cast<Scope*>((yyvsp[0].r.scope)) ,(yyvsp[-2].r.symbol),modi,modCounter);
  modCounter=0;	
  ;}
    break;

  case 387:
#line 1125 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
  (yyval.r.node) = new FunctionDefineNode(Myparser->lookUpSymbol((yyvsp[-3].r.scope)->getParentScope(),(yyvsp[-7].r.str)), (yyvsp[-1].r.node), (yyvsp[-5].r.node), (yyvsp[-4].r.myLineNo), (yyvsp[-4].r.myColno));
  cout<<"*********method declaratio4n**********\n";;}
    break;

  case 388:
#line 1131 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.symbol) = nullptr; (yyval.r.node) = nullptr;;}
    break;

  case 389:
#line 1132 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.symbol) = (yyvsp[0].r.symbol); (yyval.r.node) = (yyvsp[0].r.node) ;}
    break;

  case 391:
#line 1136 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
		 (yyval.r.m) = VOIDT; 
		 return_type = VOIDT;
		 ;}
    break;

  case 392:
#line 1142 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {	ListNode* list = new ListNode("ParametarListNode");
						list->add_node((yyvsp[0].r.node));
						(yyval.r.node) = list;
					;}
    break;

  case 393:
#line 1146 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {

			Symbol* trav = (yyvsp[-2].r.symbol);
			Symbol* travtemp = trav;
			while(trav != nullptr){
			    travtemp = trav;
				trav = trav->node;
		    }
			travtemp->node = (yyvsp[0].r.symbol);
			(yyval.r.symbol) = (yyvsp[-2].r.symbol);
			//node chain
		

			ListNode* list = dynamic_cast<ListNode*>((yyvsp[-2].r.node));
			list->add_node((yyvsp[0].r.node));
			(yyval.r.node) = list;
											;}
    break;

  case 394:
#line 1167 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.symbol) = (yyvsp[0].r.symbol); (yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 395:
#line 1168 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.symbol) = (yyvsp[0].r.symbol); (yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 396:
#line 1171 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
						Parameter* paramSymbol = new Parameter((yyvsp[0].r.str), (yyvsp[-1].r.myColno),(yyvsp[-1].r.myLineNo), (yyvsp[-1].r.m));
						(yyval.r.symbol) = paramSymbol;
					    (yyval.r.node) = new ParameterNode(paramSymbol, (yyvsp[-3].r.myLineNo), (yyvsp[-3].r.myColno));
															;}
    break;

  case 400:
#line 1183 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
											Parameter* paramSymbol = new Parameter((yyvsp[0].r.str), (yyvsp[-1].r.myColno),(yyvsp[-1].r.myLineNo),(yyvsp[0].r.m));
										    (yyval.r.symbol) = paramSymbol;
											 (yyval.r.node) = new ParameterNode(paramSymbol, (yyvsp[-3].r.myLineNo), (yyvsp[-3].r.myColno));
											;}
    break;

  case 451:
#line 1283 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
     Myparser->insertFunctionSymbol((yyvsp[-7].r.str),(yyvsp[-7].r.myColno), (yyvsp[-7].r.myLineNo) ,dynamic_cast<Scope*>((yyvsp[-2].r.scope)) ,(yyvsp[-5].r.symbol),modi,modCounter);
	 modCounter=0;
																																					;}
    break;

  case 452:
#line 1287 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {
     Myparser->insertFunctionSymbol((yyvsp[-5].r.str),(yyvsp[-5].r.myColno), (yyvsp[-5].r.myLineNo) ,(yyvsp[-3].r.symbol),modi,modCounter);
	 modCounter=0;
																												 ;}
    break;

  case 460:
#line 1314 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyvsp[0].r.scope)->setownername("struct");   modCounter=0;;}
    break;

  case 461:
#line 1315 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {cout<<"*********struct declaration**********\n";;}
    break;

  case 479:
#line 1348 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyvsp[-2].r.scope)->setownername("array init");;}
    break;

  case 480:
#line 1349 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyvsp[-3].r.scope)->setownername("ayyar init");;}
    break;

  case 482:
#line 1353 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ; cout<<"variable_initializer_list";;}
    break;

  case 483:
#line 1356 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyval.r.node) = (yyvsp[0].r.node) ;;}
    break;

  case 489:
#line 1372 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyvsp[-2].r.scope)->setownername("interface");;}
    break;

  case 510:
#line 1422 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyvsp[-1].r.scope)->setownername("interface empty");;}
    break;

  case 515:
#line 1437 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyvsp[-2].r.scope)->setownername("enum");;}
    break;

  case 516:
#line 1438 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    {(yyvsp[-3].r.scope)->setownername("enum");;}
    break;

  case 548:
#line 1510 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { ; ;}
    break;

  case 549:
#line 1513 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { ; ;}
    break;

  case 550:
#line 1516 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { ; ;}
    break;

  case 551:
#line 1519 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { ; ;}
    break;

  case 552:
#line 1522 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { ; ;}
    break;

  case 553:
#line 1525 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"
    { ; ;}
    break;


      default: break;
    }

/* Line 1126 of yacc.c.  */
#line 4400 "yacc.cpp"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  int yytype = YYTRANSLATE (yychar);
	  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
	  YYSIZE_T yysize = yysize0;
	  YYSIZE_T yysize1;
	  int yysize_overflow = 0;
	  char *yymsg = 0;
#	  define YYERROR_VERBOSE_ARGS_MAXIMUM 5
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;

#if 0
	  /* This is so xgettext sees the translatable formats that are
	     constructed on the fly.  */
	  YY_("syntax error, unexpected %s");
	  YY_("syntax error, unexpected %s, expecting %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
#endif
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytname[yytype];
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytname[yyx];
		yysize1 = yysize + yytnamerr (0, yytname[yyx]);
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + yystrlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow && yysize <= YYSTACK_ALLOC_MAXIMUM)
	    yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg)
	    {
	      /* Avoid sprintf, as that infringes on the user's name space.
		 Don't have undefined behavior even if the translation
		 produced a string with the wrong number of "%s"s.  */
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    {
	      yyerror (YY_("syntax error"));
	      goto yyexhaustedlab;
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (YY_("syntax error"));
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
        }
      else
	{
	  yydestruct ("Error: discarding", yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (0)
     goto yyerrorlab;

yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping", yystos[yystate], yyvsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token. */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK;
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 1528 "C:\\Users\\android-8ac77b8677ce\\Downloads\\compiler_28-4_44444441\\compiler_5-4_44444441\\newtest\\yacc.y"


void yyerror(char *s) 
{
extern int colNo, lineNo;
errorRec.errQ->enqueue(lineNo,colNo,s,"\ninvlid TOKEN :can't reduce grammar");

}

int yylex()
{
	return lexer->yylex();
}

 void print_ast(Node *root, std::ostream &os,string name) 
 {
  os <<  name <<"\n";
  root->print(os);
  os << "}\n";
}
void main(void)
{
  
   yydebug =1;
    freopen("code.cs","r",stdin);
	freopen("out.txt","w",stdout);

	Parser* p = new Parser();
    p->parse();

	Myparser->printMyMap();
	
	Myparser->checkclassdec();
	
	if (!errorRec.errQ->isEmpty()) {
		errorRec.printErrQueue();	
	}


	ofstream ast("ast.txt");
	print_ast(tree, ast,"AST TREE");
	ast.close();
}

