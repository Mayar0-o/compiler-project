%output ="yacc.cpp"
%{
#define YYERROR_VERBOSE
#define YYDEBUG 1
	#include <iostream>
	#include "myvalue.h"
	using namespace std;
	#include <FlexLexer.h>
	#include <string.h>
	#include "ErrorRecovery.h"
	#include "AST/allnode.hpp"
	int yylex(void);
	int yyparse();
	 char msgt[128];
	void yyerror(char *);
	MyParser* Myparser = new MyParser();
	FlexLexer* lexer = new yyFlexLexer();
	ErrorRecovery errorRec;
	int* modi = new int[9];
	int modCounter = 0;
	int scopecount=1;
	int return_type;
	class Parser
	{
		public:
		int	parse()
		{
			return yyparse();
		}
	};

	ListNode *tree = new ListNode("StartNode");
%}

%token FINAL
%token RANK_SPECIFIER 

%token IDENTIFIER 
%token INTEGER_LITERAL REAL_LITERAL CHARACTER_LITERAL STRING_LITERAL


%token  ABSTRACT AS BASE BOOL BREAK
%token  BYTE CASE CATCH CHAR CHECKED
%token  CLASSX CONST CONTINUE DECIMAL DEFAULTX
%token  DELEGATE DO DOUBLE ELSE ENUM
%token  EVENT EXPLICIT EXTERN FALSE FINALLY
%token  FIXED FLOAT FOR FOREACH GOTO
%token  IF IMPLICIT IN INT INTERFACEX
%token  INTERNAL IS LOCK LONG NAMESPACE
%token  NEW NULL_LITERAL OBJECTX OPERATOR OUT
%token  OVERRIDE PARAMS PRIVATE PROTECTED PUBLIC
%token  READONLY REF RETURN SBYTE SEALED
%token  SHORT SIZEOF STACKALLOC STATIC STRING
%token  STRUCT SWITCH THIS THROW TRUE
%token  TRY TYPEOF UINT ULONG UNCHECKED
%token  UNSAFE USHORT USING VIRTUAL VOID
%token  VOLATILE WHILE 


%token ASSEMBLY FIELD METHOD MODULE PARAM PROPERTY TYPEX

%token GET SET 

%token ADD REMOVE


%token COMMA  ","
%token LEFT_BRACKET  "["
%token RIGHT_BRACKET 


%token PLUSEQ MINUSEQ STAREQ DIVEQ MODEQ
%token XOREQ  ANDEQ   OREQ LTLT GTGT GTGTEQ LTLTEQ EQEQ NOTEQ
%token LEQ GEQ ANDAND OROR PLUSPLUS MINUSMINUS ARROW
%nonassoc IFX
%nonassoc ELSE


%start compilation_unit 

%union{
	struct R{
		
		int    myLineNo;
		int    myColno;
		int m; //num of return type
	    double value;
	    char*  str; //identefier 
		int    i; //integeral leteral
		float  f;
		char   c; //CHARACTER_LITERAL
		class Symbol * symbol; //polymorphism for all sympol type
		class Scope * scope; //just for {
		class Node* node;
		}r;

	}


%%
open_bl:'{' {$<r.scope>$ = Myparser->createNewScope(scopecount);scopecount++;}
|'{' open_bl 
;
close_bl: '}' {Myparser->finishscop();}
;
open_ar:'('
|'(' open_ar
;
literal: boolean_literal {$<r.node>$ = $<r.node>1 ; }
  | INTEGER_LITERAL {  //class Node* tnode = new Node() ; $<r.node>$ = tnode ;  
 $<r.i>$ = $<r.i>1;
$<r.node>$ = new LeafNode($<r.i>1, $<r.myLineNo>1, $<r.myColno>1); 
   cout<<"****** INTEGER_LITERALliteral*******"; }
  | REAL_LITERAL{					$<r.f>$ = $<r.f>1;
   $<r.node>$ = new LeafNode($<r.f>1, $<r.myLineNo>1, $<r.myColno>1); }
  | CHARACTER_LITERAL 
  | STRING_LITERAL {					$<r.str>$ = $<r.str>1;
  $<r.node>$ = new LeafNode(std::string($<r.str>1), $<r.myLineNo>1, $<r.myColno>1);}
  | NULL_LITERAL
  ;
boolean_literal
  : TRUE 	{$<r.node>$ = new LeafNode(bool(true), $<r.myLineNo>1, $<r.myColno>1);}
  | FALSE{$<r.node>$ = new LeafNode(bool(false), $<r.myLineNo>1, $<r.myColno>1);}
  ;



namespace_name
  : qualified_identifier
  ;
type_name
  : qualified_identifier {$<r.str>$=$<r.str>1;}
  ;

type 
  : non_array_type { $<r.m>$ = $<r.m>1; }
  | array_type
  ;
non_array_type
  : simple_type { $<r.m>$ = $<r.m>1; }
  | type_name { $<r.m>$ = TYPENAME; }
  ;
simple_type
  : primitive_type { $<r.m>$ = $<r.m>1; }
  | class_type
  | pointer_type { $<r.m>$ = $<r.m>1; }
  ;
primitive_type
  : numeric_type  { $<r.m>$ = $<r.m>1; }
  | BOOL {
		 $<r.m>$ = BOOLT; 
		 return_type = BOOLT;
		 }
  ;
numeric_type
  : integral_type { $<r.m>$ = $<r.m>1; }
  | floating_point_type {$<r.m>$ = $<r.m>1;} 
  | DECIMAL { $<r.m>$ = DECIMALT; }
  ;
integral_type
  : SBYTE {
		 $<r.m>$ = SBYTET; 
		 return_type = SBYTET;
		 }
  | BYTE {
		 $<r.m>$ = BYTET; 
		 return_type = BYTET;
		 }
  | SHORT {
		 $<r.m>$ = SHORTT; 
		 return_type = SHORTT;
		 }
  | USHORT {
		 $<r.m>$ = USHORTT; 
		 return_type = USHORTT;
		 }
  | INT {
		 $<r.m>$ = INTT; 
		 return_type = INTT;
		 }
  | UINT {
		 $<r.m>$ = UINTT; 
		 return_type = UINTT;
		 }
  | LONG {
		 $<r.m>$ = LONGT; 
		 return_type = LONGT;
		 }
  | ULONG {
		 $<r.m>$ = ULONGT; 
		 return_type = ULONGT;
		 }
  | CHAR{
		 $<r.m>$ = CHART; 
		 return_type = CHART;
		 }
  ;
floating_point_type
  : FLOAT {
		 $<r.m>$ = FLOATT; 
		 return_type = FLOATT;
		 }
  | DOUBLE{
		 $<r.m>$ = DOUBLET; 
		 return_type = DOUBLET;
		 }
  ;
class_type
  : OBJECTX | STRING
  ;
pointer_type
  : type '*'{
		 $<r.m>$ = POINTERT; 
		 return_type = POINTERT;
		 }
  | VOID '*'{
		 $<r.m>$ = POINTERT; 
		 return_type = POINTERT;
		 }
  ;
array_type
  : array_type rank_specifier
  | simple_type rank_specifier
  | qualified_identifier rank_specifier 

  ;
rank_specifiers_opt
  : 
  | rank_specifier rank_specifiers_opt
  ;
rank_specifier
  : RANK_SPECIFIER
  ;

variable_reference
  : expression
  ;

argument_list
  : argument {ListNode* list = new ListNode("argument_list");
			   list->add_node($<r.node>1);
                $<r.node>$ = list;
				}
  | argument_list COMMA argument {  dynamic_cast<ListNode*>($<r.node>1)->add_node($<r.node>3);}
	
  ;
argument
  : expression{$<r.node>$ = $<r.node>1 ;}
  | REF variable_reference
  | OUT variable_reference
  ;
primary_expression
  : parenthesized_expression 
  | primary_expression_no_parenthesis  {$<r.node>$ = $<r.node>1 ;}
  ;
primary_expression_no_parenthesis
  : literal {$<r.node>$ = $<r.node>1 ;  cout<<"******literal*******";}
  | array_creation_expression  {$<r.node>$ = $<r.node>1 ;}
  | member_access 
  | invocation_expression {$<r.node>$ = $<r.node>1 ;cout<<"******invocation_expression*******";}
  | element_access 
  | this_access 
  | base_access {$<r.node>$ = $<r.node>1 ;}
  | new_expression {$<r.node>$ = $<r.node>1 ;}
  | typeof_expression {$<r.node>$ = $<r.node>1 ;}
  | sizeof_expression {$<r.node>$ = $<r.node>1 ;}
  | checked_expression {$<r.node>$ = $<r.node>1 ;}
  | unchecked_expression 
  ;
parenthesized_expression
  : '(' expression ')'
  ;
member_access
  : primary_expression '.' IDENTIFIER
  | primitive_type '.' IDENTIFIER
  | class_type '.' IDENTIFIER
  ;
invocation_expression
  : primary_expression_no_parenthesis '(' argument_list_opt ')'
  | qualified_identifier '(' argument_list_opt ')'
  ;
argument_list_opt
  :{$<r.node>$ = nullptr;}
  | argument_list{$<r.node>$ = $<r.node>1 ;}
  ;
element_access
  : primary_expression LEFT_BRACKET expression_list RIGHT_BRACKET
  | qualified_identifier LEFT_BRACKET expression_list RIGHT_BRACKET
  ;
expression_list_opt
  : 
  | expression_list
  ;
expression_list
  : expression 
  | expression_list COMMA expression
  ;
this_access
  : THIS
  ;
base_access
  : BASE '.' IDENTIFIER
  | BASE LEFT_BRACKET expression_list RIGHT_BRACKET
  ;
post_increment_expression
  : postfix_expression PLUSPLUS {$<r.node>$ = $<r.node>1 ;}
  ;
post_decrement_expression
  : postfix_expression MINUSMINUS
  ;
new_expression
  : object_creation_expression{$<r.node>$ = $<r.node>1 ;}
  ;
object_creation_expression
  : NEW type '(' argument_list_opt ')'{	 $<r.node>$ = new NewNode($<r.node>4, $<r.myLineNo>1, $<r.myColno>1);}
  ;
array_creation_expression
  : NEW non_array_type LEFT_BRACKET expression_list RIGHT_BRACKET rank_specifiers_opt array_initializer_opt
  | NEW array_type     array_initializer
  ;
array_initializer_opt
  : /* Nothing */
  | array_initializer
  ;
typeof_expression
  : TYPEOF open_ar type ')'
  | TYPEOF '(' VOID ')'
  ;
checked_expression
  : CHECKED '(' expression ')'
  ;
unchecked_expression
  : UNCHECKED '(' expression ')'
  ;
pointer_member_access
  : postfix_expression ARROW IDENTIFIER
  ;
addressof_expression
  : '&' unary_expression
  ;
sizeof_expression
  : SIZEOF open_ar type ')'
  ;
postfix_expression
  : primary_expression {$<r.node>$ = $<r.node>1 ;}
  | qualified_identifier{$<r.node>$ = $<r.node>1 ;}
  | post_increment_expression {$<r.node>$ = $<r.node>1 ;}
  | post_decrement_expression
  | pointer_member_access
  ;
unary_expression_not_plusminus
  : postfix_expression {$<r.node>$ = $<r.node>1 ;}
  | '!' unary_expression
  | '~' unary_expression
  | cast_expression
  ;
pre_increment_expression
  : PLUSPLUS unary_expression {
    class LeafNode* tnode = new LeafNode(1,0,0) ;
  $<r.node>$ = new AssignmentNode($<r.node>2, new BinaryOperationNode("+", tnode, $<r.node>2, $<r.myLineNo>1, $<r.myColno>1), $<r.myLineNo>1, $<r.myColno>1); 
    cout<<"******  pre_increment_expression*******\n";}
  ;
pre_decrement_expression
  : MINUSMINUS unary_expression{$<r.node>$ = $<r.node>2 ;}
  ;
unary_expression
  : unary_expression_not_plusminus {$<r.node>$ = $<r.node>1 ;  cout<<"******  unary_expression_not_plusminus*******\n";}
  | '+' unary_expression {$<r.node>$ = $<r.node>2 ; cout<<"****** '+' unary_expression*******\n";}
  | '-' unary_expression
  | '*' unary_expression
  | pre_increment_expression{$<r.node>$ = $<r.node>1 ; cout<<"******  pre_increment_expression*******\n";}
  | pre_decrement_expression{$<r.node>$ = $<r.node>1 ;}
  | addressof_expression
  ;

cast_expression
  : '(' expression ')' unary_expression_not_plusminus
  | '(' multiplicative_expression '*' ')' unary_expression 
  | '(' qualified_identifier rank_specifier type_quals_opt ')' unary_expression  
  | '(' primitive_type type_quals_opt ')' unary_expression
  | '(' class_type type_quals_opt ')' unary_expression
  | '(' VOID type_quals_opt ')' unary_expression
  ;
type_quals_opt
  : 
  | type_quals
  ;
type_quals
  : type_qual
  | type_quals type_qual
  ;
type_qual 
  : rank_specifier 
  | '*'
  ;
multiplicative_expression
  : unary_expression {$<r.node>$ = $<r.node>1 ; cout<<"******  multiplicative_expression*******\n";}
  | multiplicative_expression '*' unary_expression   {$<r.node>$ =	new BinaryOperationNode("*", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  | multiplicative_expression '/' unary_expression{$<r.node>$ =	new BinaryOperationNode("/", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  | multiplicative_expression '%' unary_expression{$<r.node>$ =	new BinaryOperationNode("%", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  ;
additive_expression
  : multiplicative_expression {$<r.node>$ = $<r.node>1 ;}
  | additive_expression '+' multiplicative_expression   {
          //    class Node* tnode = new Node() ;
		//	class Node* tnode2 = new Node();

            //  ListNode* list = new ListNode("BinaryOperation");
									
		  $<r.node>$ =	new BinaryOperationNode("+", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);
               // $<r.node>$ = list;
				cout<<"****** adddddddddddddddd additive_expression*******\n";
		
				}
  | additive_expression '-' multiplicative_expression {	 $<r.node>$ =	new BinaryOperationNode("-", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  ;
shift_expression
  : additive_expression {  $<r.node>$ = $<r.node>1; }
  | shift_expression LTLT additive_expression
  | shift_expression GTGT additive_expression
  ;
relational_expression
  : shift_expression 	{  $<r.node>$ = $<r.node>1; cout<<"****** shift_expression *******\n";}
  | relational_expression '<' shift_expression{	$<r.node>$ =	new BinaryOperationNode("<", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  | relational_expression '>' shift_expression{	$<r.node>$ =	new BinaryOperationNode(">", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  | relational_expression LEQ shift_expression
  | relational_expression GEQ shift_expression
  | relational_expression IS type
  | relational_expression AS type
  ;
equality_expression
  : relational_expression { $<r.node>$ = $<r.node>1; cout<<"****** relational_expression *******\n";}
  | equality_expression EQEQ relational_expression {$<r.node>$ =new BinaryOperationNode("==", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  | equality_expression NOTEQ relational_expression {$<r.node>$ =new BinaryOperationNode("!=", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  ;
and_expression
  : equality_expression { $<r.node>$ = $<r.node>1; cout<<"****** equality_expression *******\n";}
  | and_expression '&' equality_expression
  ;
exclusive_or_expression
  : and_expression { $<r.node>$ = $<r.node>1; cout<<"****** and_expression *******\n";}
  | exclusive_or_expression '^' and_expression
  ;
inclusive_or_expression
  : exclusive_or_expression {  $<r.node>$ = $<r.node>1; cout<<"****** exclusive_or_expression *******\n";}
  | inclusive_or_expression '|' exclusive_or_expression
  ;
conditional_and_expression
  : inclusive_or_expression { $<r.node>$ = $<r.node>1; cout<<"****** inclusive_or_expression *******\n";}
  | conditional_and_expression ANDAND inclusive_or_expression{$<r.node>$ =	new BinaryOperationNode("&&", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  ;
conditional_or_expression
  : conditional_and_expression { $<r.node>$ = $<r.node>1; cout<<"****** conditional_and_expression *******\n";}
  | conditional_or_expression OROR conditional_and_expression{$<r.node>$ =	new BinaryOperationNode("||", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);}
  ;
conditional_expression
  : conditional_or_expression {  $<r.node>$ = $<r.node>1; cout<<"****** conditional_or_expression *******\n";}
  | conditional_or_expression '?' expression ':' expression
  ;
assignment
: unary_expression assignment_operator expression {cout<<"********* assignment (expression) ************\n ";
  class Node* tnode = new Node() ;
class Node* tnode2 = new Node();
  $<r.node>$ =new AssignmentNode( $<r.node>1 , $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);	
   } // creat assignment nodee
  ;
 assignment
: unary_expression PLUSEQ expression {cout<<"********* assignment (expression) ************\n ";
   $<r.node>$ = new AssignmentNode($<r.node>1, new BinaryOperationNode("+", $<r.node>1, $<r.node>3, $<r.myLineNo>1, $<r.myColno>1), $<r.myLineNo>1, $<r.myColno>1); 
	
   } 
  ;
   assignment
: unary_expression '=' expression {cout<<"********* assignment 2 ************\n ";
  $<r.node>$ =new AssignmentNode( $<r.node>1 , $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);

   }
  ;     

assignment_operator
  : MINUSEQ | STAREQ | DIVEQ | MODEQ 
  | XOREQ | ANDEQ | OREQ | GTGTEQ | LTLTEQ 
  ;
expression
  : conditional_expression { $<r.node>$ = $<r.node>1; cout<<"********* conditional_expression************\n "; }
  | assignment  {$<r.node>$ = $<r.node>1; cout<<"********* expression************\n "; }
  ;
constant_expression
  : expression
  ;
boolean_expression
  : expression {$<r.node>$ = $<r.node>1;}
  ;
                 
statement 
  : labeled_statement     {$<r.node>$ = $<r.node>1; cout<<"********* labeled_statement************\n "; }
  | declaration_statement {$<r.node>$ = $<r.node>1; cout<<"*********  declaration_statement************\n "; } //todo:const
  | embedded_statement    {$<r.node>$ = $<r.node>1;cout<<"********* embedded_statement************\n ";  }
  ;
embedded_statement
  : block { $<r.node>$ = $<r.node>1; cout<<"********* block************\n ";  }
  | empty_statement         { $<r.node>$ = $<r.node>1; cout<<"********* empty_statement************\n ";  }
  | expression_statement  {$<r.node>$ = $<r.node>1; cout<<"********* expression_statement************\n ";  }
  | selection_statement     {$<r.node>$ = $<r.node>1; }  // if & switch
  | iteration_statement     {$<r.node>$ = $<r.node>1 ;}  // while & do & for & foreach 
  | jump_statement        {$<r.node>$ = $<r.node>1; } // break & continue & return  & goto & throw
  | try_statement          {$<r.node>$ = $<r.node>1; } // try & cacth & finally 
  | checked_statement      // CHECKED block
  | unchecked_statement
  | lock_statement
  | using_statement
  | unsafe_statement
  | fixed_statement
  ;
block
  : open_bl statement_list_opt close_bl { $<r.node>$ = $<r.node>2;  $<r.scope>1->setownername("block");}
  ;
statement_list_opt
  : {$<r.node>$ = nullptr; }
  | statement_list {$<r.node>$ = $<r.node>1; cout<<"********* statement_list_opt************\n ";  }
  ;

statement_list
  : statement {	
               ListNode* list = new ListNode("statement_list_Node");
			   list->add_node($<r.node>1);
                $<r.node>$ = list;
			
					} 
  | statement_list statement {		
									dynamic_cast<ListNode*>($<r.node>1)->add_node($<r.node>2);
									
							}
  ;
empty_statement
  : ';'{$<r.node>$ = nullptr; cout<<"********* empty_statement************\n ";  }
  ;
labeled_statement
  : IDENTIFIER ':' statement
  ;
declaration_statement
  : local_variable_declaration ';'  {$<r.node>$ = $<r.node>1 ;}
  | local_constant_declaration ';'   {$<r.node>$ = $<r.node>1 ;}
  ;
local_variable_declaration
  : type variable_declarators { Myparser->removev ($<r.symbol>2->name ,Myparser->getCurrentScope() );
							    $<r.symbol>$ = Myparser->insertSymbol(new Variable($<r.symbol>2->name,VARIABLE, $<r.myColno>1, $<r.myLineNo>1,$<r.m>1)); 
							  $<r.node>$ = $<r.node>2;
							     //    $<r.node>$ = new VariableDec($<r.symbol>$,$<r.node>2, $<r.myLineNo>1, $<r.myColno>1);
							 //not sure
							  }
;
variable_declarators
  : variable_declarator {	ListNode* list = new ListNode("VariablelistNode");
						  list->add_node($<r.node>1);
						 $<r.node>$ = list;
					   } 

			
  | variable_declarators COMMA variable_declarator{

			Symbol* trav = $<r.symbol>1;
			Symbol* travtemp = trav;
			while(trav != nullptr){
			    travtemp = trav;
				trav = trav->node;
		    }
			travtemp->node = $<r.symbol>3;
			$<r.symbol>$ = $<r.symbol>1;
			///////////////////////////////
		dynamic_cast<ListNode*>($<r.node>1)->add_node($<r.node>3);

				               	}
  ;
variable_declarator
  : IDENTIFIER   {$<r.symbol>$ = Myparser->insertSymbol(new Variable($<r.str>1,VARIABLE,  $<r.myColno>1, $<r.myLineNo>1,return_type));
                       $<r.node>$ = new VariableNode($<r.symbol>$, $<r.myLineNo>1, $<r.myColno>1);
				//  $<r.node>$ =	new IDNode( $<r.myLineNo>1, $<r.myColno>1,$<r.str>1);

				 }


  | IDENTIFIER '=' variable_initializer {$<r.symbol>$ = Myparser->insertSymbol(new Variable($<r.str>1,VARIABLE, $<r.myColno>1, $<r.myLineNo>1,return_type));
			                             $<r.node>$ =new AssignmentNode(new VariableNode($<r.symbol>$, $<r.myLineNo>1, $<r.myColno>1), $<r.node>3, $<r.myLineNo>1, $<r.myColno>1);	
										}
  ;
variable_initializer
  : expression  {$<r.node>$ = $<r.node>1 ;}
  | array_initializer {$<r.node>$=new Node();}
  | stackalloc_initializer {$<r.node>$=new Node();}
  ;
stackalloc_initializer
: STACKALLOC type  LEFT_BRACKET expression RIGHT_BRACKET 
  ; 
local_constant_declaration
  : CONST type constant_declarators {  Myparser->removev ($<r.symbol>3->name ,Myparser->getCurrentScope() );
									 $<r.symbol>$ = Myparser->insertSymbol(new Variable($<r.symbol>3->name,VARIABLE, $<r.myColno>1, $<r.myLineNo>1,$<r.m>2)); 
									}
  //Myparser->setvartypeforreturn($<r.symbol>2,$<r.m>2);}
  ;
constant_declarators
  : constant_declarator
  | constant_declarators COMMA constant_declarator {

			Symbol* trav = $<r.symbol>1;
			Symbol* travtemp = trav;
			while(trav != nullptr){
			    travtemp = trav;
				trav = trav->node;
		    }
			travtemp->node = $<r.symbol>3;
			$<r.symbol>$ = $<r.symbol>1;
											}
  ;
constant_declarator
  : IDENTIFIER '=' constant_expression  {  $<r.symbol>$ = Myparser->insertSymbol(new Variable($<r.str>1,VARIABLE, $<r.myColno>1, $<r.myLineNo>1,return_type)); }
  | IDENTIFIER  {errorRec.errQ->enqueue($<r.myLineNo>1,$<r.myColno>1,"A const field requaires a valu to be provided",""); $<r.symbol>$ = Myparser->insertSymbol(new Variable($<r.str>1,VARIABLE, $<r.myColno>1, $<r.myLineNo>1,return_type));
																													
  }


  ;
expression_statement
  : statement_expression ';' { $<r.node>$ = $<r.node>1;}
  ;
statement_expression
  : invocation_expression {cout<<"******invocation_expression1*******\n";}
  | object_creation_expression { $<r.node>$ = $<r.node>1;cout<<"******object_creation_expression*******\n";}
  | assignment { $<r.node>$ = $<r.node>1; cout<<"******assignment1*******\n";}
  | post_increment_expression //postfix { $<r.node>$ = $<r.node>1;}
  | post_decrement_expression { $<r.node>$ = $<r.node>1;}
  | pre_increment_expression  //unarray { $<r.node>$ = $<r.node>1;}
  | pre_decrement_expression { $<r.node>$ = $<r.node>1;}
  ;
selection_statement
  : if_statement {$<r.node>$ = $<r.node>1;}
  | switch_statement {$<r.node>$ = $<r.node>1;}
  ;
if_statement
  : IF '(' boolean_expression ')' embedded_statement {		
			Node *else_node = nullptr;
			class Node* tnode = new Node() ;
			class Node* tnode2 = new Node() ;	
			 $<r.node>$ = new IfNode($<r.node>3, $<r.node>5, else_node, $<r.myLineNo>1, $<r.myColno>1); 
			 cout<<"*********IF 1 ************\n ";
}   %prec IFX	
  | IF '(' boolean_expression RIGHT_BRACKET embedded_statement {
  			 cout<<"*********IF 2 ************\n ";
  errorRec.errQ->enqueue($<r.myLineNo>3,$<r.myColno>3,"Expected syntex ERROR in if statement","");}
  | IF '(' boolean_expression ')' embedded_statement ELSE embedded_statement {
								Node *else_node = new ElseNode($<r.node>7, $<r.myLineNo>6, $<r.myColno>6);
								//	class Node* tnode = new Node() ;
								//	class Node* tnode2 = new Node();
								$<r.node>$ = new IfNode($<r.node>3,$<r.node>5, else_node, $<r.myLineNo>1, $<r.myColno>1);
								cout<<"******if else statementx*******";}
  | IF '(' assignment ')' embedded_statement {
  errorRec.errQ->enqueue($<r.myLineNo>2,$<r.myColno>2,"Expected syntex ERROR in if condition (not bool expression)","");}
  ;
switch_statement
  : SWITCH open_ar expression ')' open_bl switch_sections_opt close_bl{cout<<"*********switch statement************\n ";
																					$<r.scope>5->setownername("SWITCH");
																				//	class Node* tnode = new Node() ;
																				//	class Node* tnode2 = new Node();
																					$<r.node>$ = new SwitchNode($<r.node>3,$<r.node>5, $<r.myLineNo>1,$<r.myColno>1);
																					}
  ;
switch_sections_opt
  : 
  | switch_sections
  ;
switch_sections
  : switch_section
  | switch_sections switch_section
  ;
switch_section
  : switch_labels statement_list
  ;
switch_labels
  : switch_label
  | switch_labels switch_label
  ;
switch_label
  : CASE constant_expression ':'
  | DEFAULTX ':'
  ;
iteration_statement
  : while_statement {$<r.node>$ = $<r.node>1;} // complete
  | do_statement
  | for_statement   {$<r.node>$ = $<r.node>1;} // for 
  | foreach_statement
  ;
unsafe_statement
  : UNSAFE block
  ;
while_statement
  : WHILE '(' boolean_expression ')' embedded_statement {
														cout<<"*********while_statement*********\n";
												//	class Node* tnode = new Node() ;
												//	class Node* tnode2 = new Node() ;
		                                           $<r.node>$ = new WhileNode($<r.node>3, $<r.node>5, $<r.myLineNo>1, $<r.myColno>1); 
														}
  | WHILE '(' assignment ')' embedded_statement {errorRec.errQ->enqueue($<r.myLineNo>2,$<r.myColno>2,"Expected syntex ERROR in while condition (not bool expression)","");}
  ;
do_statement
  : DO embedded_statement WHILE '(' boolean_expression ')' ';' {cout<<"*********do while atatement*********\n";}  
  |DO embedded_statement WHILE '(' assignment ')' ';'{errorRec.errQ->enqueue($<r.myLineNo>4,$<r.myColno>4,"Expected syntex ERROR in do_while condition (not bool expression)","");}
  ;
for_statement
  : FOR open_ar for_initializer_opt ';' for_condition_opt ';' for_iterator_opt ')' embedded_statement{ 
																		cout<<"*********for_statement**********\n";
																		class Node* tnode = new Node() ;
												                    	class Node* tnode2 = new Node() ;
																		class Node* tnode3 = new Node();
								$<r.node>$ = new ForNode($<r.node>3, $<r.node>5, $<r.node>7, $<r.node>9, $<r.myLineNo>1, $<r.myColno>1);	
								}																
  ;
for_initializer_opt
  : 
  | for_initializer {$<r.node>$ = $<r.node>1;}
  ;
for_condition_opt
  :  {$<r.node>$ = nullptr;}
  | for_condition {$<r.node>$ = $<r.node>1;}
  ;
for_iterator_opt
  :
  | for_iterator {$<r.node>$ = $<r.node>1; }
  ;
for_initializer
  : local_variable_declaration{$<r.node>$ = $<r.node>1; }
  | statement_expression_list
  ;
for_condition
  : boolean_expression{$<r.node>$ = $<r.node>1; }
  ;
for_iterator
  : statement_expression_list {$<r.node>$ = $<r.node>1; }
  ;
statement_expression_list
  : statement_expression {$<r.node>$ = $<r.node>1; }
  | statement_expression_list COMMA statement_expression
  ;
foreach_statement
  : FOREACH open_ar type IDENTIFIER IN expression ')' embedded_statement
  ;
jump_statement
  : break_statement{$<r.node>$ = $<r.node>1; }
  | continue_statement{$<r.node>$ = $<r.node>1; }
  | goto_statement
  | return_statement{$<r.node>$ = $<r.node>1; }
  | throw_statement
  ;
break_statement
  : BREAK ';' {$<r.node>$ = new BreakNode($<r.myLineNo>1, $<r.myColno>1);} 
  ;
continue_statement
  : CONTINUE ';'{$<r.node>$ = new ContinueNode($<r.myLineNo>1, $<r.myColno>1);}
  ;
goto_statement
  : GOTO IDENTIFIER ';'
  | GOTO CASE constant_expression ';'
  | GOTO DEFAULTX ';'
  ;
return_statement
  : RETURN expression_opt ';' {$<r.node>$ = new ReturnNode($<r.node>2, $<r.myLineNo>1, $<r.myColno>1);}
  ;
expression_opt
  : 
  | expression
  ;
throw_statement
  : THROW expression_opt ';'
  ;
try_statement
  : TRY block catch_clauses {    
 class catchNode *cat =  new catchNode($<r.node>3, $<r.myLineNo>1, $<r.myColno>1);
  $<r.node>$ = new tryNode($<r.node>2, cat, $<r.myLineNo>1, $<r.myColno>1);
  cout<<"*********try_catch********\n";}
  | catch_clauses           {errorRec.errQ->enqueue($<r.myLineNo>1,$<r.myColno>1,"Unexpected catch without try","");}
  | TRY block finally_clause
  | TRY block catch_clauses finally_clause

  ;
catch_clauses
  : catch_clause { $<r.node>$ = $<r.node>1;}
  | catch_clauses catch_clause
  ;
catch_clause
  : CATCH open_ar class_type identifier_opt ')' block
  | CATCH open_ar type_name identifier_opt ')' block
  | CATCH block {  $<r.node>$ = $<r.node>2;}
  ;
identifier_opt
  : 
  | IDENTIFIER
  ;
finally_clause
  : FINALLY block { $<r.node>$ = $<r.node>2;}
  ;
checked_statement
  : CHECKED block{ $<r.node>$ = $<r.node>2;}
  ;
unchecked_statement
  : UNCHECKED block{ $<r.node>$ = $<r.node>2;}
  ;
lock_statement
  : LOCK '(' expression ')' embedded_statement
  ;
using_statement
  : USING '(' resource_acquisition ')' embedded_statement
  ;
resource_acquisition
  : local_variable_declaration
  | expression
  ;
fixed_statement
/*! : FIXED '(' pointer_type fixed_pointer_declarators ')' embedded_statement */
  : FIXED '('  type fixed_pointer_declarators ')' embedded_statement
  ;
fixed_pointer_declarators
  : fixed_pointer_declarator
  | fixed_pointer_declarators COMMA fixed_pointer_declarator
  ;
fixed_pointer_declarator
  : IDENTIFIER '=' expression
  ;
compilation_unit  
  : using_directives_opt attributes_opt {cout<<"*********starrrrrrrrrtttttttttt\n";}
  | using_directives_opt namespace_member_declarations{cout<<"*********starrrrrrrrrtttttttttt\n";
														tree->add_nodes(dynamic_cast<ListNode*>($<r.node>2)->nodes);
                                                        }
  ;
using_directives_opt
  : 
  | using_directives
  ;
attributes_opt
  : 
  | attributes
  ;
namespace_member_declarations_opt
  :  
  | namespace_member_declarations   { $<r.node>$ = $<r.node>1;}
  ;
namespace_declaration
  : attributes_opt NAMESPACE qualified_identifier namespace_body comma_opt {$<r.node>$ = $<r.node>4;}
  ;
comma_opt
  : 
  | ';'
  ;

qualified_identifier      
  : IDENTIFIER  {$<r.str>$ = $<r.str>1 ; 
//class Node* tnode = new Node() ;

  $<r.node>$ =	new IDNode( $<r.myLineNo>1, $<r.myColno>1,$<r.str>1);
   }
  | qualifier IDENTIFIER{ 

           char* trav = $<r.str>1;
		   string strn(trav);
		   string strn2($<r.str>2);
		   strn.append(strn2);
		    const char *cstr = strn.c_str();
			trav = strdup(cstr);
			$<r.str>$ = trav;

	}
  ;
qualifier
  : IDENTIFIER '.' {$<r.str>$ = $<r.str>1 ;}
  | qualifier IDENTIFIER '.' {
  
           char* trav = $<r.str>1;
		   string strn(trav);
		   string strn2($<r.str>2);
		    strn.append(".");
		   strn.append(strn2);
		    const char *cstr = strn.c_str();
			trav = strdup(cstr);
			$<r.str>$ = trav;


	}
  ;
namespace_body
  : open_bl using_directives_opt namespace_member_declarations_opt close_bl {$<r.scope>1->setownername("namespace");
                                                                              { $<r.node>$ = $<r.node>3;}
																			 }
  ;
using_directives
  : using_directive
  | using_directives using_directive
  ;
using_directive
  : using_alias_directive
  | using_namespace_directive
  ;
using_alias_directive
  : USING IDENTIFIER '=' qualified_identifier ';'
  ;
using_namespace_directive
  : USING namespace_name ';'
  ;
namespace_member_declarations
  : namespace_member_declaration {                 // $<r.node>$ =$<r.node>1;
  	                                         	//	dynamic_cast<ListNode*>($<r.node>$)->add_node($<r.node>1); 

														ListNode* list = new ListNode("NameSpaceNode");
														list->add_node($<r.node>1);
														 $<r.node>$ = list;																				
			}   //it should be list ??
  | namespace_member_declarations namespace_member_declaration { 
															 dynamic_cast<ListNode*>($<r.node>1)->add_node($<r.node>2);
															  }
															   
  ;
namespace_member_declaration
  : namespace_declaration  { $<r.node>$ = $<r.node>1;}
  | type_declaration      { $<r.node>$ = $<r.node>1;}
  ;
type_declaration
  : class_declaration     { $<r.node>$ = $<r.node>1; } 
  | struct_declaration    
  | interface_declaration 
  | enum_declaration     
  | delegate_declaration  
  ; 


modifiers_opt
  :
  | modifiers
  ;
modifiers
  : modifier
  | modifiers modifier
  ;
modifier  
  : ABSTRACT  {$<r.m>$ = ABSTRACTT;
				modi[modCounter++] = ABSTRACTT;}

  | EXTERN    {$<r.m>$ = EXTERNT;
				modi[modCounter++] = EXTERNT;}

  | INTERNAL  {$<r.m>$ = INTERNALT;
				modi[modCounter++] = INTERNALT;}

  | NEW       {$<r.m>$ = NEWT;
				modi[modCounter++] = NEWT;}

  | OVERRIDE  {$<r.m>$ = OVERRIDET;
				modi[modCounter++] = OVERRIDET;}

  | PRIVATE   {$<r.m>$ = PRIVATET;
				modi[modCounter++] = PRIVATET;}

  | PROTECTED {$<r.m>$ = PROTECTEDT;
				modi[modCounter++] = PROTECTEDT;}

  | PUBLIC   {$<r.m>$ = PUBLICT;
				modi[modCounter++] = PUBLICT;}

  | READONLY {$<r.m>$ = READONLYT;
				modi[modCounter++] = READONLYT;}

  | SEALED   {$<r.m>$ = SEALEDT;
				modi[modCounter++] = SEALEDT;}

  | STATIC   {$<r.m>$ = STATICT;
				modi[modCounter++] = STATICT;}

  | UNSAFE   {$<r.m>$ = UNSAFET;
				modi[modCounter++] = UNSAFET;}

  | VIRTUAL  {$<r.m>$ = VIRTUALT;
				modi[modCounter++] = VIRTUALT;}

  | VOLATILE {$<r.m>$ = VOLATILET;
				modi[modCounter++] = VOLATILET;}
  | FINAL {$<r.m>$ = FINALT;
				modi[modCounter++] = FINALT;}
  ;

class_declaration 
  : attributes_opt modifiers_opt CLASSX IDENTIFIER class_base_opt open_bl {

  Class* Myclass = new Class($<r.str>4, $<r.myColno>3, $<r.myLineNo>3,dynamic_cast<Scope*>($<r.scope>6),modi,modCounter);
  Myparser->setClass($<r.str>5, Myclass, dynamic_cast<Scope*>($<r.scope>6),Myparser->getCurrentClassSym());
  $<r.symbol>$ = Myparser->insertSymbol(Myclass, dynamic_cast<Scope*>($<r.scope>6)->getParentScope());	
  modCounter=0;	
   Myparser->pushToClassesStack(Myclass);							 
																	     }
   class_member_declarations_opt close_bl comma_opt {
   $<r.symbol>$=Myparser->getCurrentClassSym();Myparser->checksetconstructer(dynamic_cast<Class*>($<r.symbol>$));Myparser->popFromClassesStack();
   // ListNode* list = new ListNode();
   $<r.node>$ = new ClassDefineNode($<r.symbol>$, $<r.node>8, $<r.myLineNo>6, $<r.myColno>6); 
   cout<<"*********class_declerationssss***********\n"; }
 
  | attributes_opt modifiers_opt CLASSX  class_base_opt open_bl  {
   Class* Myclass = new Class(nullptr, $<r.myColno>3, $<r.myLineNo>3,dynamic_cast<Scope*>($<r.scope>5),modi,modCounter);
  Myparser->setClass($<r.str>4, Myclass, dynamic_cast<Scope*>($<r.scope>5),Myparser->getCurrentClassSym());
  $<r.symbol>$ = Myparser->insertSymbol(Myclass, dynamic_cast<Scope*>($<r.scope>5)->getParentScope());	
  modCounter=0;	
   Myparser->pushToClassesStack(Myclass);
																  } 
   class_member_declarations_opt close_bl comma_opt {$<r.symbol>$=Myparser->getCurrentClassSym();Myparser->checksetconstructer(dynamic_cast<Class*>($<r.symbol>$));Myparser->popFromClassesStack();errorRec.errQ->enqueue($<r.myLineNo>3,$<r.myColno>3,"Unexpected class without name","");	}

  | attributes_opt modifiers_opt IDENTIFIER IDENTIFIER class_base_opt open_bl{
    Class* Myclass = new Class($<r.str>4, $<r.myColno>3, $<r.myLineNo>3,dynamic_cast<Scope*>($<r.scope>6),modi,modCounter);
  Myparser->setClass($<r.str>5, Myclass, dynamic_cast<Scope*>($<r.scope>6),Myparser->getCurrentClassSym());
  $<r.symbol>$ = Myparser->insertSymbol(Myclass, dynamic_cast<Scope*>($<r.scope>6)->getParentScope());	
  modCounter=0;	
   Myparser->pushToClassesStack(Myclass);
																			 }
   class_member_declarations_opt close_bl comma_opt {$<r.symbol>$=Myparser->getCurrentClassSym();Myparser->checksetconstructer(dynamic_cast<Class*>($<r.symbol>$));Myparser->popFromClassesStack();errorRec.errQ->enqueue($<r.myLineNo>3,$<r.myColno>3,"Unexpected keyword class wrong ","");modCounter=0;	}
  ;
class_base_opt
  : {$<r.str>$ = nullptr;}
  | class_base {$<r.str>$ = $<r.str>1;}
  ;
class_base
  : ':' class_type 
  | ':' interface_type_list {$<r.str>$ = $<r.str>2;}
  | ':' class_type COMMA interface_type_list
  ;
interface_type_list
  : type_name {$<r.str>$ = $<r.str>1;}
  | interface_type_list COMMA type_name
  ;
class_member_declarations_opt 
  :                         {   $<r.node>$ = nullptr ;}
  | class_member_declarations  {$<r.node>$ =$<r.node>1;} 
;

class_member_declarations 
  : class_member_declaration   {ListNode* list = new ListNode("ClassMemberlistNode");
								 list->add_node($<r.node>1);
								  $<r.node>$ = list
								  }
  | class_member_declarations class_member_declaration { dynamic_cast<ListNode*>($<r.node>1)->add_node($<r.node>2); }
  ;
class_member_declaration  
  : constant_declaration { $<r.node>$  = $<r.node>1 ; }
  | field_declaration	 { $<r.node>$  = $<r.node>1 ; }
  | method_declaration	 { $<r.node>$  = $<r.node>1 ;//cout<< $<r.symbol>$->getName();
   }

  | property_declaration {ListNode* list = new ListNode("property_declaration");
                          list->add_node(new Node() );
	                      $<r.node>$ = list;
						 }
  | event_declaration   {ListNode* list = new ListNode("event_declaration");
                         list->add_node(new Node() );
	                     $<r.node>$ = list;
						}
  | indexer_declaration	{ListNode* list = new ListNode("indexer_declaration");
                         list->add_node(new Node() );
	                     $<r.node>$ = list;
						}
  | operator_declaration{ ListNode* list = new ListNode("operator_declaration");
                         list->add_node(new Node() );
	                     $<r.node>$ = list;
						}
  | constructor_declaration	{ListNode* list = new ListNode("constructor_declaration");
                         list->add_node(new Node() );
	                     $<r.node>$ = list;
						}
  | destructor_declaration	{ListNode* list = new ListNode("destructor_declaration");
                         list->add_node(new Node() );
	                     $<r.node>$ = list;
						}
  | type_declaration { $<r.node>$  = $<r.node>1 ; }
  ;
constant_declaration
  : attributes_opt modifiers_opt CONST type constant_declarators ';' {
  Myparser->removev ($<r.symbol>5->name ,Myparser->getCurrentScope() );
  $<r.symbol>$ = Myparser->insertSymbol(new DataMember($<r.symbol>5->name, $<r.myColno>3, $<r.myLineNo>3,$<r.m>4,modi,modCounter));
  	modCounter=0;
	// $<r.node>$ = new ClassMemNode($<r.symbol>$,$<r.node>4, $<r.myLineNo>3, $<r.myColno>3);

  }
  | attributes_opt modifiers_opt CONST  constant_declarators ';'{errorRec.errQ->enqueue($<r.myLineNo>3,$<r.myColno>3,"constant without type\n.......>not inserted in SymbolTable","");modCounter=0;	}
  ;
field_declaration
  : attributes_opt modifiers_opt type variable_declarators ';' {
  Myparser->removev ($<r.symbol>4->name ,Myparser->getCurrentScope() );
  $<r.symbol>$ = Myparser->insertSymbol(new DataMember($<r.symbol>4->name, $<r.myColno>3, $<r.myLineNo>3,$<r.m>3,modi,modCounter));
  modCounter=0;	
  $<r.node>$ = new ClassMemNode($<r.symbol>$,$<r.node>4, $<r.myLineNo>3, $<r.myColno>3);
  }
  ;

method_declaration
  : attributes_opt modifiers_opt type qualified_identifier open_ar formal_parameter_list_opt ')' open_bl
  {
 $<r.symbol>$ = Myparser->insertFunctionSymbol($<r.str>4,$<r.m>3,$<r.myColno>3, $<r.myLineNo>3 ,dynamic_cast<Scope*>($<r.scope>8) ,$<r.symbol>6,modi,modCounter);
  modCounter=0;	
  }
  statement_list_opt close_bl {
								cout<<"*********method declaration1**********\n";
$<r.node>$ = new FunctionDefineNode(Myparser->lookUpSymbol($<r.scope>8->getParentScope(),$<r.str>4), $<r.node>10, $<r.node>6, $<r.myLineNo>7, $<r.myColno>7);

                              }
  ;

  | attributes_opt modifiers_opt type qualified_identifier open_ar formal_parameter_list_opt ')' ';'
                                                                    {
  Myparser->insertFunctionSymbol($<r.str>4,$<r.m>3,$<r.myColno>3, $<r.myLineNo>3 ,$<r.symbol>6,modi,modCounter);
   modCounter=0;cout<<"*********method declaration2**********\n";	}
  ;

  | attributes_opt modifiers_opt VOID qualified_identifier open_ar formal_parameter_list_opt ')' ';' 
																	{
  Myparser->insertFunctionSymbol($<r.str>4,$<r.m>3,$<r.myColno>3, $<r.myLineNo>3 ,$<r.symbol>6,modi,modCounter);
   modCounter=0;cout<<"*********method declaration3**********\n";	}
  ;

  | attributes_opt modifiers_opt VOID qualified_identifier open_ar formal_parameter_list_opt ')' open_bl
  {
   Myparser->insertFunctionSymbol($<r.str>4,$<r.m>3,$<r.myColno>3, $<r.myLineNo>3 ,dynamic_cast<Scope*>($<r.scope>8) ,$<r.symbol>6,modi,modCounter);
  modCounter=0;	
  }
  statement_list_opt close_bl {
  $<r.node>$ = new FunctionDefineNode(Myparser->lookUpSymbol($<r.scope>8->getParentScope(),$<r.str>4), $<r.node>10, $<r.node>6, $<r.myLineNo>7, $<r.myColno>7);
  cout<<"*********method declaratio4n**********\n";}
  ;

formal_parameter_list_opt 
  : {$<r.symbol>$ = nullptr; $<r.node>$ = nullptr;}
  | formal_parameter_list {$<r.symbol>$ = $<r.symbol>1; $<r.node>$ = $<r.node>1 }
  ;
return_type
  : type
  | VOID  {
		 $<r.m>$ = VOIDT; 
		 return_type = VOIDT;
		 }
  ;
formal_parameter_list
  : formal_parameter {	ListNode* list = new ListNode("ParametarListNode");
						list->add_node($<r.node>1);
						$<r.node>$ = list;
					} 
  | formal_parameter_list COMMA formal_parameter {

			Symbol* trav = $<r.symbol>1;
			Symbol* travtemp = trav;
			while(trav != nullptr){
			    travtemp = trav;
				trav = trav->node;
		    }
			travtemp->node = $<r.symbol>3;
			$<r.symbol>$ = $<r.symbol>1;
			//node chain
		

			ListNode* list = dynamic_cast<ListNode*>($<r.node>1);
			list->add_node($<r.node>3);
			$<r.node>$ = list;
											}
  ;


formal_parameter
  : fixed_parameter {$<r.symbol>$ = $<r.symbol>1; $<r.node>$ = $<r.node>1 ;}
  | parameter_array {$<r.symbol>$ = $<r.symbol>1; $<r.node>$ = $<r.node>1 ;}
  ;
fixed_parameter
  : attributes_opt parameter_modifier_opt type IDENTIFIER {
						Parameter* paramSymbol = new Parameter($<r.str>4, $<r.myColno>3,$<r.myLineNo>3, $<r.m>3);
						$<r.symbol>$ = paramSymbol;
					    $<r.node>$ = new ParameterNode(paramSymbol, $<r.myLineNo>1, $<r.myColno>1);
															}
  ;
parameter_modifier_opt
  : 
  | REF
  | OUT
  ;
parameter_array
  : attributes_opt PARAMS type IDENTIFIER  {
											Parameter* paramSymbol = new Parameter($<r.str>4, $<r.myColno>3,$<r.myLineNo>3,$<r.m>4);
										    $<r.symbol>$ = paramSymbol;
											 $<r.node>$ = new ParameterNode(paramSymbol, $<r.myLineNo>1, $<r.myColno>1);
											}
  ;
property_declaration
  : attributes_opt modifiers_opt type qualified_identifier 
      ENTER_getset
    open_bl accessor_declarations close_bl
      EXIT_getset
  ;
accessor_declarations
  : get_accessor_declaration set_accessor_declaration_opt
  | set_accessor_declaration get_accessor_declaration_opt
  ;
set_accessor_declaration_opt
  : 
  | set_accessor_declaration
  ;
get_accessor_declaration_opt
  : 
  | get_accessor_declaration
  ;
get_accessor_declaration
  : attributes_opt GET 
      EXIT_getset
    accessor_body
      ENTER_getset
  ;
set_accessor_declaration
  : attributes_opt SET 
      EXIT_getset
    accessor_body
      ENTER_getset
  ;
accessor_body
  : block
  | ';'
  ;
event_declaration
  : attributes_opt modifiers_opt EVENT type variable_declarators ';'
  | attributes_opt modifiers_opt EVENT type qualified_identifier 
      ENTER_accessor_decl 
    open_bl event_accessor_declarations close_bl
      EXIT_accessor_decl
  ;
event_accessor_declarations
  : add_accessor_declaration remove_accessor_declaration
  | remove_accessor_declaration add_accessor_declaration
  ;
add_accessor_declaration
  : attributes_opt ADD 
      EXIT_accessor_decl 
    block 
      ENTER_accessor_decl
  ;
remove_accessor_declaration
  : attributes_opt REMOVE 
      EXIT_accessor_decl 
    block 
      ENTER_accessor_decl
  ;
indexer_declaration
  : attributes_opt modifiers_opt indexer_declarator 
      ENTER_getset
    open_bl accessor_declarations close_bl
      EXIT_getset
  ;
indexer_declarator
  : type THIS LEFT_BRACKET formal_parameter_list RIGHT_BRACKET

  | type qualified_this LEFT_BRACKET formal_parameter_list RIGHT_BRACKET
  ;
qualified_this
  : qualifier THIS
  ;

operator_declaration
  : attributes_opt modifiers_opt operator_declarator operator_body
  ;
operator_declarator
  : overloadable_operator_declarator
  | conversion_operator_declarator
  ;
overloadable_operator_declarator
  : type OPERATOR overloadable_operator '(' type IDENTIFIER ')'
  | type OPERATOR overloadable_operator '(' type IDENTIFIER COMMA type IDENTIFIER ')'
  ;
overloadable_operator
  : '+' | '-' 
  | '!' | '~' | PLUSPLUS | MINUSMINUS | TRUE | FALSE
  | '*' | '/' | '%' | '&' | '|' | '^' 
  | LTLT | GTGT | EQEQ | NOTEQ | '>' | '<' | GEQ | LEQ
  ;
conversion_operator_declarator
  : IMPLICIT OPERATOR type '(' type IDENTIFIER ')'
  | EXPLICIT OPERATOR type '(' type IDENTIFIER ')'
  ;
constructor_declaration
  : attributes_opt modifiers_opt IDENTIFIER open_ar formal_parameter_list_opt ')' constructor_initializer_opt open_bl statement_list_opt close_bl{
     Myparser->insertFunctionSymbol($<r.str>3,$<r.myColno>3, $<r.myLineNo>3 ,dynamic_cast<Scope*>($<r.scope>8) ,$<r.symbol>5,modi,modCounter);
	 modCounter=0;
																																					}
  | attributes_opt modifiers_opt IDENTIFIER open_ar formal_parameter_list_opt ')' constructor_initializer_opt ';'{
     Myparser->insertFunctionSymbol($<r.str>3,$<r.myColno>3, $<r.myLineNo>3 ,$<r.symbol>5,modi,modCounter);
	 modCounter=0;
																												 }
  ;

constructor_initializer_opt
  :
  | constructor_initializer
  ;
constructor_initializer
  : ':' BASE open_ar argument_list_opt ')'
  | ':' THIS open_ar argument_list_opt ')'
  ;


destructor_declaration
  : attributes_opt modifiers_opt '~' IDENTIFIER '(' ')' block 
  ;
operator_body
  : block
  | ';'
  ;



struct_declaration
  : attributes_opt modifiers_opt STRUCT IDENTIFIER struct_interfaces_opt open_bl {$<r.scope>6->setownername("struct");   modCounter=0;}  
  struct_body comma_opt {cout<<"*********struct declaration**********\n";}
  ;
struct_interfaces_opt
  : 
  | struct_interfaces
  ;
struct_interfaces
  : ':' interface_type_list
  ;
struct_body
  :  struct_member_declarations_opt close_bl
  ;
struct_member_declarations_opt
  : 
  | struct_member_declarations
  ;
struct_member_declarations
  : struct_member_declaration
  | struct_member_declarations struct_member_declaration
  ;
struct_member_declaration
  : constant_declaration
  | field_declaration
  | method_declaration
  | property_declaration
  | event_declaration
  | indexer_declaration
  | operator_declaration
  | constructor_declaration
  | type_declaration
  ;

array_initializer
  : open_bl variable_initializer_list_opt close_bl {$<r.scope>1->setownername("array init");}
  | open_bl variable_initializer_list COMMA close_bl {$<r.scope>1->setownername("ayyar init");}
  ;
variable_initializer_list_opt
  :
  | variable_initializer_list {$<r.node>$ = $<r.node>1 ; cout<<"variable_initializer_list";}
  ;
variable_initializer_list
  : variable_initializer {$<r.node>$ = $<r.node>1 ;}
  | variable_initializer_list COMMA variable_initializer
  ;


interface_declaration
  : attributes_opt modifiers_opt INTERFACEX IDENTIFIER interface_base_opt interface_body comma_opt
  ;
interface_base_opt
  : 
  | interface_base
  ;
interface_base
  : ':' interface_type_list
  ;
interface_body
  : open_bl interface_member_declarations_opt close_bl {$<r.scope>1->setownername("interface");}
  ;
interface_member_declarations_opt
  : 
  | interface_member_declarations
  ;
interface_member_declarations
  : interface_member_declaration
  | interface_member_declarations interface_member_declaration
  ;
interface_member_declaration
  : interface_method_declaration
  | interface_property_declaration
  | interface_event_declaration
  | interface_indexer_declaration
  ;
interface_method_declaration
  : attributes_opt new_opt type IDENTIFIER '(' formal_parameter_list_opt ')' interface_empty_body
  | attributes_opt new_opt VOID IDENTIFIER '(' formal_parameter_list_opt ')' interface_empty_body
  ;
new_opt
  : 
  | NEW
  ;
interface_property_declaration
  : attributes_opt new_opt type IDENTIFIER 
      ENTER_getset
    open_bl interface_accessors close_bl
      EXIT_getset
  ;
interface_indexer_declaration
  : attributes_opt new_opt type THIS 
    LEFT_BRACKET formal_parameter_list RIGHT_BRACKET 
      ENTER_getset
    open_bl interface_accessors close_bl
      EXIT_getset
  ;

interface_accessors
  : attributes_opt GET interface_empty_body
  | attributes_opt SET interface_empty_body
  | attributes_opt GET interface_empty_body attributes_opt SET interface_empty_body
  | attributes_opt SET interface_empty_body attributes_opt GET interface_empty_body
  ;
interface_event_declaration
  : attributes_opt new_opt EVENT type IDENTIFIER interface_empty_body
  ;

interface_empty_body
  : ';'
  | open_bl close_bl {$<r.scope>1->setownername("interface empty");}
  ;


enum_declaration
  : attributes_opt modifiers_opt ENUM IDENTIFIER enum_base_opt enum_body comma_opt 
  ;
enum_base_opt
  : 
  | enum_base
  ;
enum_base
  : ':' integral_type
  ;
enum_body
  : open_bl enum_member_declarations_opt close_bl {$<r.scope>1->setownername("enum");}
  | open_bl enum_member_declarations COMMA close_bl {$<r.scope>1->setownername("enum");}
  ;
enum_member_declarations_opt
  : 
  | enum_member_declarations
  ;
enum_member_declarations
  : enum_member_declaration
  | enum_member_declarations COMMA enum_member_declaration
  ;
enum_member_declaration
  : attributes_opt IDENTIFIER
  | attributes_opt IDENTIFIER '=' constant_expression
  ;


delegate_declaration
  : attributes_opt modifiers_opt DELEGATE return_type IDENTIFIER '(' formal_parameter_list_opt ')' 
  ;


attributes
  : attribute_sections
  ;
attribute_sections
  : attribute_section
  | attribute_sections attribute_section
  ;
attribute_section
  : ENTER_attrib LEFT_BRACKET attribute_target_specifier_opt attribute_list RIGHT_BRACKET EXIT_attrib
  | ENTER_attrib LEFT_BRACKET attribute_target_specifier_opt attribute_list COMMA RIGHT_BRACKET EXIT_attrib
  ;
attribute_target_specifier_opt
  :
  | attribute_target_specifier
  ;
attribute_target_specifier
  : attribute_target ':'
  ;
attribute_target
  : ASSEMBLY
  | FIELD
  | EVENT
  | METHOD
  | MODULE
  | PARAM
  | PROPERTY
  | RETURN
  | TYPEX
  ;
attribute_list
  : attribute
  | attribute_list COMMA attribute
  ;
attribute
  : attribute_name attribute_arguments_opt
  ;
attribute_arguments_opt
  : 
  | attribute_arguments
  ;
attribute_name
  : type_name
  ;
attribute_arguments
  : '(' expression_list_opt ')'
  ;




ENTER_attrib 
  : { ; }
  ;
EXIT_attrib 
  : { ; }
  ;
ENTER_accessor_decl 
  : { ; }
  ;
EXIT_accessor_decl
  : { ; }
  ;
ENTER_getset
  : { ; }
  ;
EXIT_getset
  : { ; }
  ;

%%

void yyerror(char *s) 
{
extern int colNo, lineNo;
errorRec.errQ->enqueue(lineNo,colNo,s,"\ninvlid TOKEN :can't reduce grammar");

}

int yylex()
{
	return lexer->yylex();
}

 void print_ast(Node *root, std::ostream &os,string name) 
 {
  os <<  name <<"\n";
  root->print(os);
  os << "}\n";
}
void main(void)
{
  
   yydebug =1;
    freopen("code.cs","r",stdin);
	freopen("out.txt","w",stdout);

	Parser* p = new Parser();
    p->parse();

	Myparser->printMyMap();
	
	Myparser->checkclassdec();
	
	if (!errorRec.errQ->isEmpty()) {
		errorRec.printErrQueue();	
	}


	ofstream ast("ast.txt");
	print_ast(tree, ast,"AST TREE");
	ast.close();
}
