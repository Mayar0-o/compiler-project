# C# Compiler
Compiler Project
## Getting Started
These instruction will get you a copy of the project up and running on your local machine for development and testing purposes (using GIT).
```
$ git clone https://gitlab.com/Miyarn/compiler-project.git
```
### External library and programs needed
* [Flex v2.6](https://www.gnu.org/software/flex/).
* [Bison v3.0.4](https://www.gnu.org/software/bison/).
* For Windows users, we prefer to use [Win-Flex-Bison](https://sourceforge.net/projects/winflexbison/) by [lexxmark](https://sourceforge.net/u/lexxmark/profile/), and Visual Studio from Microsoft.
### C# Refrence
Offical C# reference by Microsoft [here](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/).
## Lexer and Parser raw files
use [C# Parser and Lexer](http://www.cs.may.ie/~jpower/Research/csharp/Index.html) by [James Power](http://www.cs.may.ie/~jpower/).

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.